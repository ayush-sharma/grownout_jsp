__author__ = 'ayush'

import re
import collections
import copy
import sys
import math

from pymongo import MongoClient
from elasticsearch import Elasticsearch
from datetime import datetime

# client = MongoClient('130.211.119.14')
client = MongoClient('dev-mongo1.grownout.com')
db = client['test']['test_jsp_5000']

# from experience import get_job_durations

# SOME GLOCAL DEFINITIONS
# TRACE_BACK_JOB_NO = 0
TRACE_BACK_YEAR = "2010-01-01T00:00:00.000Z"
TRACE_BACK_YEAR = datetime.strptime(TRACE_BACK_YEAR, "%Y-%m-%dT%H:%M:%S.%fZ")
MIN_JOB_NO = 2 # minimumjob count a profile should have before applying the function
DAYS_IN_MONTH = 30  # One Month is 30 days
MONTHS_IN_YEAR = 12  # One year is 12 months
MONTHS_IN_HALF_YEAR = 6  # Six months in half year
MONTHS_IN_QUARTER_YEAR = 3  # Three months in Quarter Year
MIN_SIMILAR_USERS_REQUIRED = 10  # We will not do analytics if number of similar users is less.
CAN_SWITCH_NOW = 0  # Return 0 if we think he is probable to switch now.
LOOKING_FOR_JOB_CHANGE = 0  # Return 0 if he says looking for job change
MAX_SIMILAR_LIMIT = 5000
PASSIVE = 12
MAX_FETCH_PROFILE = 1000 # Indicates the number of profiles need to fetch from elasticsearch

# ERROR CODES
# ERROR_CODE = -1  # Linkedin Profile not found
# ERROR_CODE = -2  # Currently not working.
# ERROR_CODE = -3  # Working in first company, hence skipping
# ERROR_CODE = -4  # No creasing
# ERROR_CODE = -5  # Creasing will remove all company experience
# ERROR_CODE = -6  # Cannot test, as end_date is not specified

prod_es = Elasticsearch('http://130.211.114.20:6200/')
es_index = "complete"
es_type = "user"
SCROLL_TIMEOUT_SECS = '500000s'
SCROLL_SIZE = 13


def get_job_durations(experiences, educations):

    job_arr = []
    ex_index = 0
    for experience in experiences:

        try:
            if experience['end_date'] is not None:
                if type(experience['end_date']) == str or type(experience['end_date']) == unicode:
                    end_date = datetime.strptime(experience['end_date'], "%Y-%m-%dT%H:%M:%S.%fZ")
                else:
                    end_date = experience['end_date']

            elif experience['current']:
                end_date = TRACE_BACK_YEAR
                # end_date = datetime.today()
            else:
                end_date = None
        except:
            end_date = None

        try:
            if experience['start_date'] is not None:
                if type(experience['start_date']) == str or type(experience['start_date']) == unicode:
                    start_date = datetime.strptime(experience['start_date'], "%Y-%m-%dT%H:%M:%S.%fZ")
                else:
                    start_date = experience['start_date']

            else:
                start_date = None
        except:
            start_date = None

        if start_date is not None and end_date is not None:
            job_arr.append((experience['title'].lower(), start_date, end_date,'ex',ex_index))
        ex_index = ex_index + 1

    edu_arr = []
    ed_index = 0

    for education in educations:

        try:
            if education['end_date']is not None:
                if type(education['end_date']) == str or type(education['end_date']) == unicode:
                    end_date = datetime.strptime(education['end_date'], "%Y-%m-%dT%H:%M:%S.%fZ")
                else:
                    end_date = education['end_date']
            else:
                end_date = None
        except:
            end_date = None

        try:
            if education['start_date'] is not None:
                if type(education['start_date']) == str or type(education['start_date']) == unicode:
                    start_date = datetime.strptime(education['start_date'], "%Y-%m-%dT%H:%M:%S.%fZ")
                else:
                    start_date = education['start_date']
            else:
                start_date = None
        except:
            start_date = None

        if start_date is not None and end_date is not None:
            edu_arr.append(('student', start_date, end_date,'ed',ed_index))

        ed_index = ed_index + 1

    job_edu_arr = job_arr + edu_arr
    job_edu_arr.sort(key=lambda x: x[1])

    job_ex = 0
    job_ex_arr = []
    intern_ex = 0
    intern_ex_arr = []
    if len(job_edu_arr) > 0:
        min_end = job_edu_arr[0][1]

    remove_index = {}
    for job_edu in job_edu_arr:

        title, start_date, end_date, cat, index = job_edu

        if min_end > start_date:
            start_date = min_end

        work_ex = (end_date - start_date).days
        if work_ex < 0:
            work_ex = 0

        intern_word = ['intern',
                       'visitor',
                       'visiting',
                       'education',
                       'internee',
                       'scholar',
                       'summer',
                       'scholar',
                       'recipient',
                       'volunteer']
        title_word = title.split()

        if 'student' in title:
            category = -1
        elif len(list(set(title_word).intersection(intern_word))) > 0:
            category = 1
            remove_index[cat] = index
        else:
            category = 0

        if work_ex !=0:
            if category == 0:
                job_ex += work_ex
                job_ex_arr.append(work_ex)
            elif category == 1:
                intern_ex += work_ex
                intern_ex_arr.append(work_ex)

        if min_end < end_date:
            min_end = end_date

    return job_ex_arr, intern_ex_arr, remove_index


def filter_duplicate_company(company_names, job_start_dates):

   def remove_duplicates(values):
      output = []
      seen = set()
      for i, value in enumerate(values):
         if value not in seen:
            output.append(value)
            seen.add(value)


      return output

   (filtered_company_names) = remove_duplicates(company_names)

   filtered_job_start_dates = []
   company_names.reverse()

   for name in filtered_company_names:

      ind = company_names.index(name)
      filtered_job_start_dates.append(job_start_dates[-ind-1])

   return (filtered_company_names,filtered_job_start_dates)


def create_target_array(doc):

    # components of target array = ['id', [degree], [colleges], [college_start_dates], [company names], [company profile title], [company_start_date], [company_end_date], [current_location], [major], [skills_set]]

    target=[]

    institute_name=[]
    institute_start_date=[]
    institute_degree=[]
    major = []
    skills_set = []
    current_location = ''
    current_industry = ''

    company_name=[]
    company_start_date=[]
    company_end_date=[]
    company_title=[]

    # job end dates, job profile tilte and job end date are merely for manual data tesing, they are not used in the algorithm

    id = doc['_source']['_id']
    if 'current_location' in doc['_source']['linkedin']:
        current_location = doc['_source']['linkedin']['current_location']['name']


    for education in doc['_source']['linkedin']['educations']:

        # grab degree having qualification as graduate, postgraduate and postgraduate diploma


        if 'qualification' in education:
            qualification = education['qualification']['name']
            if (qualification == 'Graduate' or qualification == 'Postgraduate' or qualification == 'Postgraduate Diploma'):

                institute_name.append(education['institute']['name'])

                if 'start_date' in education:
                    institute_start_date.append(education['start_date'])
                else:
                    institute_start_date.append(None)

                if 'degree' in education:
                    institute_degree.append(education['degree']['name'])
                else:
                    institute_degree.append(None)

                if 'major' in education:
                    major.append(education['major']['name'])
                else:
                    major.append(None)

    # if education does not have qualification as mentioned above, then grab all degrees without any qualification resemblance

    if ((not institute_degree) or institute_degree == [None]):
        institute_name = []
        institute_start_date = []
        major = []
        for education in doc['_source']['linkedin']['educations']:
            institute_name.append(education['institute']['name'])

            if 'start_date' in education:
                institute_start_date.append(education['start_date'])
            else:
                institute_start_date.append(None)

            if 'degree' in education:
                institute_degree.append(education['degree']['name'])
            else:
                institute_degree.append(None)

            if 'major' in education:
                major.append(education['major']['name'])
            else:
                major.append(None)

    # check if the first company start date is null, if yes, then grab next valid start  date as first company start date
    check_last_none = ''
    for experience in doc['_source']['linkedin']['experiences']:

        company_name.append(experience['company']['name'])
        company_title.append(experience['title'])


        if 'start_date' in experience:
            check_last_none = experience['start_date']
            company_start_date.append(check_last_none)
        else:
            company_start_date.append(None)
            check_last_none = None

        if 'end_date' in experience:
            company_end_date.append(experience['end_date'])
        else:
            company_end_date.append(None)

    if check_last_none == None:
        if company_start_date and company_start_date != [None]:
            length = len(company_start_date)

            valid_start_date = ''
            for i in xrange(-2,-length,-1):
                valid_start_date = company_start_date[i]
                if valid_start_date != None or valid_start_date != '':
                    company_start_date[-1] = valid_start_date
                    break

            if valid_start_date == '':
                company_start_date = []

    if 'skills' in doc['_source']['linkedin']:
        for skills in doc['_source']['linkedin']['skills']:
            skills_set.append(skills)

    if 'current_industry' in doc['_source']['linkedin']:
        if doc['_source']['linkedin']['current_industry'] != None and doc['_source']['linkedin']['current_industry'] != '':
            current_industry = doc['_source']['linkedin']['current_industry']

    target.append(id)
    target.append(institute_degree)
    target.append(institute_name)
    target.append(institute_start_date)
    target.append(company_name)
    target.append(company_title)
    target.append(company_start_date)
    target.append(company_end_date)
    target.append([current_location])
    target.append(major)
    target.append(skills_set)
    target.append([current_industry])

    # filter_duplicate_company function filters duplicate company names and thier start dates
    if target[4] and target[6]:
        (company_names, job_start_dates) = filter_duplicate_company(target[4], target[6])
        target[4] = company_names
        target[6] = job_start_dates

    # components of target array = ['id', [degree], [colleges], [college_start_dates], [company names], [company profile title], [company_start_date], [company_end_date], [current_location], [major], [skills_set]]

    # college_start_dates_num = []
    # if target[3] and target[3] != [None] and target[3] != ['']:
    #
    #     for date in target[3]:
    #
    #         if date != None and date != '':
    #
    #             date = re.match(r'^(\d{4})\-',date,re.I)
    #             date = date.group(1)
    #             date = int(date)
    #         else:
    #             date = None
    #
    #         college_start_dates_num.append(date)
    #
    # target.append(college_start_dates_num)
    #
    # company_start_dates_num = []
    # if target[6] and target[6] != [None] and target[6] != ['']:
    #
    #     for date in target[6]:
    #
    #         if date != None and date != '':
    #
    #             date = re.match(r'^(\d{4})\-',date,re.I)
    #             date = date.group(1)
    #             date = int(date)
    #         else:
    #             date = None
    #
    #         company_start_dates_num.append(date)
    #
    # target.append(company_start_dates_num)

    return(target)


def go_to_past(profile,similar_users):


    creased_education_index = 0
    new_educations = []
    for education in profile['_source']['linkedin']['educations']:

        if 'start_date' in education:

            if education['start_date'] != None and education['start_date'] != '':

                if type(education['start_date']) == str or type(education['start_date']) == unicode:
                    start_date = datetime.strptime(education['start_date'], "%Y-%m-%dT%H:%M:%S.%fZ")
                else:
                    start_date = education['start_date']

                if start_date > TRACE_BACK_YEAR:
                    creased_education_index = creased_education_index + 1
                else:
                    new_educations = profile['_source']['linkedin']['educations'][creased_education_index : len(education)]
                    break


    new_experiences = []
    creased_experience_index = 0

    for experience in profile['_source']['linkedin']['experiences']:

        if 'start_date' in experience:

            if experience['start_date'] != None and experience['start_date'] != '':

                if type(experience['start_date']) == str or type(experience['start_date']) == unicode:
                    start_date = datetime.strptime(experience['start_date'], "%Y-%m-%dT%H:%M:%S.%fZ")
                else:
                    start_date = experience['start_date']

                if start_date > TRACE_BACK_YEAR:
                    creased_experience_index = creased_experience_index + 1
                else:
                    new_experiences = profile['_source']['linkedin']['experiences'][creased_experience_index : len(experience)]
                    break

    print "creased_experience_index",creased_experience_index
    print "creased_education_index", creased_education_index

    if not new_educations:
        new_educations = profile['_source']['linkedin']['educations']

    actual_end_date = None
    actual_start_date = None
    if not new_experiences:
        return(-4)
    else:
        actual_end_date = new_experiences[0]['end_date']
        print "actual_end_date",actual_end_date
        actual_start_date = new_experiences[0]['start_date']
        new_experiences[0]['end_date'] = None
        new_experiences[0]['current'] = True

    profile['_source']['linkedin']['educations'] = copy.deepcopy(new_educations)
    profile['_source']['linkedin']['experiences'] = copy.deepcopy(new_experiences)

    print get_job_switch_months(profile,actual_start_date,actual_end_date,similar_users)


    # college_start_dates_num = target[11]
    # company_start_dates_num = target[12]
    #
    # job_count = len(target[6])
    #
    # if job_count < 1:
    #     return -2
    #
    # if len(target[6]) < MIN_JOB_NO:
    #     return -3
    #
    # # components of target array = ['id', [degree], [colleges], [college_start_dates], [company names], [company profile title], [company_start_date], [company_end_date], [current_location], [major], [skills_set]]
    #
    # creased_company_index = 0
    # creased_college_index = 0
    # new_target = []
    # actual_end_date = None
    # actual_start_date = None
    #
    # if (TRACE_BACK_JOB_NO != 0) and (TRACE_BACK_JOB_NO < len(company_start_dates_num) and TRACE_BACK_YEAR == 0):
    #
    #     creased_date = company_start_dates_num[TRACE_BACK_JOB_NO]
    #
    #     old_company_start_dates = target[6]
    #     new_company_start_dates = old_company_start_dates[TRACE_BACK_JOB_NO : len(old_company_start_dates)]
    #
    #     old_exp_title = target[5]
    #     new_exp_title = old_exp_title[TRACE_BACK_JOB_NO : len(old_exp_title)]
    #
    #     old_company_end_dates = target[7]
    #     new_company_end_dates = old_company_end_dates[TRACE_BACK_JOB_NO : len(old_company_end_dates)]
    #
    #     old_company_names = target[4]
    #     new_company_names = old_company_names[TRACE_BACK_JOB_NO : len(old_company_names)]
    #
    #     creased_index = 0
    #
    #     if college_start_dates_num:
    #         for index, date in enumerate(college_start_dates_num):
    #             if date != None and date != '':
    #                 if date > creased_date:
    #                     creased_index = index + 1
    #                 else:
    #                     break
    #
    #     old_college_start_dates = target[3]
    #
    #     new_college_start_dates = old_college_start_dates[creased_index : len(old_college_start_dates)]
    #
    #     old_degree_names = target[1]
    #     new_degree_names = old_degree_names[creased_index : len(old_degree_names)]
    #
    #     old_college_names = target[2]
    #     new_college_names = old_college_names[creased_index : len(old_college_names)]
    #
    #     old_major = target[9]
    #     new_major = old_major[creased_index : len(old_major)]
    #
    #     new_target = [target[0], new_degree_names, new_college_names, new_college_start_dates, new_company_names, new_exp_title, new_company_start_dates, new_company_end_dates, new_major, target[10]]
    #
    #     creased_company_index = TRACE_BACK_JOB_NO
    #     creased_college_index = creased_index
    #
    #     print creased_index
    #     actual_end_date = new_company_end_dates[0]
    #
    #
    # elif (TRACE_BACK_YEAR != 0 and TRACE_BACK_JOB_NO == 0):
    #
    #     creased_company_index = 0
    #
    #     for index, date in enumerate(company_start_dates_num):
    #         if date != None and date != '':
    #             if date > TRACE_BACK_YEAR:
    #                 creased_company_index = index + 1
    #             else:
    #                 break
    #
    #     old_company_start_dates = target[6]
    #
    #     if creased_company_index >= len(old_company_start_dates):
    #         return(-5)
    #
    #     new_company_start_dates = old_company_start_dates[creased_company_index : len(old_company_start_dates)]
    #
    #     old_exp_title = target[5]
    #     new_exp_title = old_exp_title[creased_company_index : len(old_exp_title)]
    #
    #     old_company_end_dates = target[7]
    #     new_company_end_dates = old_company_end_dates[creased_company_index : len(old_company_end_dates)]
    #
    #     old_company_names = target[4]
    #     new_company_names = old_company_names[creased_company_index : len(old_company_names)]
    #
    #     creased_college_index = 0
    #
    #     for index, date in enumerate(college_start_dates_num):
    #         if date != None and date != '':
    #             if date > TRACE_BACK_YEAR:
    #                 creased_college_index = index + 1
    #             else:
    #                 break
    #
    #     old_college_start_dates = target[3]
    #
    #     new_college_start_dates = old_college_start_dates[creased_college_index : len(old_college_start_dates)]
    #
    #     old_degree_names = target[1]
    #     new_degree_names = old_degree_names[creased_college_index : len(old_degree_names)]
    #
    #     old_college_names = target[2]
    #     new_college_names = old_college_names[creased_college_index : len(old_college_names)]
    #
    #     old_major = target[9]
    #     new_major = old_major[creased_college_index : len(old_major)]
    #
    #     new_target = [target[0], new_degree_names, new_college_names, new_college_start_dates, new_company_names, new_exp_title, new_company_start_dates, new_company_end_dates, target[8],new_major, target[10]]
    #
    #     actual_start_date = new_company_start_dates[0]
    #     actual_end_date = new_company_end_dates[0]
    #     print "creased_company_index", creased_company_index
    #     print "creased_college_index", creased_college_index
    #
    #
    #
    #
    # else:
    #     print "enter TRACE_BACK_JOB_NO or TRACE_BACK_YEAR_COUNT"
    #     exit(0)
    #
    #
    # if creased_college_index == 0 and creased_company_index == 0:
    #     return(-4)
    #
    #
    # educations = profile['_source']['linkedin']['educations']
    # experiences = profile['_source']['linkedin']['experiences']
    #
    # print "new1 exp", experiences
    #
    # # for i in xrange(0,creased_college_index-1):
    # #     del educations[0]
    # #
    # # for i in xrange(0,creased_company_index-1):
    # #     del experiences[0]
    # #
    # experiences[0]['current'] = True
    # experiences[0]['end_date'] = None
    #
    # new_educations = educations[creased_college_index : len(educations)]
    # new_experiences = experiences[creased_company_index : len(experiences)]
    #
    # new_experiences[0]['current'] = True
    # new_experiences[0]['end_date'] = None
    #
    #
    #
    # print "new1 exp", new_experiences
    #
    # print "ACTUAL END DATE", actual_end_date
    # print get_job_switch_months(new_target, profile,actual_end_date,year_str,new_educations,new_experiences)


def is_looking_for_job_change(profile):

    profile_text = profile['name'] if ('name' in profile) else ''
    profile_text += profile['headline'] if ('headline' in profile) else ''
    profile_text += profile['summary'] if ('summary' in profile) else ''
    if profile_text == '':
        return False

    # Code for checking if he has specifically written that he is
    # looking for job change or looking for opportunities.
    # active_text_list = ["looking for job",
    #                     "looking for opportunity",
    #                     "looking for opportunities",
    #                     "looking for change"
    #                     ]
    active_text_list = ["looking for"]
    for active_text in active_text_list:
        if active_text in profile_text:
            return True

    # Code for checking email id.
    email_matched = re.search(r'[\w\.-]+@[\w\.-]+', profile_text)
    if email_matched:
        return True

    # Code for checking phone number.
    # Assuming indian phone numbers
    if "+91" in profile_text:
        return True
    phone_matched = map(int, re.findall(r'(\d{10})', profile_text))
    if phone_matched:
        return True

    return False


def get_job_switch_months(profile,actual_start_date,actual_end_date,similar_users):
    print"***** Entering get_job_switch_months *****"

    profile_id = str(profile['_id'])

    if 'linkedin' not in profile['_source']:
        return -1

    if is_looking_for_job_change(profile):
        return LOOKING_FOR_JOB_CHANGE

    educations = profile['_source']['linkedin']['educations'] if ('educations' in profile['_source']['linkedin']) else []
    experiences = profile['_source']['linkedin']['experiences'] if ('experiences' in profile['_source']['linkedin']) else []

    job_durations, intern_durations, remove_index = get_job_durations(experiences, educations)

    new_experiences = []
    new_educations = []

    if remove_index:

        remove_ex_indexes = []
        remove_ed_indexes = []
        for key, value in remove_index.iteritems():

            if key == 'ex':
                remove_ex_indexes.append(value)
            if key == 'ed':
                remove_ed_indexes.append(value)

        if remove_ex_indexes:
            new_experiences = [value for index, value in enumerate(experiences) if index not in remove_ex_indexes]

        if remove_ed_indexes:
            new_educations = [value for index, value in enumerate(educations) if index not in remove_ed_indexes]

    if new_experiences:
        profile['_source']['linkedin']['experiences'] = copy.deepcopy(new_experiences)

    if new_educations:
        profile['_source']['linkedin']['educations'] = copy.deepcopy(new_educations)

    print "new profile", profile

    job_count = len(job_durations)

    if job_count < 1:
        return -2


    current_job_duration = job_durations[-1]
    current_job_duration_in_months = current_job_duration/DAYS_IN_MONTH
    print "current_job_duration", current_job_duration_in_months

    month_diff = None
    if (type(actual_end_date) == str or type(actual_end_date) == unicode) and (actual_end_date != None):
        actual_end_date = datetime.strptime(actual_end_date, "%Y-%m-%dT%H:%M:%S.%fZ")

        # actual_start_date = datetime.strptime(actual_start_date, "%Y-%m-%dT%H:%M:%S.%fZ")

        month_diff = (actual_end_date - TRACE_BACK_YEAR).days/DAYS_IN_MONTH

    print "*****Actual job month diff", month_diff

    if month_diff == None or month_diff <=0: #sharma
        return(-6)

    target = create_target_array(profile)

    print "new_target", target

    final_es_query = build_es_query(target)

    # target_job_start_dates = target[6]
    # target_job_end_dates = target[7]
    #
    # target_job_start_dates.reverse()
    # target_job_end_dates.reverse()



    # try:
    #
    #     similar_users = prod_es.search(index=es_index, doc_type=es_type, body=final_es_query, request_timeout=30000)
    #     result = db.save({"target_id": profile_id,"similar_profiles":similar_users})
    #     print "data saved", result
    # except Exception as e:
    #     print e
    #     return(0)


    if similar_users['hits']['total'] > 0:
        print "profiles fetched", similar_users['hits']['total']
        if similar_users['hits']['total'] < 10: #sharma
            return(-7)
        similar_users = similar_users['hits']['hits']

    similar_user_job_durations = []
    same_user_job_durations = []
    closeness_array = [] #sharma
    candidate_job_durs = []
    for similar_user in similar_users:
        # print similar_user
        if '_source' in similar_user:
            similar_user = similar_user['_source']

            # reminder for khaleeque: include nth term

            closeness = None
            if 'experiences' in similar_user['linkedin'] and len(similar_user['linkedin']['experiences']) >= job_count:
                jds, ids, remove_index = get_job_durations(similar_user['linkedin']['experiences'], similar_user['linkedin']['educations'])

                if len(jds) >= job_count:

                    candi_job_dur_to_write = None

                    for index, job_duration in enumerate(job_durations):

                        candidate_job_dur = jds[index]
                        diff = abs(candidate_job_dur - job_duration)/DAYS_IN_MONTH
                        if closeness ==None:
                            closeness = 0
                        closeness += diff
                        print "calc diff",job_duration,candidate_job_dur,diff,closeness,jds
                        candi_job_dur_to_write = candidate_job_dur/DAYS_IN_MONTH

                    if candi_job_dur_to_write != None and closeness != None:
                        candidate_job_durs.append(candi_job_dur_to_write)
                    else:
                        continue

                    score = 0
                    if closeness != None:

                        closeness_array.append(closeness) #sharma

                        cond = float(closeness)/job_count

                        if cond <= 2:
                            score = 1
                        else:
                            score = 2*(math.exp(-0.347*cond))

                    # print jds
                    if len(jds) > job_count:
                        # print "similar_user", similar_user['_id']
                        similar_user_job_durations.append((jds[job_count-1],score))

                    elif len(jds) == job_count:
                        same_user_job_durations.append(jds[job_count-1])

                    print "\n##############################################\n"


    reg_str_write = profile_id #sharma

    for closeness in closeness_array:
        reg_str_write = reg_str_write + "," + str(closeness)
    # reg_str_write = reg_str_write + "," + month_diff + "\n"

    reg_str_write = reg_str_write + "\n"

    reg_inp.write(reg_str_write)

    reg_str_out_write = str(month_diff) + "\n"
    reg_out.write(reg_str_out_write)

    reg_out_curr_job_dur_write = str(current_job_duration/DAYS_IN_MONTH) + "\n"
    reg_out_curr_job_dur.write(reg_out_curr_job_dur_write)

    reg_out_total_job_dur_write = str((current_job_duration/DAYS_IN_MONTH)+month_diff) + "\n"
    reg_out_total_job_dur.write(reg_out_total_job_dur_write)

    reg_candidate_job_durs_write = profile_id
    for candi in candidate_job_durs:
        reg_candidate_job_durs_write = reg_candidate_job_durs_write + "," +str(candi)

    reg_candidate_job_durs_write = reg_candidate_job_durs_write + "\n"

    reg_output_candidate_job_durs.write(reg_candidate_job_durs_write)

    similar_users_count = len(similar_user_job_durations)

    print"Printing number of similar users : ", similar_users_count

    # Performing Analysis after getting similar users

    print "current_job_duration", current_job_duration_in_months
    print "*****Actual job month diff", month_diff

    # print "similar_user_job_durations", similar_user_job_durations

    # If no similar user is found, What we can suggest is that
    # he will complete his this year if more than 3 moths passed
    # else ready to switch (25:75 thing)
    if (similar_users_count == 0 ) or (similar_users_count < MIN_SIMILAR_USERS_REQUIRED):
        similar_users_count = len(same_user_job_durations)
        if similar_users_count < MIN_SIMILAR_USERS_REQUIRED:
            months_into_current_job = (current_job_duration/DAYS_IN_MONTH) % MONTHS_IN_YEAR
            if months_into_current_job > MONTHS_IN_QUARTER_YEAR:
                job_switch_months_left = MONTHS_IN_YEAR - months_into_current_job
                str_write = profile_id + "," + str(month_diff) + "," + str(job_switch_months_left) + "\n"
                f.write(str_write)
            else:
                job_switch_months_left = 0
                print "Months Remaining to Switch", job_switch_months_left
                str_write = profile_id + "," + str(month_diff) + "," + str(job_switch_months_left) + "\n"
                f.write(str_write)
            return job_switch_months_left
        else:
            minimum_working_duration = get_minimum_working_duration(same_user_job_durations, current_job_duration_in_months)
            if minimum_working_duration - current_job_duration_in_months > 3:
                str_write = profile_id + "," + str(month_diff) + "," + str(PASSIVE) + "\n"
                f.write(str_write)
                return PASSIVE
            else:
                str_write = profile_id + "," + str(month_diff) + "," + str(minimum_working_duration - current_job_duration_in_months + 1) + "\n"
                f.write(str_write)
                return minimum_working_duration - current_job_duration_in_months + 1

    # Doing some more analysis over the returned array of job durations.

    # similar_user_job_durations = [x/DAYS_IN_MONTH for x in similar_user_job_durations]
    # print "similar_user_job_durations in months", similar_user_job_durations
    # print "same_user_job_durations", same_user_job_durations
    optimal_job_duration = get_analysis_level_two(similar_user_job_durations, current_job_duration_in_months)
    months_remaining_to_switch = optimal_job_duration - current_job_duration_in_months

    print "Months Remaining to Switch", months_remaining_to_switch
    print "RESULT*********************************************"
    str_write = profile_id + "," + str(month_diff) + "," + str(months_remaining_to_switch) + "\n"
    print str_write
    f.write(str_write)
    if months_remaining_to_switch < 1:
        return 0
    return months_remaining_to_switch


def modified_build_es_query(target):

    # components of target array = ['id', [degree], [colleges], [college_start_dates], [company names], [company profile title], [company_start_date], [company_end_date], [current_location], [major], [skills_set]]

    print "new_target",target

    all_degree_query = [] # it contains the AND query components for education (i.e degrees,major and start_date)
    degree_filter_only = [] # it contains the filter weight query for degrees and major(if exists)
    considered_degrees = [] # out of all valid and rubbish degree names, which degrees were chosen is maintained here using their index value
    filters = [] # it contains all the filter weights for used in es query
    considered_filter_degree = [] # it contains the index of considered degree filter

    # check if target degrees exists, if yes then check for corresponding major_name and start_date

    degree_query = {
        "bool": {
            "must": []
        }
    }

    education_index = max(len(target[1]),len(target[3]),len(target[9])) - 1

    for index in xrange(0,education_index):

        if target[1] and target[1] != [None]:
            if target[1][index] != None and target[1][index] != '':

                input_degree = target[1][index]

                degree_query['bool']['must'].append(
                    {
                        "term":{
                            "linkedin.educations.degree.name": input_degree.encode('utf8')
                        }
                    }
                )

        if target[9] and target[9] != [None]:
            if target[9][index] != None and target[9][index] != '':

                input_major = target[9][index]

                degree_query['bool']['must'].append(
                    {
                        "term":{
                            "linkedin.educations.major.name": input_major.encode('utf8')
                        }
                    }
                )

        if target[3] and target[3] != [None]:
            if target[3][index] != None and target[3][index] != '':

                input_college_start_date = target[3][index]

                degree_query['bool']['must'].append({
                       "range": {
                          "linkedin.educations.start_date": {
                             "gte": input_college_start_date.encode('utf8') + "||-72M",
                             "lte": input_college_start_date.encode('utf8') + "||+12M",
                          }
                       }
                    })

        all_degree_query.append(degree_query)






    filter_deg_query = {
        "filter":{
            "nested": {
                "path": "linkedin.educations",
                "query": {
                    "bool": {
                        "must": []
                    }
                }
            }
        }
    }

    if education_index:

        each_degree_weight = 28/float(education_index+1)

        for index in xrange(0,education_index):

            if target[1] and target[1] != [None]:
                if target[1][index] != None and target[1][index] != '':

                    input_degree = target[1][index]

                    filter_deg_query['filter']['nested']['query']['bool']['must'].append(
                        {
                            "term":{
                                "linkedin.educations.degree.name": input_degree.encode('utf8')
                            }
                        }
                    )

            if target[9] and target[9] != [None]:
                if target[9][index] != None and target[9][index] != '':

                    input_major = target[9][index]

                    filter_deg_query['filter']['nested']['query']['bool']['must'].append(
                        {
                            "term":{
                                "linkedin.educations.major.name": input_major.encode('utf8')
                            }
                        }
                    )

            if target[3] and target[3] != [None]:
                if target[3][index] != None and target[3][index] != '':

                    input_college_start_date = target[3][index]

                    filter_deg_query['filter']['nested']['query']['bool']['must'].append({
                           "range": {
                              "linkedin.educations.start_date": {
                                 "gte": input_college_start_date.encode('utf8') + "||-72M",
                                 "lte": input_college_start_date.encode('utf8') + "||+12M",
                              }
                           }
                        })



            filters.append(filter_deg_query)
























    # if target[1] and target[1] != [None]:
    #
    #    for index, input_degree in enumerate(target[1]):
    #
    #        if input_degree != None:
    #
    #           degree_query = {
    #              "bool": {
    #                 "must": [
    #                    {
    #                       "match": {
    #                          "linkedin.educations.degree.name": input_degree.encode('utf8')
    #                       }
    #                    }
    #                 ]
    #              }
    #            }
    #
    #           # check if major exists for corresponding degree
    #
    #           if target[9] and target[9][index] != None and target[9][index] != '':
    #
    #              major = target[9][index]
    #              degree_query['bool']['must'].append({
    #                 "match": {
    #                    "linkedin.educations.major.name": major.encode('utf8')
    #                 }
    #              })
    #
    #           # check if start_date exists for corresponding degree
    #
    #           if target[3] and target[3][index] != None and target[3][index] != '':
    #
    #              degree_start_date = target[3][index]
    #              degree_query['bool']['must'].append({
    #                    "range": {
    #                       "linkedin.educations.start_date": {
    #                          "gte": degree_start_date.encode('utf8') + "||-72M",
    #                          "lte": degree_start_date.encode('utf8') + "||+12M",
    #                       }
    #                    }
    #                 })
    #
    #           all_degree_query.append(degree_query)
    #
    #           considered_degrees.append(index)
    #
    #
    # # if degree array does not exist, then coose major names and start date only
    # elif target[1] == [None] or (not target[1]):
    #
    #    print "NOT A PERFECT PROFILE" # because it does not have degree mentioned
    #
    #    # check if major exists for corresponding degree
    #
    #    if target[9] and target[9] != [None] and target[9] != '':
    #
    #       for index, major in enumerate(target[9]):
    #
    #          if major != None and major != '':
    #
    #              degree_query = {
    #                 "bool": {
    #                    "must": [
    #                       {
    #                          "match": {
    #                             "linkedin.educations.major.name": major.encode('utf8')
    #                          }
    #                       }
    #                    ]
    #                 }
    #               }
    #
    #              # check if start_date exists for corresponding degree
    #
    #              if target[3] and target[3][index] != None and target[3]['index'] != '':
    #
    #                 degree_start_date = target[3][index]
    #                 degree_query['bool']['must'].append({
    #                       "range": {
    #                          "linkedin.educations.start_date": {
    #                             "gte": degree_start_date.encode('utf8') + "||-72M",
    #                             "lte": degree_start_date.encode('utf8') + "||+12M",
    #                          }
    #                       }
    #                    })
    #
    #              all_degree_query.append(degree_query)
    #
    #              considered_degrees.append(index)
    #
    #    # if degree_name and major_name does not exist, then select start_date only
    #
    #    elif (target[9] == [None] or (not target[9]) or target[9] == ['']) and (target[3] and target[3] != [None] and target[3] != ['']):
    #
    #       for index, degree_start_date in enumerate(target[3]):
    #
    #          if degree_start_date != None and degree_start_date != '':
    #
    #              degree_query = {
    #                 "bool": {
    #                    "must": [
    #                       {
    #                          "range": {
    #                             "linkedin.educations.start_date": {
    #                                "gte": degree_start_date.encode('utf8') + "||-72M",
    #                                "lte": degree_start_date.encode('utf8') + "||+12M",
    #                             }
    #                          }
    #                       }
    #                    ]
    #                 }
    #               }
    #
    #              all_degree_query.append(degree_query)
    #
    #              considered_degrees.append(index)



    # create filters for degree with major if exists with specific weights

    if considered_degrees:
        each_degree_weight = 28/float(len(considered_degrees))

        if target[1] and target[1] != [None]:

           for index, input_degree in enumerate(target[1]):

              if input_degree != None and input_degree != '':

                  filter_deg_query = {
                      "filter":{
                         "nested": {
                            "path": "linkedin.educations",
                            "query": {
                               "bool": {
                                 "must": [
                                    {
                                       "match": {
                                          "linkedin.educations.degree.name": input_degree.encode('utf8')
                                       }
                                    }
                                 ]
                              }
                            }
                         }
                      },
                      "weight": each_degree_weight
                   }


                  if target[9] and target[9][index] != None and target[9][index] != '':

                     major = target[9][index]
                     filter_deg_query['filter']['nested']['query']['bool']['must'].append({
                        "match": {
                           "linkedin.educations.major.name": major.encode('utf8')
                        }
                     })

                  filters.append(filter_deg_query)
                  degree_filter_only.append(filter_deg_query)
                  considered_filter_degree.append(index)

        # create filter for major, if degree does not exist with specific weights

        elif target[1] == [None] or (not target[1]):

           if target[9] and target[9] != [None] and target[9] != ['']:
              each_degree_weight = 28/float(len(target[9]))

              for index, major in enumerate(target[9]):

                  if major != None and major != '':

                      filter_deg_query = {
                          "filter":{
                             "nested": {
                                "path": "linkedin.educations",
                                "query": {
                                   "bool": {
                                     "must": [
                                        {
                                           "match": {
                                              "linkedin.educations.major.name": major.encode('utf8')
                                           }
                                        }
                                     ]
                                  }
                                }
                             }
                          },
                          "weight": each_degree_weight
                       }

                      filters.append(filter_deg_query)
                      degree_filter_only.append(filter_deg_query)
                      considered_filter_degree.append(index)


    # create filter for colleges for consisdered degrees only, (i.e ignoring school names) with specific weights

    if considered_degrees:
       each_college_weight = 12/float(len(considered_degrees))
       for index in considered_degrees:

          if target[2] and target[2][index] != None and target[2][index] != '' :

             input_college = target[2][index]

             filter_college = {
                 "filter":{
                    "nested": {
                       "path": "linkedin.educations",
                       "query": {
                          "term": {
                             "linkedin.educations.institute.name.raw": {
                                "value": input_college.encode('utf8')
                             }
                          }
                       }
                    }
                 },
                "weight": each_college_weight
              }

             filters.append(filter_college)

    # if we did not catch considered degrees, then grab all the institue names as college degrees and create their filters with specific weights

    elif target[2] and target[2] != [None]:
       each_college_weight = 12/float(len(target[2]))
       for index, college_name in enumerate(target[2]):

          input_college = target[2][index]

          if input_college != None and input_college != '':

              filter_college = {
                  "filter":{
                     "nested": {
                        "path": "linkedin.educations",
                        "query": {
                           "term": {
                              "linkedin.educations.institute.name.raw": {
                                 "value": input_college.encode('utf8')
                              }
                           }
                        }
                     }
                  },
                 "weight": each_college_weight
               }

              filters.append(filter_college)



    # create filter for college start date using degree name, major name and start date if exists
    # created each filter covering all the time duration individually, as there was no other means to ...
    # do it except in script by looping in education of each profile which would be very time consuming

    if degree_filter_only:
        degree_filter_only_length = len(degree_filter_only)
        for index, fil_deg in enumerate(degree_filter_only):

            fil_index = considered_filter_degree[index]

            if target[3][fil_index] != None and target[3][fil_index] != '':

                degree_start_date = target[3][fil_index]

                filter_college_start_date = copy.deepcopy(fil_deg)

                filter_college_start_date['filter']['nested']['query']['bool']['must'].append(
                  {
                     "range": {
                        "linkedin.educations.start_date": {
                           "gte": degree_start_date.encode('utf8'),
                           "lte": degree_start_date.encode('utf8') + "||+11M"
                        }
                     }
                  })


                filter_college_start_date['weight'] = 16*0.2/float(degree_filter_only_length)

                filters.append(filter_college_start_date)


                filter_college_start_date1 = copy.deepcopy(fil_deg)
                filter_college_start_date1['filter']['nested']['query']['bool']['must'].append(
                    {
                     "range": {
                         "linkedin.educations.start_date": {
                            "gte": degree_start_date.encode('utf8') + "||-12M",
                            "lte": degree_start_date.encode('utf8') + "||-1M"
                         }
                      }
                    })

                filter_college_start_date1['weight'] = 16*0.6/degree_filter_only_length

                filters.append(filter_college_start_date1)


                filter_college_start_date2 = copy.deepcopy(fil_deg)
                filter_college_start_date2['filter']['nested']['query']['bool']['must'].append(
                    {
                     "range": {
                         "linkedin.educations.start_date": {
                            "gte": degree_start_date.encode('utf8') + "||-24M",
                            "lte": degree_start_date.encode('utf8') + "||-13M"
                         }
                      }
                    })

                filter_college_start_date2['weight'] = 16/degree_filter_only_length


                filters.append(filter_college_start_date2)


                filter_college_start_date3 = copy.deepcopy(fil_deg)
                filter_college_start_date3['filter']['nested']['query']['bool']['must'].append(
                    {
                     "range": {
                         "linkedin.educations.start_date": {
                            "gte": degree_start_date.encode('utf8') + "||-36M",
                            "lte": degree_start_date.encode('utf8') + "||-25M"
                         }
                      }
                    })

                filter_college_start_date3['weight'] = 16*0.8/degree_filter_only_length

                filters.append(filter_college_start_date3)


                filter_college_start_date4 = copy.deepcopy(fil_deg)
                filter_college_start_date4['filter']['nested']['query']['bool']['must'].append(
                    {
                     "range": {
                         "linkedin.educations.start_date": {
                            "gte": degree_start_date.encode('utf8') + "||-48M",
                            "lte": degree_start_date.encode('utf8') + "||-37M"
                         }
                      }
                    })

                filter_college_start_date4['weight'] = 16*0.4/degree_filter_only_length

                filters.append(filter_college_start_date4)

                filter_college_start_date = []
                filter_college_start_date1 = []
                filter_college_start_date2 = []
                filter_college_start_date3 = []
                filter_college_start_date4 = []

















    # create filter for company names with specific weights

    company_length = 0
    if target[4] and target[4] != [None] and target[4] != '':

       company_length = len(target[4])
       for company_name in target[4]:

           if company_name != None and company_name != '':

               filter_company_name = {
                   "filter": {
                      "nested": {
                         "path": "linkedin.experiences",
                         "query": {
                            "match_phrase": {
                               "linkedin.experiences.company.name": company_name.encode('utf8')
                            }
                         }
                      }
                   },
                   "weight": 14/float(company_length)
                }

               filters.append(filter_company_name)

    # create filter for current location with specific weights

    curr_loc_name = target[8][0]
    if curr_loc_name != None and curr_loc_name != '':
        filter_curr_loc = {
        "filter": {
           "term": {
              "linkedin.current_location": curr_loc_name.encode('utf8')
           }
        },
        "weight": 4
        }

        filters.append(filter_curr_loc)




    # create AND component having skill set

    skill_query = ''
    if target[10] and target[10] != [None] and target[10] != ['']:
       min_match = 0.4*len(target[10])

       min_match = int(min_match)

       if min_match > 6:
           min_match = 6

       skill_query = {
          "terms": {
             "linkedin.skills.raw": target[10],"minimum_match": min_match
          }
       }



    es_input_query = {
      "bool": {
         "must": []
      }
    }

    # if degree AND components created then append in the es query
    if all_degree_query:

        es_input_query['bool']['must'].append(
            {
                "nested": {
                  "path": "linkedin.educations",
                  "query": {
                      "bool": {
                         "should": [
                         ]
                      }
                   }
               }
            }
        )

        for degree in all_degree_query:
            es_input_query['bool']['must'][0]['nested']['query']['bool']['should'].append(degree)

    # if skills_set AND component created above, then append in the es query
    if skill_query:
        es_input_query['bool']['must'].append(skill_query)


    # if degree and skills_set AND components created above, then only create filter having scripting score, ...
    # otherwise the script would run on huge number of records if any of the AND components does not exist
    if (all_degree_query) and (skill_query):

        if target[6] and target[6][-1] != None and target[6][-1] != '' and company_length != 0:
            first_job_start_date = target[6][-1]
            first_job_start_date = re.match(r'^(\d{4})\-',first_job_start_date,re.I)
            first_job_start_date = first_job_start_date.group(1)

            first_job_start_date = str(first_job_start_date)
            company_length = str(company_length)

            # inp = raw_input("script_score")

            filters.append(
                {
                   "script_score": {
                      "script" : "f_score=0;total=0;def date='';target_year=" + first_job_start_date + ";target_no_jobs=" + company_length + ";for(exp in _source.linkedin.experiences){total=total+1;if(exp.containsKey('start_date')){date=exp['start_date'];};};match=date=~/^(?<year>\\d{4})\\-/;if(match){def year=match.group('year').toInteger();def diff=year-target_year;if((-4<diff) && (diff<0)){f_score=Math.abs(diff);f_score=f_score*3;};else{return(-100);};if(f_score<0){f_score=0};};def job_diff=Math.abs(target_no_jobs-total+1);job_score=target_no_jobs-job_diff;job_score=(job_score/target_no_jobs)*11;if(job_score<0){job_score=0;};f_score=f_score+job_score;return(f_score);"

                      # "script" : "f_score=0;total=0;def date='';target_year=" + first_job_start_date + ";target_no_jobs=" + company_length + ";for(exp in _source.linkedin.experiences){total=total+1;if(exp.containsKey('start_date')){date=exp['start_date'];};};if(total<=target_no_jobs){return(-100)};match=date=~/^(?<year>\\d{4})\\-/;if(match){def year=match.group('year').toInteger();def diff=Math.abs(target_year-year);if(diff<5){f_score=5-diff;f_score=f_score*3;};if(f_score<0){f_score=0};};def job_diff=Math.abs(target_no_jobs-total+1);job_score=target_no_jobs-job_diff;job_score=(job_score/target_no_jobs)*11;if(job_score<0){job_score=0;};f_score=f_score+job_score;return(f_score);"
                       # "script": "f_score=0;total=0;def date='';target_year=" + first_job_start_date + ";target_no_jobs=" + company_length + ";for(exp in _source.linkedin.experiences){total=total+1;if(exp.containsKey('start_date')){date=exp['start_date'];};};match=date=~/^(?<year>\\d{4})\\-/;if(match){def year=match.group('year').toInteger();def diff=Math.abs(target_year-year);if(diff<5){f_score=5-diff;f_score=f_score*3;};if(f_score<0){f_score=0};};def job_diff=Math.abs(target_no_jobs-total+1);job_score=target_no_jobs-job_diff;job_score=(job_score/target_no_jobs)*11;if(job_score<0){job_score=0;};f_score=f_score+job_score;return(f_score);"
                   }
                }
                )

    # if degree and skills_set AND component are NOT created then simply match_all the es query with the assigned filters

    if (not all_degree_query) and (not skill_query):
        es_input_query = {
            "match_all": {}
        }

        print "NOT A PERFECT PROFILE"





    print "es_input_query : ", es_input_query
    print "\n"
    print "filters : ", filters
    print "\n"

    final_es_query = {
           "size": MAX_FETCH_PROFILE,
           "query": {
              "function_score": {
                 "query": es_input_query,
                 "functions": filters,
                 "score_mode": "sum",
                 "boost_mode": "replace"
              }
           }
        }

    print "final_es_query : ", final_es_query

    return final_es_query


def build_es_query(target):

    # components of target array = ['id', [degree], [colleges], [college_start_dates], [company names], [company profile title], [company_start_date], [company_end_date], [current_location], [major], [skills_set]]

    all_degree_query = [] # it contains the AND query components for education (i.e degrees,major and start_date)
    degree_filter_only = [] # it contains the filter weight query for degrees and major(if exists)
    considered_degrees = [] # out of all valid and rubbish degree names, which degrees were chosen is maintained here using their index value
    filters = [] # it contains all the filter weights for used in es query
    considered_filter_degree = [] # it contains the index of considered degree filter

    # check if target degrees exists, if yes then check for corresponding major_name and start_date
    if target[1] and target[1] != [None]:

       for index, input_degree in enumerate(target[1]):

           if input_degree != None:

              degree_query = {
                 "bool": {
                    "must": [
                       {
                          "match": {
                             "linkedin.educations.degree.name": input_degree.encode('utf8')
                          }
                       }
                    ]
                 }
               }

              # check if major exists for corresponding degree

              if target[9] and target[9][index] != None and target[9][index] != '':

                 major = target[9][index]
                 degree_query['bool']['must'].append({
                    "match": {
                       "linkedin.educations.major.name": major.encode('utf8')
                    }
                 })

              # check if start_date exists for corresponding degree

              if target[3] and target[3][index] != None and target[3][index] != '':

                 degree_start_date = target[3][index]
                 degree_query['bool']['must'].append({
                       "range": {
                          "linkedin.educations.start_date": {
                             "gte": degree_start_date.encode('utf8') + "||-72M",
                             "lte": degree_start_date.encode('utf8') + "||+12M",
                          }
                       }
                    })

              all_degree_query.append(degree_query)

              considered_degrees.append(index)


    # if degree array does not exist, then coose major names and start date only
    elif target[1] == [None] or (not target[1]):

       print "NOT A PERFECT PROFILE" # because it does not have degree mentioned

       # check if major exists for corresponding degree

       if target[9] and target[9] != [None] and target[9] != '':

          for index, major in enumerate(target[9]):

             if major != None and major != '':

                 degree_query = {
                    "bool": {
                       "must": [
                          {
                             "match": {
                                "linkedin.educations.major.name": major.encode('utf8')
                             }
                          }
                       ]
                    }
                  }

                 # check if start_date exists for corresponding degree

                 if target[3] and target[3][index] != None and target[3]['index'] != '':

                    degree_start_date = target[3][index]
                    degree_query['bool']['must'].append({
                          "range": {
                             "linkedin.educations.start_date": {
                                "gte": degree_start_date.encode('utf8') + "||-72M",
                                "lte": degree_start_date.encode('utf8') + "||+12M",
                             }
                          }
                       })

                 all_degree_query.append(degree_query)

                 considered_degrees.append(index)

       # if degree_name and major_name does not exist, then select start_date only

       elif (target[9] == [None] or (not target[9]) or target[9] == ['']) and (target[3] and target[3] != [None] and target[3] != ['']):

          for index, degree_start_date in enumerate(target[3]):

             if degree_start_date != None and degree_start_date != '':

                 degree_query = {
                    "bool": {
                       "must": [
                          {
                             "range": {
                                "linkedin.educations.start_date": {
                                   "gte": degree_start_date.encode('utf8') + "||-72M",
                                   "lte": degree_start_date.encode('utf8') + "||+12M",
                                }
                             }
                          }
                       ]
                    }
                  }

                 all_degree_query.append(degree_query)

                 considered_degrees.append(index)



    # create filters for degree with major if exists with specific weights

    if considered_degrees:
        each_degree_weight = 28/float(len(considered_degrees))

        if target[1] and target[1] != [None]:

           for index, input_degree in enumerate(target[1]):

              if input_degree != None and input_degree != '':

                  filter_deg_query = {
                      "filter":{
                         "nested": {
                            "path": "linkedin.educations",
                            "query": {
                               "bool": {
                                 "must": [
                                    {
                                       "match": {
                                          "linkedin.educations.degree.name": input_degree.encode('utf8')
                                       }
                                    }
                                 ]
                              }
                            }
                         }
                      },
                      "weight": each_degree_weight
                   }


                  if target[9] and target[9][index] != None and target[9][index] != '':

                     major = target[9][index]
                     filter_deg_query['filter']['nested']['query']['bool']['must'].append({
                        "match": {
                           "linkedin.educations.major.name": major.encode('utf8')
                        }
                     })

                  filters.append(filter_deg_query)
                  degree_filter_only.append(filter_deg_query)
                  considered_filter_degree.append(index)

        # create filter for major, if degree does not exist with specific weights

        elif target[1] == [None] or (not target[1]):

           if target[9] and target[9] != [None] and target[9] != ['']:
              each_degree_weight = 28/float(len(target[9]))

              for index, major in enumerate(target[9]):

                  if major != None and major != '':

                      filter_deg_query = {
                          "filter":{
                             "nested": {
                                "path": "linkedin.educations",
                                "query": {
                                   "bool": {
                                     "must": [
                                        {
                                           "match": {
                                              "linkedin.educations.major.name": major.encode('utf8')
                                           }
                                        }
                                     ]
                                  }
                                }
                             }
                          },
                          "weight": each_degree_weight
                       }

                      filters.append(filter_deg_query)
                      degree_filter_only.append(filter_deg_query)
                      considered_filter_degree.append(index)


    # create filter for colleges for consisdered degrees only, (i.e ignoring school names) with specific weights

    if considered_degrees:
       each_college_weight = 12/float(len(considered_degrees))
       for index in considered_degrees:

          if target[2] and target[2][index] != None and target[2][index] != '' :

             input_college = target[2][index]

             filter_college = {
                 "filter":{
                    "nested": {
                       "path": "linkedin.educations",
                       "query": {
                          "term": {
                             "linkedin.educations.institute.name.raw": {
                                "value": input_college.encode('utf8')
                             }
                          }
                       }
                    }
                 },
                "weight": each_college_weight
              }

             filters.append(filter_college)

    # if we did not catch considered degrees, then grab all the institue names as college degrees and create their filters with specific weights

    elif target[2] and target[2] != [None]:
       each_college_weight = 12/float(len(target[2]))
       for index, college_name in enumerate(target[2]):

          input_college = target[2][index]

          if input_college != None and input_college != '':

              filter_college = {
                  "filter":{
                     "nested": {
                        "path": "linkedin.educations",
                        "query": {
                           "term": {
                              "linkedin.educations.institute.name.raw": {
                                 "value": input_college.encode('utf8')
                              }
                           }
                        }
                     }
                  },
                 "weight": each_college_weight
               }

              filters.append(filter_college)



    # create filter for college start date using degree name, major name and start date if exists
    # created each filter covering all the time duration individually, as there was no other means to ...
    # do it except in script by looping in education of each profile which would be very time consuming

    if degree_filter_only:
        degree_filter_only_length = len(degree_filter_only)
        for index, fil_deg in enumerate(degree_filter_only):

            fil_index = considered_filter_degree[index]

            if target[3][fil_index] != None and target[3][fil_index] != '':

                degree_start_date = target[3][fil_index]

                filter_college_start_date = copy.deepcopy(fil_deg)

                filter_college_start_date['filter']['nested']['query']['bool']['must'].append(
                  {
                     "range": {
                        "linkedin.educations.start_date": {
                           "gte": degree_start_date.encode('utf8'),
                           "lte": degree_start_date.encode('utf8') + "||+11M"
                        }
                     }
                  })


                filter_college_start_date['weight'] = 16*0.2/float(degree_filter_only_length)

                filters.append(filter_college_start_date)


                filter_college_start_date1 = copy.deepcopy(fil_deg)
                filter_college_start_date1['filter']['nested']['query']['bool']['must'].append(
                    {
                     "range": {
                         "linkedin.educations.start_date": {
                            "gte": degree_start_date.encode('utf8') + "||-12M",
                            "lte": degree_start_date.encode('utf8') + "||-1M"
                         }
                      }
                    })

                filter_college_start_date1['weight'] = 16*0.6/degree_filter_only_length

                filters.append(filter_college_start_date1)


                filter_college_start_date2 = copy.deepcopy(fil_deg)
                filter_college_start_date2['filter']['nested']['query']['bool']['must'].append(
                    {
                     "range": {
                         "linkedin.educations.start_date": {
                            "gte": degree_start_date.encode('utf8') + "||-24M",
                            "lte": degree_start_date.encode('utf8') + "||-13M"
                         }
                      }
                    })

                filter_college_start_date2['weight'] = 16/degree_filter_only_length


                filters.append(filter_college_start_date2)


                filter_college_start_date3 = copy.deepcopy(fil_deg)
                filter_college_start_date3['filter']['nested']['query']['bool']['must'].append(
                    {
                     "range": {
                         "linkedin.educations.start_date": {
                            "gte": degree_start_date.encode('utf8') + "||-36M",
                            "lte": degree_start_date.encode('utf8') + "||-25M"
                         }
                      }
                    })

                filter_college_start_date3['weight'] = 16*0.8/degree_filter_only_length

                filters.append(filter_college_start_date3)


                filter_college_start_date4 = copy.deepcopy(fil_deg)
                filter_college_start_date4['filter']['nested']['query']['bool']['must'].append(
                    {
                     "range": {
                         "linkedin.educations.start_date": {
                            "gte": degree_start_date.encode('utf8') + "||-48M",
                            "lte": degree_start_date.encode('utf8') + "||-37M"
                         }
                      }
                    })

                filter_college_start_date4['weight'] = 16*0.4/degree_filter_only_length

                filters.append(filter_college_start_date4)

                filter_college_start_date = []
                filter_college_start_date1 = []
                filter_college_start_date2 = []
                filter_college_start_date3 = []
                filter_college_start_date4 = []

















    # create filter for company names with specific weights

    company_length = 0
    if target[4] and target[4] != [None] and target[4] != '':

       company_length = len(target[4])
       for company_name in target[4]:

           if company_name != None and company_name != '':

               filter_company_name = {
                   "filter": {
                      "nested": {
                         "path": "linkedin.experiences",
                         "query": {
                            "match_phrase": {
                               "linkedin.experiences.company.name": company_name.encode('utf8')
                            }
                         }
                      }
                   },
                   "weight": 14/float(company_length)
                }

               filters.append(filter_company_name)

    # create filter for current location with specific weights

    curr_loc_name = target[8][0]
    if curr_loc_name != None and curr_loc_name != '':
        filter_curr_loc = {
        "filter": {
           "term": {
              "linkedin.current_location": curr_loc_name.encode('utf8')
           }
        },
        "weight": 4
        }

        filters.append(filter_curr_loc)




    # create AND component having skill set

    skill_query = ''
    if target[10] and target[10] != [None] and target[10] != ['']:
       min_match = 0.4*len(target[10])

       min_match = int(min_match)

       if min_match > 6:
           min_match = 6

       skill_query = {
          "terms": {
             "linkedin.skills.raw": target[10],"minimum_match": min_match
          }
       }


    curr_ind = target[11][0]
    if curr_ind != None and curr_ind != '':
        filter_curr_ind = {
            "filter": {
               "term": {
                  "linkedin.current_industry": curr_ind.encode('utf8')
               }
            },
            "weight": 10
            }

        filters.append(filter_curr_ind)


    es_input_query = {
      "bool": {
         "must": []
      }
    }

    # if degree AND components created then append in the es query
    if all_degree_query:

        es_input_query['bool']['must'].append(
            {
                "nested": {
                  "path": "linkedin.educations",
                  "query": {
                      "bool": {
                         "should": [
                         ]
                      }
                   }
               }
            }
        )

        for degree in all_degree_query:
            es_input_query['bool']['must'][0]['nested']['query']['bool']['should'].append(degree)

    # if skills_set AND component created above, then append in the es query
    if skill_query:
        es_input_query['bool']['must'].append(skill_query)


    # if degree and skills_set AND components created above, then only create filter having scripting score, ...
    # otherwise the script would run on huge number of records if any of the AND components does not exist
    if (all_degree_query) and (skill_query):

        if target[6] and target[6][-1] != None and target[6][-1] != '' and company_length != 0:
            first_job_start_date = target[6][-1]
            first_job_start_date = re.match(r'^(\d{4})\-',first_job_start_date,re.I)
            first_job_start_date = first_job_start_date.group(1)

            first_job_start_date = str(first_job_start_date)
            company_length = str(company_length)

            # inp = raw_input("script_score")

            filters.append(
                {
                   "script_score": {
                       "script" : "f_score=0;total=0;def date='';target_year=" + first_job_start_date + ";target_no_jobs=" + company_length + ";for(exp in _source.linkedin.experiences){total=total+1;if(exp.containsKey('start_date')){date=exp['start_date'];};};if(total<=target_no_jobs){return(-100);};if(date!=''){def year = date[0..3].toInteger();year = year.toInteger();def diff=year-target_year;if((-4<diff) && (diff<0)){f_score=Math.abs(diff)*3;};else{return(-100);};};def job_diff=Math.abs(target_no_jobs-total+1);job_score=target_no_jobs-job_diff;job_score=(job_score/target_no_jobs)*11;return(f_score+job_score);"

                      # "script" : "f_score=0;total=0;def date='';target_year=" + first_job_start_date + ";target_no_jobs=" + company_length + ";for(exp in _source.linkedin.experiences){total=total+1;if(exp.containsKey('start_date')){date=exp['start_date'];};};if(total<=target_no_jobs){return(-100)};match=date=~/^(?<year>\\d{4})\\-/;if(match){def year=match.group('year').toInteger();def diff=Math.abs(target_year-year);if(diff<5){f_score=5-diff;f_score=f_score*3;};if(f_score<0){f_score=0};};def job_diff=Math.abs(target_no_jobs-total+1);job_score=target_no_jobs-job_diff;job_score=(job_score/target_no_jobs)*11;if(job_score<0){job_score=0;};f_score=f_score+job_score;return(f_score);"
                       # "script": "f_score=0;total=0;def date='';target_year=" + first_job_start_date + ";target_no_jobs=" + company_length + ";for(exp in _source.linkedin.experiences){total=total+1;if(exp.containsKey('start_date')){date=exp['start_date'];};};match=date=~/^(?<year>\\d{4})\\-/;if(match){def year=match.group('year').toInteger();def diff=Math.abs(target_year-year);if(diff<5){f_score=5-diff;f_score=f_score*3;};if(f_score<0){f_score=0};};def job_diff=Math.abs(target_no_jobs-total+1);job_score=target_no_jobs-job_diff;job_score=(job_score/target_no_jobs)*11;if(job_score<0){job_score=0;};f_score=f_score+job_score;return(f_score);"
                   }
                }
            )

    # if degree and skills_set AND component are NOT created then simply match_all the es query with the assigned filters

    if (not all_degree_query) and (not skill_query):
        es_input_query = {
            "match_all": {}
        }

        print "NOT A PERFECT PROFILE"





    print "es_input_query : ", es_input_query
    print "\n"
    print "filters : ", filters
    print "\n"

    final_es_query = {
           "size": MAX_FETCH_PROFILE,
           "query": {
              "function_score": {
                 "query": es_input_query,
                 "functions": filters,
                 "score_mode": "sum",
                 "boost_mode": "replace"
              }
           }
        }

    print "final_es_query : ", final_es_query

    return final_es_query


def get_minimum_working_duration(currently_working_user_job_durations, current_job_duration_in_months):
    larger_durations = []
    for x in currently_working_user_job_durations:
        x = x/DAYS_IN_MONTH
        if x >= current_job_duration_in_months:
            larger_durations.append(x)
    if not len(larger_durations):
        return current_job_duration_in_months

    avg_min_duration = sum(larger_durations)/len(larger_durations)
    return avg_min_duration


def get_analysis_level_two(similar_user_job_durations, current_job_duration_in_months):
    print"***** Entering get_analysis_level_two *****"

    longer_job_durations = {}

    # for jd in similar_user_job_durations:
    #     if jd - current_job_duration_in_months > -3:
    #         longer_job_durations.append(jd)


    print "initial simi",similar_user_job_durations

    for index,similar_user_job_duration in enumerate(similar_user_job_durations):

        jd = similar_user_job_duration[0]/DAYS_IN_MONTH
        similar_user_job_durations[index] = jd
        if jd >= current_job_duration_in_months:
            score = similar_user_job_duration[1]

            if jd in longer_job_durations:
                longer_job_durations[jd] += score
            else:
                longer_job_durations[jd] = score

    print "similar user job durations in months", similar_user_job_durations
    print"Longer Job Durations ", longer_job_durations

    max_score = 0
    nearest_local_maxima = 0
    for ljd,l_score in longer_job_durations.iteritems():

        if l_score > max_score:
            max_score = l_score
            nearest_local_maxima = ljd


    # sorted_longer_job_durations_index = sorted(longer_job_durations, key=longer_job_durations.__getitem__,reverse=True)
    #
    #
    # probabilities = []
    # i = 0
    # max_probability = 0
    #
    # for key in sorted_longer_job_durations_index:
    #
    #     peak = key
    #
    #     # filtered_freqs = []
    #     total_people = 0
    #     filtered_people = 0
    #
    #     for new_key,new_value in longer_job_durations.iteritems():
    #
    #         if new_key <= peak:
    #             # filtered_freqs.append(freq)
    #             filtered_people += new_value
    #         total_people += new_value
    #
    #     probability = float(filtered_people)/total_people
    #
    #     probabilities.append((peak,probability))
    #
    #     if probability > max_probability:
    #         max_probability = probability
    #         nearest_local_maxima = peak
    #
    #     i += 1
    #     if i >= 5:
    #         break
    #
    # print "nearest local maxima", nearest_local_maxima
    # print "first five probabilities", probabilities


    # counter = collections.Counter(longer_job_durations)
    # print counter

    # nearest_local_maxima = 0
    #
    # max_score = 0
    # for longer_job_duration in longer_job_durations:
    #
    #     score = longer_job_duration[1]
    #
    #     if score > max_score:
    #         print "score", score
    #         max_score = score
    #         nearest_local_maxima = longer_job_duration[0]
    #         print "maxima", nearest_local_maxima


    print "\n\nmax score", max_score
    print "nearest local", nearest_local_maxima

    # print "Printing Top frequencies", counter.most_common()
    # for x in counter.most_common(len(counter)):
    #     abs_diff_from_current_jd = abs(x[0] - current_job_duration_in_months)
    #     inp = raw_input("paused")
    #     if abs_diff_from_current_jd < MONTHS_IN_YEAR:
    #         nearest_local_maxima = x[0]
    #         print "updated Local Maxima ", nearest_local_maxima
    #         break


    # if current_job_duration_in_months <= 24:

    # for x in counter.most_common(len(counter)):
    #     if x[0] >= current_job_duration_in_months:
    #         nearest_local_maxima = x[0]
    #         print "nearest local maxima", nearest_local_maxima
    #         break
    # else:
    #
    #     probabilities = []
    #     i = 0
    #     max_probability = 0
    #
    #     for x in counter.most_common(len(counter)):
    #
    #         peak = x[0]
    #
    #         # filtered_freqs = []
    #         total_people = 0
    #         filtered_people = 0
    #
    #         for freq in counter.most_common(len(counter)):
    #
    #             if freq[0] <= peak:
    #                 # filtered_freqs.append(freq)
    #                 filtered_people += freq[1]
    #             total_people += freq[1]
    #
    #         probability = float(filtered_people)/total_people
    #
    #         probabilities.append((peak,probability))
    #
    #         if probability > max_probability:
    #             max_probability = probability
    #             nearest_local_maxima = peak
    #
    #         i += 1
    #         if i >= 5:
    #             break
    #
    #     print "nearest local maxima", nearest_local_maxima
    #     print "first five probabilities", probabilities

    # print"Nearest Local Maxima ", nearest_local_maxima
    if nearest_local_maxima == 0:
        print "in nearest_local_maxima == 0 cond"
        print"Nearest Local Maxima ", ((current_job_duration_in_months / MONTHS_IN_YEAR) + 1) * MONTHS_IN_YEAR
        return ((current_job_duration_in_months / MONTHS_IN_YEAR) + 1) * MONTHS_IN_YEAR

    # if nearest_local_maxima < current_job_duration_in_months:
    #     print "in nearest_local_maxima < current_job_duration_in_months cond"
    #     # if the nearest local maxima found is less than current job duration
    #     # then use 25:75 to reply activeness
    #     if current_job_duration_in_months - nearest_local_maxima > MONTHS_IN_QUARTER_YEAR:
    #         print "1st cond Nearest Local Maxima ",((current_job_duration_in_months / MONTHS_IN_YEAR) + 1) * MONTHS_IN_YEAR
    #         return ((current_job_duration_in_months / MONTHS_IN_YEAR) + 1) * MONTHS_IN_YEAR
    #     else:
    #         print "2nd cond Nearest Local Maxima ",current_job_duration_in_months
    #         return current_job_duration_in_months

    return nearest_local_maxima









if __name__ == '__main__':
    print "In main"

    # fetch_target_query = {
    #     "query": {
    #         "match_all": {}
    #     }
    # }
    #55028a30650b84a55fbfa289,5501d54e650b84a55faa4c12,550035e8650b84a55f5edcb9

    # fetch_target_query = {
    #     "query": {
    #         "term": {
    #            "_id": {
    #               "value": "5501d461650b84a55faa102b"
    #            }
    #         }
    #     }
    # }


    # print "fetching 20000 input profiles"

    args_array = sys.argv

    # print "here", args_array
    #
    # if len(args_array) > 1:
    #     from_num = args_array[1]
    #     from_num = int(from_num)
    #     fetch_target_query["from"] = from_num
    #
    # total_fetched = 0
    #
    # try:
    #     resp = prod_es.search(index=es_index, doc_type=es_type, body=fetch_target_query, request_timeout=200000, search_type="scan", scroll=SCROLL_TIMEOUT_SECS, size=SCROLL_SIZE)
    #
    #     while True:
    #         resp = prod_es.scroll(resp['_scroll_id'], scroll=SCROLL_TIMEOUT_SECS)
    #         if len(resp['hits']['hits']) == 0 or total_fetched >=5000:
    #             break
    #         else:
    #             es_resps = resp['hits']['hits']
    #             profiles = []
    #             profiles = es_resps
    #             profile_count = (total_fetched + 1)
    #             total_fetched += len(profiles)
    #             print "total profiles fetched : ",total_fetched
    #
    #             f = open('modified_test_jsp_exp_plus_1.csv','a')
    #             reg_inp = open('regression.csv','a')
    #             reg_out = open('regression_output.csv','a')
    #             reg_out_curr_job_dur = open('regression_output_current_job_dur.csv','a')
    #             reg_out_total_job_dur = open('regression_output_total_job_dur.csv','a')
    #             reg_output_candidate_job_durs = open('regression_output_candidate_job_durs.csv','a')
    #
    #             for profile in profiles:
    #
    #                 print profile['_id']
    #
    #                 print "profile no", profile_count
    #
    #                 try:
    #                     (target) = create_target_array(profile)
    #                     print "actual_target",target
    #                 except Exception as e:
    #                     print e
    #                     print "ERROR in create_target_array"
    #
    #                 # components of target array = ['id', [degree], [colleges], [college_start_dates], [company names], [company profile title], [company_start_date], [company_end_date], [current_location], [major], [skills_set]]
    #
    #                 print "og  profile",profile
    #
    #                 output = None
    #
    #                 try:
    #                     output = go_to_past(profile)
    #                 except Exception as e:
    #                     print e
    #                     print "ERROR in go_to_past"
    #
    #                 print "output",output
    #
    #                 print "\n\n\n\n-------------------------------------------------------------------------\n\n\n\n"
    #
    #                 profile_count = profile_count + 1
    #
    #             f.close()
    # #
    # #
    # #
    # except Exception as e:
    #     print Exception, e

    # ids = ['5500064c650b84a55f573ac0','5500064d650b84a55f573af4','550035e1650b84a55f5edb03','550286a3650b84a55fbf1399','550286a3650b84a55fbf139a','550286a3650b84a55fbf13b8','550286a4650b84a55fbf13c8','550286a4650b84a55fbf13ef','5501253d650b84a55f835ea7','55012b17650b84a55f8457ac','5500064e650b84a55f573b24','5500064f650b84a55f573b43','550286a5650b84a55fbf13fb','55012541650b84a55f835eee','55012b19650b84a55f845818','5501cb90650b84a55fa7a2e1','5501cb91650b84a55fa7a2fe','55000650650b84a55f573b84','550286a7650b84a55fbf146a','550286a9650b84a55fbf14c2','549b2765b3ec42333ba71314','549b2765b3ec42333ba7131d','5501cb92650b84a55fa7a329','5501d466650b84a55faa1186','550035e4650b84a55f5edbca','550035e5650b84a55f5edc0f','550286ab650b84a55fbf1536','549b2766b3ec42333ba71322','549b2766b3ec42333ba7132b','55012b1e650b84a55f84592f','55012b1e650b84a55f84593e','5501cb93650b84a55fa7a397','550286ac650b84a55fbf1555','55028ade650b84a55fbfc24e']

    # ids = ['5501d461650b84a55faa1013','5501d461650b84a55faa102b','5500064c650b84a55f573ab9','5500064d650b84a55f573adf','5500064d650b84a55f573aee','550035e0650b84a55f5edaa8','550035e0650b84a55f5edad5','5500064f650b84a55f573b2e','550286a7650b84a55fbf1461','549b2763b3ec42333ba712d4','5501d464650b84a55faa10de','550035e3650b84a55f5edb89','549b2765163659371fca8813','55012543650b84a55f835f1f','55012544650b84a55f835f31','55000653650b84a55f573bf1','55000653650b84a55f573c0f','550035e4650b84a55f5edbd8','550035e5650b84a55f5edc24','55000718650b84a55f575ff9','550035e8650b84a55f5edc83','55012b1f650b84a55f84597d','5501cb94650b84a55fa7a3fc','5501d469650b84a55faa124f','55012b23650b84a55f845a18','5501cb96650b84a55fa7a49c','5500071b650b84a55f5760a1','55012551650b84a55f8360d7','55012553650b84a55f836111','5500071e650b84a55f576115','550036b4650b84a55f5f067f','5501cb99650b84a55fa7a58b','5501d46e650b84a55faa139d','5501d46f650b84a55faa13c1','5500071f650b84a55f576152','55000720650b84a55f57618d','5500064c650b84a55f573ac0','5500064d650b84a55f573af4','550286a3650b84a55fbf1391','550286a3650b84a55fbf1399','550286a3650b84a55fbf13b8','550286a4650b84a55fbf13ef','549b2761b3ec42333ba7128b','5500064e650b84a55f573b24','5500064f650b84a55f573b43','550286a5650b84a55fbf13fb','550286a7650b84a55fbf1469','5501cb91650b84a55fa7a2fe','5501d464650b84a55faa10f3','5501d465650b84a55faa1134','55000650650b84a55f573b84','550286a7650b84a55fbf146a','550286a9650b84a55fbf14c2','5501d466650b84a55faa1186','550035e4650b84a55f5edbca','550286a9650b84a55fbf14da','550286aa650b84a55fbf151c','550286ab650b84a55fbf1536','549b2766b3ec42333ba7132b','55012549650b84a55f835ff1','55012b1e650b84a55f84593e','5501d469650b84a55faa1219','550286ac650b84a55fbf1555','55028ade650b84a55fbfc24e','549b2767972b9a338dee6399','549b2767b3ec42333ba71356','55012b1f650b84a55f845983','5501d469650b84a55faa1255','550035e8650b84a55f5edcb9','55028adf650b84a55fbfc26d','549b2767b3ec42333ba71365','550035e1650b84a55f5edaff','5501253d650b84a55f835eb6','550286a7650b84a55fbf1487','549b2765163659371fca87f7','5501d469650b84a55faa1246','55028ae0650b84a55fbfc2a6','550036b2650b84a55f5f0607','5501d46e650b84a55faa13a4','550036ba650b84a55f5f0761','55012b2d650b84a55f845c19','550036bb650b84a55f5f07a1','55028aeb650b84a55fbfc47d','549b2733163659371fca826a','550036c3650b84a55f5f08c4','5501cba1650b84a55fa7a7c3','5501cba3650b84a55fa7a82c','5501cba4650b84a55fa7a882','5501d614650b84a55faa7141','55028b01650b84a55fbfc686','5501d619650b84a55faa71d4','55028a2e650b84a55fbfa1fb','5501d61c650b84a55faa721d','550005a5650b84a55f571c7f','55012492650b84a55f833e49','55012493650b84a55f833e5a','5501d53f650b84a55faa48f8','5501d53f650b84a55faa4923','550036d8650b84a55f5f0b92','550005aa650b84a55f571d97','5501cbae650b84a55fa7abbe','55028a3d650b84a55fbfa4f5','549b2745972b9a338dee6081','55012bb1650b84a55f847257','5500365c650b84a55f5ef4c5','550005b4650b84a55f571f44','550124a5650b84a55f834187','550035e0650b84a55f5edab7','550035e1650b84a55f5edae7','55012b18650b84a55f8457e2','55012b1c650b84a55f845892','55000653650b84a55f573bdc','550286aa650b84a55fbf1522','549b2766b3ec42333ba71331','55012548650b84a55f835fc7','549b2766b3ec42333ba71348','5500071a650b84a55f576085','5501254e650b84a55f83607a','5501254f650b84a55f836089','5501d46c650b84a55faa12ce','55028ae0650b84a55fbfc2cd','549b2769163659371fca886b','549b276ab3ec42333ba713f3','55012553650b84a55f836120','55012555650b84a55f836171','5501cb99650b84a55fa7a56d','550036b5650b84a55f5f06a7','550036b6650b84a55f5f06c8','55028ae6650b84a55fbfc3b4','55012b29650b84a55f845b68','549b2731163659371fca81fd','55012b2d650b84a55f845c37','550036be650b84a55f5f07e6','55028aef650b84a55fbfc4f9','5501cb9e650b84a55fa7a6e6','5501d474650b84a55faa1512','55028af1650b84a55fbfc544','549b2736163659371fca829c','5501d611650b84a55faa70e4','550036c7650b84a55f5f0946','550036c8650b84a55f5f0985','5501d616650b84a55faa7174','5501d616650b84a55faa7183']

    # ids = ['5501d474650b84a55faa1512']

    # ids = ['5501d461650b84a55faa1013','5501d461650b84a55faa102b','5500064c650b84a55f573ab9','5500064c650b84a55f573ac0','5500064d650b84a55f573adf','5500064d650b84a55f573aee','5500064d650b84a55f573af4','550035e0650b84a55f5edaa8','550035e0650b84a55f5edab7','550035e0650b84a55f5edad5','550035e1650b84a55f5edae7','550035e1650b84a55f5edaff','550286a3650b84a55fbf1391','550286a3650b84a55fbf1399','550286a3650b84a55fbf13b8','550286a4650b84a55fbf13ef','549b2761b3ec42333ba7128b','5501253d650b84a55f835eb6','55012b18650b84a55f8457e2','5500064e650b84a55f573b24','5500064f650b84a55f573b2e','5500064f650b84a55f573b43','550286a5650b84a55fbf13fb','550286a7650b84a55fbf1461','550286a7650b84a55fbf1469','549b2763b3ec42333ba712d4','5501cb91650b84a55fa7a2fe','5501d464650b84a55faa10de','5501d464650b84a55faa10f3','5501d465650b84a55faa1134','55000650650b84a55f573b84','550035e3650b84a55f5edb89','550286a7650b84a55fbf146a','550286a7650b84a55fbf1487','550286a9650b84a55fbf14c2','549b2765163659371fca87f7','549b2765163659371fca8813','55012543650b84a55f835f1f','55012544650b84a55f835f31','55012b1c650b84a55f845892','5501d466650b84a55faa1186','55000653650b84a55f573bdc','55000653650b84a55f573bf1','55000653650b84a55f573c0f','550035e4650b84a55f5edbca','550035e4650b84a55f5edbd8','550035e5650b84a55f5edc24','550286a9650b84a55fbf14da','550286aa650b84a55fbf151c','550286aa650b84a55fbf1522','550286ab650b84a55fbf1536','549b2766b3ec42333ba7132b','549b2766b3ec42333ba71331','55012548650b84a55f835fc7','55012549650b84a55f835ff1','55012b1e650b84a55f84593e','5501d469650b84a55faa1219','55000718650b84a55f575ff9','550035e8650b84a55f5edc83','550286ac650b84a55fbf1555','55028ade650b84a55fbfc24e','549b2766b3ec42333ba71348','549b2767972b9a338dee6399','549b2767b3ec42333ba71356','55012b1f650b84a55f84597d','55012b1f650b84a55f845983','5501cb94650b84a55fa7a3fc','5501d469650b84a55faa1246','5501d469650b84a55faa124f','5501d469650b84a55faa1255','5500071a650b84a55f576085','550035e8650b84a55f5edcb9','55028adf650b84a55fbfc26d','55028ae0650b84a55fbfc2a6','549b2767b3ec42333ba71365','5501254b650b84a55f836050','5501254e650b84a55f83607a','5501254f650b84a55f836089','55012b23650b84a55f845a18','55012b23650b84a55f845a45','5501cb96650b84a55fa7a49c','5501d46c650b84a55faa12ce','5500071b650b84a55f57609a','5500071b650b84a55f5760a1','550035eb650b84a55f5edd00','550036b2650b84a55f5f0607','550036b3650b84a55f5f0637','55028ae0650b84a55fbfc2cd','549b2769163659371fca886b','55012551650b84a55f8360d7','55012552650b84a55f8360ee','55012552650b84a55f836101','55012553650b84a55f836111','55012b25650b84a55f845a92','55012b25650b84a55f845ab1','5501cb98650b84a55fa7a527','5501d46d650b84a55faa132d','5501d46e650b84a55faa1349','5500071e650b84a55f57610f','5500071e650b84a55f576115','5500071e650b84a55f576124','550036b4650b84a55f5f067f','550036b4650b84a55f5f0685','55028ae3650b84a55fbfc330','549b276ab3ec42333ba713ed','549b276ab3ec42333ba713f3','55012553650b84a55f836120','55012555650b84a55f836171','5501cb99650b84a55fa7a56d','5501cb99650b84a55fa7a58b','5501d46e650b84a55faa139d','5501d46e650b84a55faa13a4','5501d46f650b84a55faa13b2','5501d46f650b84a55faa13c1','5500071f650b84a55f576152','55000720650b84a55f57618d','550036b5650b84a55f5f06a7','550036b5650b84a55f5f06b6','550036b6650b84a55f5f06c0','550036b6650b84a55f5f06c8','550036b6650b84a55f5f06d7','550036b7650b84a55f5f0705','55028ae5650b84a55fbfc381','55028ae5650b84a55fbfc3a0','55028ae6650b84a55fbfc3b4','549b276cb3ec42333ba713f9','55012555650b84a55f836188','55012556650b84a55f8361a4','55012b29650b84a55f845b5a','55012b29650b84a55f845b68','55012b2a650b84a55f845b86','55012b2a650b84a55f845bae','5501d46f650b84a55faa1403','55000722650b84a55f5761aa','550036b7650b84a55f5f0714','550036ba650b84a55f5f0761','55028ae7650b84a55fbfc3f1','55028ae7650b84a55fbfc406','55028ae8650b84a55fbfc414','55028ae9650b84a55fbfc437','55028aea650b84a55fbfc455','549b2730163659371fca819b','55012557650b84a55f8361db','55012558650b84a55f836220','55012b2d650b84a55f845c19','5501cb9c650b84a55fa7a661','55000726650b84a55f576249','550036bb650b84a55f5f07a1','550036bc650b84a55f5f07ad','55028aeb650b84a55fbfc47d','55028aeb650b84a55fbfc483','55028aeb650b84a55fbfc48c','55028aec650b84a55fbfc497','55028aec650b84a55fbfc4a7','549b2730b3ec42333ba70e75','549b2731163659371fca81df','549b2731163659371fca81fd','55012b2d650b84a55f845c37','5501d472650b84a55faa14c3','5501d473650b84a55faa14e8','5501d474650b84a55faa150c','550036bd650b84a55f5f07ce','550036bd650b84a55f5f07d4','550036be650b84a55f5f07e6','550036bf650b84a55f5f081d','55028aee650b84a55fbfc4e9','55028aee650b84a55fbfc4f0','55028aef650b84a55fbfc4f9','55028aef650b84a55fbfc4fa','55028af0650b84a55fbfc529','55028af0650b84a55fbfc530','549b2733163659371fca826a','5501255c650b84a55f8362eb','55012b31650b84a55f845cce','5501cb9e650b84a55fa7a6e6','5501d474650b84a55faa1512','5501d476650b84a55faa155d','55028af1650b84a55fbfc544','55028afd650b84a55fbfc561','549b2736163659371fca829c','5501255d650b84a55f836325','5501255e650b84a55f836357','5501cba1650b84a55fa7a7a5','5501d610650b84a55faa70a9','5501d610650b84a55faa70b8','550036c3650b84a55f5f08c4','550036c6650b84a55f5f0916','55028afe650b84a55fbfc593','55028afe650b84a55fbfc5d0','549b2737972b9a338dee5e7e','5501255f650b84a55f83637f','5501255f650b84a55f8363ad','5501255f650b84a55f8363b3','55012560650b84a55f8363b8','55012560650b84a55f8363d6','55012b34650b84a55f845d62','55012b34650b84a55f845d71','5501cba1650b84a55fa7a7c3','5501d611650b84a55faa70d5','5501d611650b84a55faa70e4','5501d613650b84a55faa711e','5501d614650b84a55faa712c','5501d614650b84a55faa713b','5500072d650b84a55f5763f6','5500072d650b84a55f576403','5500072d650b84a55f576429','550036c7650b84a55f5f0946','550036c8650b84a55f5f0985','55028aff650b84a55fbfc61c','55028b00650b84a55fbfc62c','55028b01650b84a55fbfc651','549b2738163659371fca82c7','55012b9a650b84a55f846eeb','5501cba3650b84a55fa7a82c','5501cba4650b84a55fa7a882','5501d614650b84a55faa7141','5501d614650b84a55faa7149','5501d616650b84a55faa7174','5501d616650b84a55faa717d','5501d616650b84a55faa7183','5501d616650b84a55faa7192','550036cd650b84a55f5f0a14','55028b01650b84a55fbfc659','55028b01650b84a55fbfc660','55028b01650b84a55fbfc686','549b2739163659371fca82f3','55012562650b84a55f83645b','55012563650b84a55f836495','55012b9f650b84a55f846f7b','5501cba5650b84a55fa7a8e6','5501d617650b84a55faa71b0','5501d619650b84a55faa71d4','5500072f650b84a55f5764d7','55000730650b84a55f57652c','55028b02650b84a55fbfc6ba','55028b02650b84a55fbfc6c0','55028a2e650b84a55fbfa1f2','55028a2e650b84a55fbfa1fb','55028a2f650b84a55fbfa22b','55012ba1650b84a55f846fe5','5501d61a650b84a55faa71fb','5501d61c650b84a55faa721d','5501d61c650b84a55faa7223','5501d61e650b84a55faa7238','550036cf650b84a55f5f0a9b','550036cf650b84a55f5f0aab','55028a2f650b84a55fbfa23a','55028a2f650b84a55fbfa240','55028a2f650b84a55fbfa248','55028a30650b84a55fbfa289','55028a30650b84a55fbfa298','549b273b972b9a338dee5f53','55012ba1650b84a55f846ffd','55012ba2650b84a55f847015','55012ba4650b84a55f84703a','5501cba7650b84a55fa7a96b','5501d61f650b84a55faa727c','5501d622650b84a55faa72a4','55000732650b84a55f5765bc','55000733650b84a55f5765e5','550005a5650b84a55f571c7f','55028a31650b84a55fbfa2ce','55028a32650b84a55fbfa2d5','55028a32650b84a55fbfa2de','55028a33650b84a55fbfa30e','55028a33650b84a55fbfa314','549b273cb3ec42333ba70ef3','55012492650b84a55f833e49','55012493650b84a55f833e5a','55012493650b84a55f833e95','55012ba6650b84a55f84705e','55012ba7650b84a55f84707f','5501d625650b84a55faa72fd','5501d626650b84a55faa7300','5501d626650b84a55faa7317','5501d626650b84a55faa7326','5501d627650b84a55faa7343','550005a6650b84a55f571c91','550005a6650b84a55f571cb8','550036d6650b84a55f5f0b58','55028a34650b84a55fbfa35a','55028a35650b84a55fbfa365','549b273d972b9a338dee5f95','549b273e972b9a338dee5fad','55012494650b84a55f833eb9','55012495650b84a55f833ed8','55012495650b84a55f833f03','55012ba8650b84a55f8470c5','55012ba8650b84a55f8470ce','5501cbab650b84a55fa7aa76','5501cbab650b84a55fa7aa8e','5501d53f650b84a55faa48f8','5501d53f650b84a55faa490e','5501d53f650b84a55faa4923','550005a8650b84a55f571cf2','550005a8650b84a55f571d07','550005a9650b84a55f571d51','550036d8650b84a55f5f0b92','55028a36650b84a55fbfa39c','55028a38650b84a55fbfa3ce','55028a38650b84a55fbfa3e3','549b273eb3ec42333ba70f2d','55012496650b84a55f833f26','55012496650b84a55f833f35','5501cbac650b84a55fa7aaaa','5501d542650b84a55faa4997','5501d542650b84a55faa49a7','5501d542650b84a55faa49bf','550005aa650b84a55f571d71','550005aa650b84a55f571d97','55003657650b84a55f5ef385','55003657650b84a55f5ef38e','55028a39650b84a55fbfa426','55028a3a650b84a55fbfa435','55028a3a650b84a55fbfa444','549b2740b3ec42333ba70f46','5501249a650b84a55f833fa4','5501249a650b84a55f833fcb','55012bad650b84a55f8471af','55012bad650b84a55f8471b5','5501cbac650b84a55fa7ab18','5501d544650b84a55faa4a06','5501d545650b84a55faa4a23','5501d545650b84a55faa4a32','550005ac650b84a55f571e03','550005ad650b84a55f571e39','55003658650b84a55f5ef3ec','55028a3b650b84a55fbfa485','55028a3b650b84a55fbfa48e','549b2742972b9a338dee6040','5501249e650b84a55f83402d','5501249e650b84a55f83403c','5501249e650b84a55f834042','55012bad650b84a55f8471dc','55012bae650b84a55f847201','55012baf650b84a55f847222','5501cbae650b84a55fa7abbe','5501d546650b84a55faa4a57','550005ad650b84a55f571e40','550005ad650b84a55f571e66','550005ae650b84a55f571e8a','5500365a650b84a55f5ef455','55028a3d650b84a55fbfa4f5','55028a3d650b84a55fbfa51a','549b2745972b9a338dee6081','5501249e650b84a55f83405a','5501249e650b84a55f834068','5501249f650b84a55f834078','550124a0650b84a55f834092','55012baf650b84a55f847231','55012bb1650b84a55f847257','5501cbae650b84a55fa7abd3','5501cbaf650b84a55fa7ac1f','5501d54a650b84a55faa4b2f','5500365c650b84a55f5ef4c5','5500365c650b84a55f5ef4dd','5500365d650b84a55f5ef519','55028a3e650b84a55fbfa52a','55028a3f650b84a55fbfa56b','55028a3f650b84a55fbfa571','55028a3f650b84a55fbfa580','55028a40650b84a55fbfa594','549b2745972b9a338dee60a8','549b2584b3ec4232b02037a9','549b2586972b9a3303bc83c4','550124a1650b84a55f8340a3','550124a1650b84a55f8340bb','55012bb4650b84a55f8472d5','55012bb5650b84a55f84730c','5501cbb0650b84a55fa7ac66','5501d54a650b84a55faa4b44','5501d54b650b84a55faa4b70','550005b2650b84a55f571f22','550005b4650b84a55f571f44','550005b4650b84a55f571f53','5500365d650b84a55f5ef54f','55028a41650b84a55fbfa5b4','55028a41650b84a55fbfa5bd','55028a41650b84a55fbfa5cc','55028a41650b84a55fbfa5e9','550124a3650b84a55f834118','550124a4650b84a55f834142','550124a4650b84a55f83414b','55012bb7650b84a55f84734e','5501d54b650b84a55faa4ba6','5501d54c650b84a55faa4bb4','550005b7650b84a55f571f9f','550005b8650b84a55f571fbb','5500365f650b84a55f5ef5bb','55028a42650b84a55fbfa5f7','55028a42650b84a55fbfa604','55028a6b650b84a55fbfae65','55028a6d650b84a55fbfae96','55028a6d650b84a55fbfae9f','549b258916365936936ba168','550124a5650b84a55f834187','550124a6650b84a55f8341db','550124a6650b84a55f8341e1','55012bb8650b84a55f847384','55012bb8650b84a55f84739c','55012bb9650b84a55f84739f','5501cbb2650b84a55fa7ad5d','5501d54e650b84a55faa4c12','550005ba650b84a55f571ff7','550005bb650b84a55f572016','55003660650b84a55f5ef640','55028a6e650b84a55fbfaef2','55028a6e650b84a55fbfaefb','55028a6f650b84a55fbfaf04','55028a6f650b84a55fbfaf0d','549b258b16365936936ba19d','550124a6650b84a55f8341f0','550124a7650b84a55f834202','550124a7650b84a55f83420b','550124a8650b84a55f834212','550124a9650b84a55f834226','550124a9650b84a55f83422f','55012bba650b84a55f8473e2','55012bbc650b84a55f84743e','5501cbb3650b84a55fa7ad7a','5501cbb3650b84a55fa7ada7','550005be650b84a55f572066','55003662650b84a55f5ef6a5','55003662650b84a55f5ef6c3','55028a6f650b84a55fbfaf22','55028a6f650b84a55fbfaf2b','55028a70650b84a55fbfaf4e','55028a70650b84a55fbfaf54','550124aa650b84a55f83426b','55012bbe650b84a55f847486','5501d55c650b84a55faa4cf6','5501d55e650b84a55faa4d3f','55003663650b84a55f5ef6de','55003663650b84a55f5ef6ed','55028a71650b84a55fbfafa9','55028a71650b84a55fbfafb0','55028a72650b84a55fbfafbe','549b258eb3ec4232b0203826','550124ab650b84a55f8342cf','550124ab650b84a55f8342e4','55012bc1650b84a55f847526','5501cbb5650b84a55fa7ae50','5501d55f650b84a55faa4d44','5501d55f650b84a55faa4d53','5501d628650b84a55faa7351','550005c1650b84a55f57210f','550005c2650b84a55f57211f','55003665650b84a55f5ef795','55003665650b84a55f5ef7bd','55028a73650b84a55fbfafec','55028a73650b84a55fbfaff2','55028a73650b84a55fbfb005','549b258fb3ec4232b0203843','550124ad650b84a55f834333','55012af7650b84a55f845245','55012af7650b84a55f84524e','5501cbb8650b84a55fa7af1f','5501d62a650b84a55faa7385','5501d62a650b84a55faa738e','5501d62b650b84a55faa73ac','5501d62c650b84a55faa73ba','5501d62d650b84a55faa73d6','550005c3650b84a55f57216f','550005c4650b84a55f57218c','550005c5650b84a55f5721ca','55003665650b84a55f5ef7d2','55003666650b84a55f5ef7e4','55003666650b84a55f5ef7ed','55003667650b84a55f5ef816','55028a74650b84a55fbfb020','55028a75650b84a55fbfb05d','55028a75650b84a55fbfb06c','55028a76650b84a55fbfb07c','549b259116365936936ba21c','549b2591972b9a3303bc854e','550124ae650b84a55f834394','550124af650b84a55f8343a9','550124af650b84a55f8343c7','55012af9650b84a55f8452b9','5501cbb8650b84a55fa7af4c','5501cbb9650b84a55fa7af71','5501d62d650b84a55faa73f4','5501d62e650b84a55faa7408','5501d62f650b84a55faa741f','550005c6650b84a55f5721f3','55003668650b84a55f5ef85e','55003668650b84a55f5ef864','55028a76650b84a55fbfb08b','55028a76650b84a55fbfb09a','549b2591b3ec4232b0203865','549b2592b3ec4232b0203873','550124fd650b84a55f8353ce','550124fe650b84a55f835413','55012afa650b84a55f8452eb','55012afb650b84a55f84532c','55012afb650b84a55f845341','5501d636650b84a55faa748f','5501d638650b84a55faa74a3','5500366a650b84a55f5ef89d','5500366a650b84a55f5ef8cb','55028a78650b84a55fbfb0dd','55028a79650b84a55fbfb0f7','55028a79650b84a55fbfb10d','55028a79650b84a55fbfb113','55028a7a650b84a55fbfb131','549b2594972b9a3303bc85ae','55012500650b84a55f835468','5501d63a650b84a55faa74c8','5501d63c650b84a55faa74d5','5501d63c650b84a55faa74e4','5501d63d650b84a55faa7507','550006a7650b84a55f574af3','550006a7650b84a55f574afc','550006a7650b84a55f574b00','550006a9650b84a55f574b2d','5500366c650b84a55f5ef94f','55028a7a650b84a55fbfb148','55028a7c650b84a55fbfb199','549b259516365936936ba280','549b2595972b9a3303bc85da','549b2595b3ec4232b0203896','55012502650b84a55f8354c9','55012503650b84a55f8354f7','55012afe650b84a55f8453ea','55012afe650b84a55f8453f0','5501cb7d650b84a55fa79cb9','5501d63e650b84a55faa753c','5501d63e650b84a55faa7542','5501d640650b84a55faa7591','550006aa650b84a55f574b86','5500366e650b84a55f5ef9a7','55003691650b84a55f5f0087','55028a7c650b84a55fbfb1a9','55028a7c650b84a55fbfb1b0','55028a7d650b84a55fbfb1d7','55028a7e650b84a55fbfb201','55028a7e650b84a55fbfb210','55012b00650b84a55f84544c','55012b00650b84a55f845452','5501d642650b84a55faa75b6','5501d4f0650b84a55faa3568','5501d4f0650b84a55faa359e','550006ab650b84a55f574bc8','550006ad650b84a55f574bed','55003692650b84a55f5f00e5','55028a7f650b84a55fbfb23c','55028a7f650b84a55fbfb242','55028a7f650b84a55fbfb24b','55028a42650b84a55fbfa613','55028a42650b84a55fbfa61c','55012b01650b84a55f845482','55012b02650b84a55f8454a4','55012b02650b84a55f8454c2','55012b03650b84a55f8454c9','5501d4f1650b84a55faa35da','5501d4f1650b84a55faa35f7','55028a42650b84a55fbfa622','55028a44650b84a55fbfa682','549b2599972b9a3303bc8644','5501250b650b84a55f835613','5501250b650b84a55f83561c','5501d4f3650b84a55faa3673','550006b2650b84a55f574cab','550006b2650b84a55f574cba','5500369a650b84a55f5f01a1','5500369a650b84a55f5f01b0','55028a45650b84a55fbfa6a6','55028a45650b84a55fbfa6cd','55028a45650b84a55fbfa6d3','549b2599b3ec4232b02038f5','5501250d650b84a55f83566b','5501250d650b84a55f835671','5501250e650b84a55f835690','5501cb80650b84a55fa79e48','5501d4f3650b84a55faa3699','5501d4f3650b84a55faa36a1','5501d4f3650b84a55faa36aa','5501d4f3650b84a55faa36b8','550006b4650b84a55f574cd5','550006b4650b84a55f574cf3','550006b5650b84a55f574d14','5500369b650b84a55f5f0213','55028a46650b84a55fbfa6ec','55028a46650b84a55fbfa6f2','55028a47650b84a55fbfa71c','55028a47650b84a55fbfa722','55028a48650b84a55fbfa741','549b259ab3ec4232b0203911','5501250f650b84a55f8356bd','55012511650b84a55f8356e7','55012512650b84a55f835708','55012512650b84a55f835717','55012b0a650b84a55f8455b3','55012b0b650b84a55f8455c9','55012b0b650b84a55f8455d0','55012b0c650b84a55f8455e2','5501cb81650b84a55fa79ea8','5501cb82650b84a55fa79eee','5500369e650b84a55f5f0290','5500369e650b84a55f5f02c6','55028a48650b84a55fbfa767','55028a49650b84a55fbfa77e','55028a49650b84a55fbfa784','55028a49650b84a55fbfa793','55028a4a650b84a55fbfa7a9','55028a4a650b84a55fbfa7b0','549b259b16365936936ba346','549b259c16365936936ba34e','55012513650b84a55f835727','55012517650b84a55f8357a2','5501d4f7650b84a55faa379d','550006b8650b84a55f574da1','550006b8650b84a55f574db0','550006b8650b84a55f574db8','550006b9650b84a55f574dd7','550036a0650b84a55f5f030a','550036a1650b84a55f5f034d','550036a1650b84a55f5f035c','55028a4c650b84a55fbfa809','55028a4c650b84a55fbfa818','549b259c972b9a3303bc86db','55012517650b84a55f8357b1','55012518650b84a55f8357ca','55012519650b84a55f835800','55012b12650b84a55f8456a9','55012b13650b84a55f8456c2','5501cb84650b84a55fa79f76','5501d4f7650b84a55faa37bc','550006bb650b84a55f574e16','550006bb650b84a55f574e1f','550006bc650b84a55f574e5c','550036a2650b84a55f5f0374','550036a3650b84a55f5f03a5','55028a4c650b84a55fbfa845','55028a4d650b84a55fbfa859','55028a4d650b84a55fbfa85a','55028a4d650b84a55fbfa886','55028a4e650b84a55fbfa896','55028a4e650b84a55fbfa8af','549b259db3ec4232b0203961','549b259e972b9a3303bc871b','55012b14650b84a55f84572a','5501d4f9650b84a55faa3855','5501d4fa650b84a55faa3873','5501d4fa650b84a55faa387c','550006be650b84a55f574ebd','550036a6650b84a55f5f03eb','550036a6650b84a55f5f03f1','550036a6650b84a55f5f041e','550036a7650b84a55f5f0427','55028a4e650b84a55fbfa8b5','55028a4f650b84a55fbfa8ca','55028a4f650b84a55fbfa8d0','55028a4f650b84a55fbfa8f6','549b259eb3ec4232b02039ae','5501251c650b84a55f8358bd','5501cb86650b84a55fa7a027','5501d4fa650b84a55faa3899','5501d4fb650b84a55faa38c6','550006bf650b84a55f574eef','5500055b650b84a55f570d48','550036a8650b84a55f5f047a','55028a50650b84a55fbfa92f','55028a50650b84a55fbfa94d','55028a51650b84a55fbfa95d','55028a52650b84a55fbfa986','549b2745b3ec42333ba70fd1','549b2746163659371fca8399','549b2746972b9a338dee60b6','5501251e650b84a55f835914','5501251e650b84a55f83592c','5501251e650b84a55f835932','55012a8a650b84a55f843fd3','55012a8b650b84a55f843ff4','55012a8b650b84a55f843ffd','5501cb87650b84a55fa7a082','5501cb88650b84a55fa7a0dc','5501d4fc650b84a55faa3943','550036a9650b84a55f5f04d7','55028a53650b84a55fbfa99d','55028a53650b84a55fbfa9a4','55028a53650b84a55fbfa9b3','55028a53650b84a55fbfa9bc','55028a54650b84a55fbfa9d2','55028a54650b84a55fbfa9db','55028a54650b84a55fbfa9e1','5501251f650b84a55f835973','55012520650b84a55f83599f','55012a8b650b84a55f844007','55012a8c650b84a55f844020','55012a8c650b84a55f844046','55012a8c650b84a55f844055','5501d4fd650b84a55faa3977','5500055f650b84a55f570e2e','550036aa650b84a55f5f051a','55028a55650b84a55fbfaa2a','55028a55650b84a55fbfaa47','549b2748972b9a338dee60ea','549b2749163659371fca8424','549b2749972b9a338dee60f7','549b2749972b9a338dee6104','55012521650b84a55f8359bf','55012565650b84a55f836512','55012a8d650b84a55f84407f','55012a8d650b84a55f84408e','55012a8e650b84a55f8440c5','55012a8e650b84a55f8440d4','5501cb8a650b84a55fa7a152','5501cb8a650b84a55fa7a161','5501cb8b650b84a55fa7a19c','5501d4fe650b84a55faa39cb','55000560650b84a55f570e6f','550036af650b84a55f5f058a','550036af650b84a55f5f0598','55028a56650b84a55fbfaa75','55028a56650b84a55fbfaa7e','55028a56650b84a55fbfaa84','55028a57650b84a55fbfaa98','55028a57650b84a55fbfaaa0','55028a58650b84a55fbfaac7','55028a58650b84a55fbfaad6','549b2749b3ec42333ba71006','549b274a163659371fca842d','55012565650b84a55f836529','55012565650b84a55f836530','55012566650b84a55f83655c','55012567650b84a55f83656c','55012567650b84a55f83657b','55012a8e650b84a55f8440ec','55012a8f650b84a55f84410f','5501cb8c650b84a55fa7a1d9','5501cb8d650b84a55fa7a20c','5501d663650b84a55faa7864','5501d663650b84a55faa786d','55000561650b84a55f570ea8','55000562650b84a55f570ef2','550036b1650b84a55f5f05ed','55003635650b84a55f5eee08','55028a58650b84a55fbfaaf4','55028a58650b84a55fbfab01','55028a59650b84a55fbfab4b','549b274b972b9a338dee6129','549b274bb3ec42333ba7100e','549b274c972b9a338dee6146','549b274d163659371fca846e','549b274d163659371fca8474','549b274d163659371fca847d','55012567650b84a55f836589','55012568650b84a55f8365ae','55012a90650b84a55f844156','55012a92650b84a55f844189','5501cb8e650b84a55fa7a22f','5501cb8e650b84a55fa7a25c','5501d664650b84a55faa788a','5501d665650b84a55faa7897','5501d667650b84a55faa78b4','55000564650b84a55f570f32','55000565650b84a55f570f60','55028a59650b84a55fbfab59','55028a5a650b84a55fbfab8f','55028a5a650b84a55fbfaba5','549b274db3ec42333ba71029','549b274e163659371fca8499','5501256c650b84a55f8365f5','5501cb56650b84a55fa78f34','5501d66a650b84a55faa78f6','5501d66a650b84a55faa78ff','55000567650b84a55f570fac','55000569650b84a55f570fc5','55003637650b84a55f5eee8f','55003639650b84a55f5eeec9','55003639650b84a55f5eeed0','55028a5a650b84a55fbfabcc','55028a5b650b84a55fbfabeb','549b274f163659371fca84de','5501256f650b84a55f836681','5501cb58650b84a55fa78fbe','5501d66e650b84a55faa7952','5501d672650b84a55faa79a0','5500056a650b84a55f571034','55003640650b84a55f5eef83','55028a5c650b84a55fbfac3b','55028a5c650b84a55fbfac41','55028a5c650b84a55fbfac49','549b274fb3ec42333ba7103f','5501256f650b84a55f83668a','5501256f650b84a55f836690','5501256f650b84a55f836698','55012570650b84a55f8366a5','55012570650b84a55f8366b4','55012570650b84a55f8366db','55012a98650b84a55f8442cc','55012a99650b84a55f8442ed','5501cb5a650b84a55fa7903f','5501d674650b84a55faa79be','5501d677650b84a55faa79e7','5501d67f650b84a55faa7a32','5500056b650b84a55f57105a','5500056b650b84a55f571086','5500056c650b84a55f57109b','55003640650b84a55f5eef9b','55003641650b84a55f5eefae','55003641650b84a55f5eefbd','55003642650b84a55f5eefcf','55028a5d650b84a55fbfac7c','55028a5d650b84a55fbfac82','55028a5e650b84a55fbfac9b','55028a5f650b84a55fbfacbf','549b2750163659371fca8567','549b2750972b9a338dee61bb','55012a9a650b84a55f844300','5501d680650b84a55faa7a4f','5501d684650b84a55faa7a9e','5500056c650b84a55f5710a2','5500056c650b84a55f5710c0','55003644650b84a55f5ef013','55003645650b84a55f5ef052','55003645650b84a55f5ef05b','55028a60650b84a55fbfacf7','55028a61650b84a55fbfad1d','55028a62650b84a55fbfad28','549b2751163659371fca85ac','55012573650b84a55f836735','55012573650b84a55f836744','55012574650b84a55f836769','55012574650b84a55f836770','55012575650b84a55f836797','5501cb5b650b84a55fa790d6','5501cb5b650b84a55fa7910a','55003646650b84a55f5ef0ae','55028a63650b84a55fbfad56','55028a64650b84a55fbfad7c','55028a66650b84a55fbfad97','55028a66650b84a55fbfada7','55028a67650b84a55fbfadbc','549b2752163659371fca85c8','55012576650b84a55f8367a4','55012576650b84a55f8367ad','55012576650b84a55f8367b3','55012aa0650b84a55f8443f3','55012aa0650b84a55f8443fc','55012aa1650b84a55f84442a','5501cb5d650b84a55fa7915b','5501d68e650b84a55faa7b39','5501d690650b84a55faa7b5c','55000571650b84a55f571174','55000571650b84a55f57118c','55003649650b84a55f5ef115','55003649650b84a55f5ef133','55028a68650b84a55fbfaddb','55028a68650b84a55fbfade1','55028a68650b84a55fbfade9','55028a69650b84a55fbfadef','55012577650b84a55f83680e','55012578650b84a55f83684f','55012aa2650b84a55f844460','5501cb5d650b84a55fa79196','5501cb5e650b84a55fa791c1','5501cb5e650b84a55fa791d0','5501d43d650b84a55faa05ec','5501d43d650b84a55faa05fb','5500364a650b84a55f5ef14b','5500364b650b84a55f5ef189','55028a6a650b84a55fbfae20','549b2754163659371fca8617','55012aa6650b84a55f8444d2','5501cb60650b84a55fa79227','5501d43e650b84a55faa0642','5501d43f650b84a55faa0650','55028a82650b84a55fbfb2e4','549b2755163659371fca8670','549b2755972b9a338dee6251','5501257c650b84a55f8368fa','55012aaa650b84a55f844533','55012aaa650b84a55f844551','55012aab650b84a55f84455d','55012aac650b84a55f844575','5501cb61650b84a55fa79268','5501d43f650b84a55faa06bc','550006c2650b84a55f574fc4','55003650650b84a55f5ef22c','55028a83650b84a55fbfb310','549b2755b3ec42333ba710e4','549b2755b3ec42333ba710ed','549b2756163659371fca869e','5501257f650b84a55f836967','55012580650b84a55f83696d','55012bc1650b84a55f847535','55012bc1650b84a55f847544','5501cb64650b84a55fa7931a','5501d442650b84a55faa076a','550006c4650b84a55f575046','55028a85650b84a55fbfb396','55028a86650b84a55fbfb3ae','549b2756b3ec42333ba7111f','549b2715972b9a338dee5bb1','550124c9650b84a55f83487a','5501cb64650b84a55fa7935e','5501d443650b84a55faa0795','550006c5650b84a55f575069','550006c5650b84a55f575070','55003653650b84a55f5ef2db','55003541650b84a55f5ebb6d','55028a87650b84a55fbfb3c8','55028a87650b84a55fbfb402','55028a88650b84a55fbfb403','549b2717972b9a338dee5bde','550124cb650b84a55f8348d5','550124cb650b84a55f8348f3','550124cd650b84a55f834933','5501cb66650b84a55fa793f2','550006c6650b84a55f5750c5','550006c7650b84a55f5750e8','55003541650b84a55f5ebba1','55028a88650b84a55fbfb438','55028a8a650b84a55fbfb46b','55028a8a650b84a55fbfb471','55028a8b650b84a55fbfb47b','549b2719163659371fca7f97','550124cd650b84a55f83494b','550124cd650b84a55f834959','550124cd650b84a55f83495a','550124ce650b84a55f83498c','550124ce650b84a55f8349a2','55012bc7650b84a55f847650','5501cb66650b84a55fa79452','5501d446650b84a55faa0899','5501d446650b84a55faa08b0','550006c8650b84a55f575130','550006ca650b84a55f57516f','550006cb650b84a55f575189','55003543650b84a55f5ebc07','55028a8b650b84a55fbfb489','55028a8b650b84a55fbfb490','55028a8e650b84a55fbfb4ca','549b271b163659371fca7fe2','549b271bb3ec42333ba70cd2','550124cf650b84a55f8349e7','55012bc9650b84a55f8476b1','55012bc9650b84a55f8476b9','55012bc9650b84a55f8476c8','5501cb68650b84a55fa794cb','5501d447650b84a55faa0908','550006cb650b84a55f5751a0','550006cc650b84a55f5751fd','55003545650b84a55f5ebc76','55028a90650b84a55fbfb507','55028a90650b84a55fbfb525','549b271c163659371fca7ff8','549b271c972b9a338dee5c22','550124d1650b84a55f834a60','55012bcc650b84a55f847722','55012bcc650b84a55f847731','5501cb68650b84a55fa794f7','5501cb69650b84a55fa79530','5501d449650b84a55faa095a','550006cc650b84a55f575201','55003547650b84a55f5ebd19','55028a91650b84a55fbfb553','55028a92650b84a55fbfb576','55028a93650b84a55fbfb586','549b271eb3ec42333ba70d55','550124d2650b84a55f834a7f','55012bcd650b84a55f847761','55012bcf650b84a55f847794','55012bcf650b84a55f8477a4','5501cb48650b84a55fa789ad','5501d44a650b84a55faa09ae','550006ce650b84a55f575261','55003549650b84a55f5ebd6f','55028a95650b84a55fbfb5c9','55028a96650b84a55fbfb5e4','55028a96650b84a55fbfb5fc','55028a98650b84a55fbfb626','549b2721163659371fca803a','550124d3650b84a55f834ae1','550124d3650b84a55f834ae9','550124d3650b84a55f834aea','55012bd0650b84a55f8477b8','55012bd3650b84a55f847828','5501d44a650b84a55faa0a15','5501d44b650b84a55faa0a58','55028a98650b84a55fbfb63e','55028a99650b84a55fbfb667','55028a9a650b84a55fbfb676','549b2722163659371fca8056','550124d5650b84a55f834b56','55012bd4650b84a55f847867','55012bd7650b84a55f84789a','5501d44b650b84a55faa0a9d','5501d44c650b84a55faa0aac','5501d44c650b84a55faa0ab2','5501d5ac650b84a55faa5e94','550006d3650b84a55f57533b','550006d5650b84a55f57536e','550006d5650b84a55f57537d','5500354e650b84a55f5ebe34','5500354e650b84a55f5ebe52','5500354e650b84a55f5ebe61','55028a9b650b84a55fbfb6b4','55028a9c650b84a55fbfb6c8','55028a9c650b84a55fbfb6d7','549b2724972b9a338dee5c6d','549b2724b3ec42333ba70dc7','549b2725163659371fca80a7','550124d7650b84a55f834bd5','550124d7650b84a55f834c17','5501cb4b650b84a55fa78aba','5501cb4b650b84a55fa78ad7','5501d5ac650b84a55faa5ebc','5501d5ad650b84a55faa5eca','5501d5ad650b84a55faa5ed0','5501d5ad650b84a55faa5ef6','5501d5ad650b84a55faa5eff','550006d6650b84a55f5753b2','550006d6650b84a55f5753bb','55000622650b84a55f5732f5','55003553650b84a55f5ebeca','55028acd650b84a55fbfbeef','55028acd650b84a55fbfbefe','55028acd650b84a55fbfbf11','549b2725972b9a338dee5ca8','549b2726972b9a338dee5cc5','549b2726972b9a338dee5cce','550124d9650b84a55f834c8d','55012bdd650b84a55f847939','55012bde650b84a55f84794c','5501cb4d650b84a55fa78b53','55000624650b84a55f573353','55003555650b84a55f5ebf3f','55028acd650b84a55fbfbf37','55028acd650b84a55fbfbf4f','55028acd650b84a55fbfbf55','55028acd650b84a55fbfbf5e','55028ace650b84a55fbfbf87','549b2727b3ec42333ba70dfa','55012be0650b84a55f847996','55012be0650b84a55f8479a6','55012be1650b84a55f8479c7','55012be2650b84a55f8479da','5501cb4e650b84a55fa78bbc','5501cb4e650b84a55fa78bc2','5501d5ae650b84a55faa5f73','5501d5af650b84a55faa5f81','5501d5af650b84a55faa5f90','55028acf650b84a55fbfbfcc','55028acf650b84a55fbfbfd2','549b2728972b9a338dee5d23','550124dc650b84a55f834d18','550124dc650b84a55f834d45','550124dd650b84a55f834d64','55012be7650b84a55f847a35','55012be8650b84a55f847a47','550036da650b84a55f5f0bb4','550036da650b84a55f5f0bbd','55028ad0650b84a55fbfbffe','55028ad0650b84a55fbfc000','55028ad0650b84a55fbfc008','55028ad0650b84a55fbfc026','55028ad0650b84a55fbfc02f','55028ad0650b84a55fbfc03e','55028ad0650b84a55fbfc044','55028ad1650b84a55fbfc054','55028ad1650b84a55fbfc063','549b2729972b9a338dee5d57','550124dd650b84a55f834d82','550124de650b84a55f834dbe','55012bea650b84a55f847a68','55012beb650b84a55f847a89','5501cb4f650b84a55fa78c4a','5501d5b1650b84a55faa605e','55000628650b84a55f573421','55000628650b84a55f573429','550036db650b84a55f5f0be4','550036db650b84a55f5f0bed','550036de650b84a55f5f0c0e','550036de650b84a55f5f0c32','55028ad1650b84a55fbfc06c','55028ad1650b84a55fbfc090','549b272b972b9a338dee5d9b','5501cb50650b84a55fa78caa','5500062a650b84a55f573484','550036df650b84a55f5f0c3e','55028ad3650b84a55fbfc0cd','55028ad6650b84a55fbfc136','549b272c972b9a338dee5db8','549b272d972b9a338dee5dd5','549b272e163659371fca8143','549b272e163659371fca814c','5501cb50650b84a55fa78d01','5501d5b4650b84a55faa614b','5501d5b4650b84a55faa6151','5500062c650b84a55f5734d4','550036e2650b84a55f5f0cab','550036e2650b84a55f5f0cb1','550036e4650b84a55f5f0cf3','55028ad8650b84a55fbfc172','55028ada650b84a55fbfc1a5','550124e2650b84a55f834ec7','550124e2650b84a55f834ee5','550124e3650b84a55f834ef3','55012c2d650b84a55f8484e4','55012c2e650b84a55f8484f6','5501d5b5650b84a55faa61ad','5500062f650b84a55f57353c','55000630650b84a55f573574','550036e6650b84a55f5f0d46','550036e7650b84a55f5f0d76','55028adb650b84a55fbfc1c4','55028adb650b84a55fbfc1cd','55028adb650b84a55fbfc1d3','549b2704b3ec42333ba70ac4','550124e4650b84a55f834f3b','550124e5650b84a55f834f4b','550124e5650b84a55f834f86','55012c34650b84a55f84857d','5501cb52650b84a55fa78e04','55000630650b84a55f57357d','55028adc650b84a55fbfc21b','55028a9e650b84a55fbfb708','55028aa0650b84a55fbfb741','5501cb54650b84a55fa78e73','5501d5b7650b84a55faa6264','5501d5b7650b84a55faa6273','5501d5b7650b84a55faa629a','55000636650b84a55f573619','55000637650b84a55f573630','550036ed650b84a55f5f0e43','550036ee650b84a55f5f0e55','55028aa0650b84a55fbfb750','55028aa0650b84a55fbfb767','55028aa2650b84a55fbfb7a2','549b2706972b9a338dee59ca','550124e7650b84a55f835036','550124e7650b84a55f835045','550124e7650b84a55f83504e','55012c38650b84a55f848615','55012c38650b84a55f84861e','55012c39650b84a55f848627','55012c39650b84a55f848636','55012c3a650b84a55f84863f','55012c3b650b84a55f848657','5501cb55650b84a55fa78ef5','5501cb55650b84a55fa78f0b','5501d5b8650b84a55faa6300','55000637650b84a55f573656','550036f1650b84a55f5f0e8e','550036f3650b84a55f5f0eba','55028aa4650b84a55fbfb802','55028aa4650b84a55fbfb80b','549b2707b3ec42333ba70aff','549b2708972b9a338dee5a0b','549b2708b3ec42333ba70b19','550124e9650b84a55f8350b3','55012c3e650b84a55f8486d5','55012c3f650b84a55f8486e7','5501d5ba650b84a55faa634c','5500063b650b84a55f5736cc','5500063b650b84a55f5736db','550036f6650b84a55f5f0f16','55028aa5650b84a55fbfb82e','55028aa6650b84a55fbfb86b','55028aa6650b84a55fbfb871','549b2709163659371fca7e46','549b2709b3ec42333ba70b4e','549b270a972b9a338dee5a36','549b270a972b9a338dee5a45','550124eb650b84a55f8350f8','550124eb650b84a55f835114','550124ec650b84a55f835120','55012c40650b84a55f848708','55012c41650b84a55f848747','5501d5ba650b84a55faa6370','5500063e650b84a55f573728','550036fc650b84a55f5f0fdb','55028aa6650b84a55fbfb880','55028aa7650b84a55fbfb8ad','55028aa7650b84a55fbfb8c2','549b270bb3ec42333ba70b80','55012c46650b84a55f84879e','55012c47650b84a55f8487c6','5501d5bc650b84a55faa6431','550036fc650b84a55f5f0ff8','550036fd650b84a55f5f1006','550036fe650b84a55f5f102c','55003700650b84a55f5f104c','55028aa9650b84a55fbfb920','55028aaa650b84a55fbfb94f','55028aaa650b84a55fbfb955','550124f1650b84a55f8351d7','55012c48650b84a55f8487ff','55012c4a650b84a55f84882d','5501cb3c650b84a55fa7850e','55000596650b84a55f57184a','55028aab650b84a55fbfb983','55028aad650b84a55fbfb9c4','549b270d972b9a338dee5ac3','549b270d972b9a338dee5ad2','550124f2650b84a55f835221','550124f3650b84a55f83525c','550124f4650b84a55f835278','55012c4c650b84a55f84887a','55012c4c650b84a55f848897','5501cb3d650b84a55fa785c3','5501d5bd650b84a55faa64b9','5501d5bd650b84a55faa64c0','5501d5bd650b84a55faa64c8','5501d5bd650b84a55faa64e6','5501d5bd650b84a55faa650b','55000599650b84a55f571903','55003709650b84a55f5f10c3','55028aad650b84a55fbfb9fa','55028aae650b84a55fbfba08','55028aaf650b84a55fbfba41','549b270eb3ec42333ba70bcb','549b270f163659371fca7e7f','550124f4650b84a55f835287','550124f6650b84a55f8352d1','550124f6650b84a55f8352d9','55012c4e650b84a55f8488cc','55012c4f650b84a55f8488e4','5501cb3e650b84a55fa785f5','5501d5be650b84a55faa653f','5500059a650b84a55f57192a','5500059a650b84a55f571930','5500360b650b84a55f5ee4b5','5500360b650b84a55f5ee4be','55028ab1650b84a55fbfba92','549b2710163659371fca7eb0','549b2710163659371fca7eb8','549b2710972b9a338dee5b17','550124fa650b84a55f835338','55012c51650b84a55f84892f','55012c52650b84a55f84895f','5501cb41650b84a55fa786a6','5501cb41650b84a55fa786af','5500059b650b84a55f5719ce','55028ab2650b84a55fbfbaa7','55028ab2650b84a55fbfbab6','55028ab3650b84a55fbfbac6','55028ab3650b84a55fbfbade','549b2711163659371fca7ed5','549b2711972b9a338dee5b2e','550124fb650b84a55f835371','550124fb650b84a55f835379','550124fb650b84a55f835388','55012c55650b84a55f8489ce','5500059c650b84a55f571a18','5500059e650b84a55f571a58','5500360d650b84a55f5ee540','5500360e650b84a55f5ee587','55028ab7650b84a55fbfbb4f','55028ab8650b84a55fbfbb65','55028ab8650b84a55fbfbb6e','549b2713972b9a338dee5b76','55012471650b84a55f8338eb','55012472650b84a55f833921','5501cb42650b84a55fa78729','5501cb42650b84a55fa7872a','5500059f650b84a55f571ab4','550005a0650b84a55f571ac1','55028aba650b84a55fbfbb7a','55028aba650b84a55fbfbba7','55028aba650b84a55fbfbbb6','549b2714163659371fca7f1d','549b2714163659371fca7f23','549b2714b3ec42333ba70c39','55012472650b84a55f833929','55012472650b84a55f833947','55028abc650b84a55fbfbbf8','55028abd650b84a55fbfbc2d','55028abe650b84a55fbfbc41','55028abe650b84a55fbfbc4a','55012477650b84a55f8339db','55012478650b84a55f8339e2','55012478650b84a55f8339eb','55012b75650b84a55f846918','55012b75650b84a55f84694e','55003614650b84a55f5ee70e','55028abe650b84a55fbfbc67','55028ac0650b84a55fbfbca6','55028ac0650b84a55fbfbcaf','549b26ca972b9a334804fd81','549b26cb972b9a334804fda7','549b26cbb3ec4232f6183eb7','5501247b650b84a55f833a23','5501247b650b84a55f833a32','550005a3650b84a55f571bf2','55003615650b84a55f5ee73e','55003615650b84a55f5ee75c','55003616650b84a55f5ee765','55028ac2650b84a55fbfbd22','549b26cbb3ec4232f6183ecf','549b26cc972b9a334804fdcd','55012b79650b84a55f846a27','550005a4650b84a55f571c40','55003617650b84a55f5ee7b4','55028ac3650b84a55fbfbd41','55028ac3650b84a55fbfbd50','55028ac4650b84a55fbfbd7c','549b26cd972b9a334804fdea','549b26cf16365936d9923187','5501247f650b84a55f833ad3','55012481650b84a55f833b22','55012482650b84a55f833b36','5501cb46650b84a55fa7892c','5501d5cb650b84a55faa68ec','55000736650b84a55f576662','55000739650b84a55f57669d','55028ac5650b84a55fbfbdab','55028ac5650b84a55fbfbdba','55028ac6650b84a55fbfbdd6','549b26d0972b9a334804fe26','549b26d116365936d992319b','549b26d116365936d99231a2','55012b7d650b84a55f846aaa','55012b7d650b84a55f846ab0','5501d5cc650b84a55faa6942','55003619650b84a55f5ee865','5500361b650b84a55f5ee8a9','55028ac7650b84a55fbfbe06','55028ac7650b84a55fbfbe1e','55028ac9650b84a55fbfbe57','549b26d2972b9a334804fe59','55012486650b84a55f833bcb','55012486650b84a55f833be0','55012b82650b84a55f846b20','5501cb6f650b84a55fa79759','5501d5ce650b84a55faa69c9','5500073e650b84a55f576726','5500361c650b84a55f5ee8ad','5500361c650b84a55f5ee8d9','5500361c650b84a55f5ee8f7','55028acb650b84a55fbfbead','55028acb650b84a55fbfbeb3','550289f9650b84a55fbf9933','550289f9650b84a55fbf993c','550289f9650b84a55fbf9942','549b26d2b3ec4232f6183f5b','549b26d2b3ec4232f6183f69','549b26d2b3ec4232f6183f6a','549b26d4972b9a334804fe9c','55012488650b84a55f833c31','55012b86650b84a55f846ba1','55012b86650b84a55f846baa','5501d5ce650b84a55faa69f6','55000740650b84a55f5767b0','55000740650b84a55f5767b8','5500361f650b84a55f5ee985','550289fa650b84a55fbf9977','550289fb650b84a55fbf999f','549b26d4972b9a334804fea3','549b26d4b3ec4232f6183f85','549b26d5972b9a334804feb9','5501248a650b84a55f833c81','5501248b650b84a55f833ca1','55012b88650b84a55f846bfb','55012b8a650b84a55f846c37','5501d5d3650b84a55faa6ab3','55000742650b84a55f576818','55000743650b84a55f57682d','55000743650b84a55f576833','55000744650b84a55f57685b','5500361f650b84a55f5ee994','5500361f650b84a55f5ee9a4','550289fc650b84a55fbf99ca','550289fc650b84a55fbf99f6','550289fc650b84a55fbf9a04','5501248b650b84a55f833cb8','5501248c650b84a55f833cdc','5501248c650b84a55f833d0f','55012b8b650b84a55f846c67','55012b8c650b84a55f846c7e','55012b8d650b84a55f846caf','5501d5d4650b84a55faa6ac1','5501e022650b84a55fab6a21','5501e025650b84a55fab6a53','5501e025650b84a55fab6a62','55000745650b84a55f57686f','55000745650b84a55f57687e','55000747650b84a55f5768c7','55003622650b84a55f5eea26','55003624650b84a55f5eea82','550289ff650b84a55fbf9a54','550289ff650b84a55fbf9a5d','55028a00650b84a55fbf9a61','55028a00650b84a55fbf9a70','55012b8e650b84a55f846ce5','55012b8f650b84a55f846d0d','55012b8f650b84a55f846d13','55012b90650b84a55f846d27','55012b90650b84a55f846d3f','5501cb73650b84a55fa798f9','5501e027650b84a55fab6a86','5501e028650b84a55fab6a94','5501e02a650b84a55fab6aac','5501e02b650b84a55fab6ab1','55000748650b84a55f5768ed','55000749650b84a55f576927','55000749650b84a55f576936','55000749650b84a55f57693f','55028a01650b84a55fbf9aac','55028a02650b84a55fbf9ab3','55028a03650b84a55fbf9adf','55028a03650b84a55fbf9ae5','549b26dd16365936d992324c','549b26de16365936d9923251','5501248f650b84a55f833dad','55012522650b84a55f8359f7','55012523650b84a55f835a15','55012523650b84a55f835a24','55012b92650b84a55f846d9f','5501cb74650b84a55fa7996e','5501e02c650b84a55fab6ac7','5501e02c650b84a55fab6ad6','5500074a650b84a55f576972','55003626650b84a55f5eeaf2','55028a04650b84a55fbf9afe','55028a05650b84a55fbf9b27','55028a06650b84a55fbf9b42','549b26dfb3ec4232f6184044','55012524650b84a55f835a38','55012524650b84a55f835a47','55012525650b84a55f835a66','55012525650b84a55f835a8d','55012525650b84a55f835aa3','5501cb76650b84a55fa79a27','5501e030650b84a55fab6b56','5501e032650b84a55fab6b89','5501e032650b84a55fab6b90','5501e032650b84a55fab6ba0','5500074c650b84a55f5769b5','5500074d650b84a55f5769f6','55000691650b84a55f574673','55000692650b84a55f57467b','55003628650b84a55f5eeba4','55028a06650b84a55fbf9b59','55028a07650b84a55fbf9b87','549b26e1972b9a334804ff1d','549b26e1b3ec4232f6184066','549b26e416365936d99232b4','55012526650b84a55f835aba','5501cb76650b84a55fa79a36','5501cb76650b84a55fa79a6c','5501e033650b84a55fab6bc5','5501e034650b84a55fab6bcd','5501e034650b84a55fab6be2','5501e035650b84a55fab6bf0','55000692650b84a55f574689','55003628650b84a55f5eebbc','55028a09650b84a55fbf9bc2','549b26e416365936d99232bd','549b26e5972b9a334804ff7b','549b26e6972b9a334804ff97','55012527650b84a55f835b25','55012c0e650b84a55f847fe0','5501e036650b84a55fab6c31','5501e037650b84a55fab6c38','5501e037650b84a55fab6c47','5501e039650b84a55fab6c72','55000693650b84a55f5746e1','55000693650b84a55f5746ea','5500362d650b84a55f5eec84','55028a0b650b84a55fbf9c36','55028a0b650b84a55fbf9c3f','55028a0b650b84a55fbf9c45','55028a0c650b84a55fbf9c7c','55028a0d650b84a55fbf9c87','549b26b5972b9a334804fb71','549b26b5972b9a334804fb79','55012528650b84a55f835b79','5501252a650b84a55f835bad','5501252a650b84a55f835bb3','5501252a650b84a55f835bbc','55012c11650b84a55f84804e','55012c12650b84a55f84806f','55012c12650b84a55f848075','5501cb78650b84a55fa79b06','5501e03c650b84a55fab6c96','5501e03c650b84a55fab6c9f','5501e03d650b84a55fab6cbd','5501e03d650b84a55fab6ccc','5501e03e650b84a55fab6cd9','55000696650b84a55f574775','55028a0d650b84a55fbf9c96','55028a0d650b84a55fbf9cbe','55028a0e650b84a55fbf9ccc','55028a0e650b84a55fbf9cd2','55028a0e650b84a55fbf9cdb','55028a0e650b84a55fbf9ce9','549b26b5b3ec4232f6183ced','549b26b6972b9a334804fb87','549b26b6b3ec4232f6183d1f','5501252b650b84a55f835bea','5501252c650b84a55f835c0b','55012c13650b84a55f8480b5','55012c15650b84a55f8480e0','5501cb79650b84a55fa79b78','5501cb7a650b84a55fa79bbe','5501e03e650b84a55fab6ce8','5501e040650b84a55fab6d17','5501e041650b84a55fab6d3d','5501e041650b84a55fab6d52','55000698650b84a55f574810','5500362f650b84a55f5eecfd','55028a0e650b84a55fbf9d05','55028a0e650b84a55fbf9d0e','55028a10650b84a55fbf9d65','549b26b716365936d9922fb1','5501252d650b84a55f835c30','5501252e650b84a55f835c73','55012c16650b84a55f8480fb','55012c16650b84a55f848107','5501cb7b650b84a55fa79c12','5501e044650b84a55fab6d8d','5501e045650b84a55fab6dba','55000699650b84a55f574844','55003634650b84a55f5eedd6','55028a10650b84a55fbf9d74','55028a10650b84a55fbf9d7d','55028a11650b84a55fbf9d97','55028a11650b84a55fbf9da7','549b26b8b3ec4232f6183d85','5501252f650b84a55f835c96','55012c1c650b84a55f848191','5501cac5650b84a55fa7617e','5501e046650b84a55fab6dd6','5501e047650b84a55fab6dde','5501e048650b84a55fab6df2','5501e049650b84a55fab6e06','5501e04a650b84a55fab6e0f','5501e069650b84a55fab7116','5501e06a650b84a55fab711f','5501e06a650b84a55fab7125','5501e06b650b84a55fab712d','5500069a650b84a55f57489e','5500069b650b84a55f5748bc','5500069b650b84a55f5748cb','5500069b650b84a55f5748f7','550035bc650b84a55f5ed2fb','550035bc650b84a55f5ed316','550288fe650b84a55fbf705c','550288fe650b84a55fbf706b','550288fe650b84a55fbf7071','550288ff650b84a55fbf7087','55028900650b84a55fbf70b6','549b26b9b3ec4232f6183d9c','549b26ba972b9a334804fc03','55012533650b84a55f835d64','55012c1d650b84a55f8481e8','5501caca650b84a55fa7623b','5501e06e650b84a55fab716e','5501e070650b84a55fab7196','5501e072650b84a55fab719d','5501e073650b84a55fab71a3','55028901650b84a55fbf70c6','55028902650b84a55fbf70cc','55028902650b84a55fbf70ea','549b26bc972b9a334804fc18','55012534650b84a55f835d70','55012536650b84a55f835dda','55012c20650b84a55f848261','5501caca650b84a55fa76249','5501e076650b84a55fab71c6','5501e076650b84a55fab71d5','5501e077650b84a55fab71dd','5501e078650b84a55fab71eb','5501e07b650b84a55fab7204','550006a2650b84a55f5749e1','550035bf650b84a55f5ed3c9','55028905650b84a55fbf716f','55028905650b84a55fbf7175','55028905650b84a55fbf7184','55028906650b84a55fbf7189','549b26bd972b9a334804fc3e','55012537650b84a55f835df6','55012539650b84a55f835e45','55012c21650b84a55f8482a1','5501cacc650b84a55fa762ba','5501e07c650b84a55fab7221','5501e07c650b84a55fab7230','5501e07e650b84a55fab725d','550006a4650b84a55f574a39','550035c1650b84a55f5ed429','550035c1650b84a55f5ed430','55028907650b84a55fbf71a7','55028907650b84a55fbf71ce','549b26c016365936d9923068','550124b0650b84a55f8343eb','5501e080650b84a55fab7285','5501e081650b84a55fab729c','5501e083650b84a55fab72e5','55000898650b84a55f57a41c','550035c3650b84a55f5ed481','550035c3650b84a55f5ed498','550035c4650b84a55f5ed4ac','55028908650b84a55fbf71fc','55028909650b84a55fbf721d','55028909650b84a55fbf7232','55028909650b84a55fbf723b','549b26c0972b9a334804fc5f','550124b1650b84a55f83445b','550124b2650b84a55f834466','550124b2650b84a55f834475','55012c26650b84a55f848384','55012c26650b84a55f848393','55012c27650b84a55f8483af','5501cacd650b84a55fa7638c','5501e084650b84a55fab72f3','5501e084650b84a55fab72fc','5501e088650b84a55fab733a','5501e088650b84a55fab7348','55000898650b84a55f57a431','55000899650b84a55f57a43b','5500089a650b84a55f57a47f','550035c5650b84a55f5ed4f5','550035c5650b84a55f5ed4fe','55028909650b84a55fbf7258','5502890a650b84a55fbf7267','5502890a650b84a55fbf7276','5502890a650b84a55fbf728e','5502890b650b84a55fbf729e','5502890b650b84a55fbf72bd','5502890b650b84a55fbf72c3','549b26c2972b9a334804fc7b','549b26c2b3ec4232f6183e4e','549b26c316365936d99230c8','549b26c3972b9a334804fc88','549b26c416365936d99230d6','550124b3650b84a55f8344d1','550124b4650b84a55f8344ee','55012c28650b84a55f8483df','5501e08e650b84a55fab73c4','5501e08e650b84a55fab73cd','5500089a650b84a55f57a485','5500089b650b84a55f57a4b0','5500089b650b84a55f57a4d6','550035c8650b84a55f5ed574','5502890c650b84a55fbf72d7','5502890e650b84a55fbf7335','5502890f650b84a55fbf7345','5502890f650b84a55fbf734e','549b26c5972b9a334804fccc','549b26c5972b9a334804fcea','550124b6650b84a55f834525','550124b7650b84a55f834560','55012c29650b84a55f848426','55012c2a650b84a55f848457','5501e090650b84a55fab740b','5500089c650b84a55f57a508','5500089c650b84a55f57a54d','550035ca650b84a55f5ed5d2','550035cb650b84a55f5ed5d5','550035cb650b84a55f5ed5ed','5502890f650b84a55fbf7354','5502890f650b84a55fbf736c','5502890f650b84a55fbf7372','5502890f650b84a55fbf737b','5502890f650b84a55fbf7389','55028910650b84a55fbf739e','549b26c6972b9a334804fd0d','549b26c6972b9a334804fd13','549b26c716365936d9923124','549b26c716365936d992312d','550124ba650b84a55f8345ac','5501cad3650b84a55fa764ee','5501cad3650b84a55fa764f4','5501e092650b84a55fab743e','5501e092650b84a55fab744d','5501e093650b84a55fab7452','5501e093650b84a55fab7461','5501e095650b84a55fab7494','5500089d650b84a55f57a58a','550035cb650b84a55f5ed5f3','550035cd650b84a55f5ed63c','550035ce650b84a55f5ed64a','55028910650b84a55fbf73cc','55028911650b84a55fbf73d1','55028911650b84a55fbf73e0','55012b39650b84a55f845e55','5501cad5650b84a55fa76561','5501e095650b84a55fab74a4','5501e095650b84a55fab74ad','5501e0d0650b84a55fab7b6c','5501e0d1650b84a55fab7b88','5500089e650b84a55f57a5e1','5500089f650b84a55f57a606','550035cf650b84a55f5ed697','55028912650b84a55fbf7410','55028913650b84a55fbf7455','55028913650b84a55fbf746d','549b269bb3ec4232f6183b68','549b269cb3ec4232f6183b9d','55012b3b650b84a55f845eb3','55012b3b650b84a55f845ec2','55012b3b650b84a55f845ed9','55012b3c650b84a55f845eec','5501e0d6650b84a55fab7bfe','5501e0d8650b84a55fab7c08','5501e0db650b84a55fab7c2d','5501e0db650b84a55fab7c33','5500089f650b84a55f57a624','550035d0650b84a55f5ed6d1','550035d0650b84a55f5ed6da','550035d1650b84a55f5ed6f2','550035d2650b84a55f5ed71a','55028944650b84a55fbf7d9c','55028945650b84a55fbf7dbc','549b269e16365936d9922eb5','55012b3d650b84a55f845f0b','55012b3d650b84a55f845f20','55012b3f650b84a55f845f57','55012b3f650b84a55f845f6f','5501e0dc650b84a55fab7c41','5501e0e0650b84a55fab7c70','5501e0e0650b84a55fab7c78','5501e0e1650b84a55fab7c86','5501e0e1650b84a55fab7c9e','550008a1650b84a55f57a68c','550008a1650b84a55f57a6b1','550008a2650b84a55f57a6b6','5500366f650b84a55f5ef9ab','55028945650b84a55fbf7dd9','55028946650b84a55fbf7de5','55028946650b84a55fbf7dfd','55028947650b84a55fbf7e11','549b269f972b9a334804f90a','549b26a0b3ec4232f6183bb6','550124c2650b84a55f83474d','55012b40650b84a55f845f89','55012b40650b84a55f845f98','5501e0e2650b84a55fab7cbc','5501e0e2650b84a55fab7cc2','5501e0e3650b84a55fab7cc9','5501e0e4650b84a55fab7cf5','5501e0e9650b84a55fab7d14','550008a3650b84a55f57a726','550008a3650b84a55f57a73e','55003671650b84a55f5efa2a','55003671650b84a55f5efa30','55028947650b84a55fbf7e28','55028949650b84a55fbf7e6b','55028949650b84a55fbf7e71','55028949650b84a55fbf7e79','549b26a2972b9a334804f950','549b26a2b3ec4232f6183bd2','549b26a2b3ec4232f6183bdb','549b26a3972b9a334804f96f','550124c3650b84a55f834763','5501cad8650b84a55fa766fe','5501e0eb650b84a55fab7d31','5501e0eb650b84a55fab7d3a','5501e0ed650b84a55fab7d5e','550008a5650b84a55f57a7c3','550008a6650b84a55f57a7c8','5502894a650b84a55fbf7e97','5502894a650b84a55fbf7eb6','5502894a650b84a55fbf7ebf','5502894b650b84a55fbf7ecb','549b26a4972b9a334804f992','550124c6650b84a55f8347ff','550124c6650b84a55f834812','550124c6650b84a55f834821','55012b43650b84a55f846078','5501cb6c650b84a55fa795cf','5501e0f1650b84a55fab7d8d','5501e0f2650b84a55fab7dc0','5501e0f2650b84a55fab7dc8','5501e0f3650b84a55fab7dd6','5502894c650b84a55fbf7f14','5502894c650b84a55fbf7f23','5502894d650b84a55fbf7f31','5502894d650b84a55fbf7f39','549b26a4b3ec4232f6183c2a','549b26a5b3ec4232f6183c4f','550124c6650b84a55f834829','550124c6650b84a55f83482a','55012400650b84a55f8324ac','55012b44650b84a55f8460a1','55012b45650b84a55f8460d1','5501cb6d650b84a55fa795f4','5501cb6d650b84a55fa795fd','5501cb6d650b84a55fa79609','5501cb6d650b84a55fa79610','5501cb6d650b84a55fa79627','5501e0f6650b84a55fab7e0f','5501e0f6650b84a55fab7e24','5501e0f8650b84a55fab7e40','5501e0f8650b84a55fab7e48','5501e0f8650b84a55fab7e57','550008a8650b84a55f57a864','550008a9650b84a55f57a8a6','55003678650b84a55f5efb25','5502894f650b84a55fbf7f8b','55028950650b84a55fbf7f9f','55028951650b84a55fbf7fa3','55028951650b84a55fbf7fb2','549b26a8972b9a334804fa16','55012402650b84a55f832509','55012403650b84a55f83252d','55012403650b84a55f832533','55012403650b84a55f83253c','55012b45650b84a55f846104','55012b46650b84a55f84612e','55012b46650b84a55f846134','5501cb6e650b84a55fa79668','5501cb6e650b84a55fa79686','5501cb6e650b84a55fa7968f','5501e0f9650b84a55fab7e5f','5501e0fa650b84a55fab7e92','5501e0fb650b84a55fab7ec7','5501e0fc650b84a55fab7ecf','550008aa650b84a55f57a8eb','5500367d650b84a55f5efbd7','5500367e650b84a55f5efbe9','55028952650b84a55fbf7fc2','55028952650b84a55fbf7fe0','55028953650b84a55fbf7fee','55028953650b84a55fbf8007','55028954650b84a55fbf8017','55028954650b84a55fbf802f','549b26a916365936d9922f19','549b26a9972b9a334804fa42','55012403650b84a55f83255a','55012b48650b84a55f846198','5501cb6f650b84a55fa796dc','5501cb6f650b84a55fa796f9','5501e0fd650b84a55fab7efb','5501e0fe650b84a55fab7f1e','5501e0ff650b84a55fab7f41','550008ac650b84a55f57a92b','550008ac650b84a55f57a93a','5500367f650b84a55f5efc35','55003680650b84a55f5efc4a','55028954650b84a55fbf8035','55028955650b84a55fbf8043','55028956650b84a55fbf8084','55012405650b84a55f8325c9','55012405650b84a55f8325ca','55012b49650b84a55f8461d8','5501e100650b84a55fab7f4d','5501e101650b84a55fab7f5b','5501e102650b84a55fab7f68','5501e111650b84a55fab7f95','5501e112650b84a55fab7fa4','550008ae650b84a55f57a98b','55003680650b84a55f5efc67','55003681650b84a55f5efc88','55003681650b84a55f5efcdd','55028957650b84a55fbf80bc','55028957650b84a55fbf80c2','55028957650b84a55fbf80cb','549b26ae972b9a334804faa9','549b26af16365936d9922f7f','55012409650b84a55f832654','55012b4a650b84a55f84625f','5501cbbc650b84a55fa7b0a8','5501cbbd650b84a55fa7b0c7','5501e112650b84a55fab7fb3','5501e112650b84a55fab7fcb','5501e112650b84a55fab7fd1','550008b1650b84a55f57aa0e','550008b2650b84a55f57aa1c','550008b2650b84a55f57aa31','55003682650b84a55f5efcfe','55028958650b84a55fbf80ee','5502895a650b84a55fbf810b','5502895b650b84a55fbf8116','5502895c650b84a55fbf8126','5502895d650b84a55fbf8141','549b26af972b9a334804fab7','549b26b0972b9a334804fac3','5501240a650b84a55f83268a','5501240d650b84a55f8326ca','5501240d650b84a55f8326f6','55012bef650b84a55f847b71','55012bef650b84a55f847b7a','5501e116650b84a55fab8042','5501e116650b84a55fab804b','55000872650b84a55f579dc1','55003685650b84a55f5efd9f','5502895d650b84a55fbf8149','5502895d650b84a55fbf814a','550289de650b84a55fbf950b','550289de650b84a55fbf9511','550289de650b84a55fbf9519','549b26b1972b9a334804fb1c','549b26e7972b9a334804ffbe','5501240e650b84a55f83271c','5501240e650b84a55f832722','55012410650b84a55f832759','5501e11c650b84a55fab80d5','5501e11d650b84a55fab80dd','5501e11e650b84a55fab80f9','55000872650b84a55f579dca','55003686650b84a55f5efdee','550289df650b84a55fbf952e','550289e0650b84a55fbf9566','550289e0650b84a55fbf956f','550289e0650b84a55fbf9575','550289e0650b84a55fbf957e','550289e0650b84a55fbf9584','549b26e7972b9a334804ffcd','55012411650b84a55f832794','55012411650b84a55f8327ad','55012634650b84a55f838599','55012bf5650b84a55f847c30','5501cbc0650b84a55fa7b1ca','5501cbc1650b84a55fa7b208','5501cbc1650b84a55fa7b226','5501e11f650b84a55fab810e','5501e11f650b84a55fab811d','5501e120650b84a55fab8146','55000874650b84a55f579e4e','55003688650b84a55f5efe52','55003688650b84a55f5efe6a','550289e1650b84a55fbf9594','550289e1650b84a55fbf95a4','550289e2650b84a55fbf95aa','550289e3650b84a55fbf95ed','549b26ea972b9a334804fff7','549b26eab3ec4232f61840ca','55012634650b84a55f8385a1','55012bf7650b84a55f847c8e','55012bf9650b84a55f847cb1','5501cbc2650b84a55fa7b26c','5501cbc3650b84a55fa7b295','5501e122650b84a55fab8180','5501e123650b84a55fab819f','5501e124650b84a55fab81bd','5501e125650b84a55fab81c2','55000876650b84a55f579ea6','55000876650b84a55f579eaf','5500368a650b84a55f5efee5','550289e4650b84a55fbf95fd','550289e4650b84a55fbf9601','550289e4650b84a55fbf9609','550289e4650b84a55fbf9627','550289e6650b84a55fbf9652','549b26ec972b9a3348050015','549b26ecb3ec4232f6184101','55012637650b84a55f838635','55012638650b84a55f838649','55012638650b84a55f83864a','55012bff650b84a55f847d1d','5501cbc3650b84a55fa7b2ae','5501e125650b84a55fab81d9','5501e125650b84a55fab81e0','5501e127650b84a55fab820b','5501e127650b84a55fab8211','5501e127650b84a55fab8219','5500368a650b84a55f5efefd','5500368b650b84a55f5eff17','5500368b650b84a55f5eff53','550289e6650b84a55fbf966a','550289e7650b84a55fbf968d','550289e7650b84a55fbf96ac','549b26ed972b9a334805003b','5501263a650b84a55f8386b4','5501e0b2650b84a55fab7821','5501e0b2650b84a55fab782a','5500087e650b84a55f579fbe','5500368d650b84a55f5effa5','5500368d650b84a55f5effae','5500368d650b84a55f5effcc','550289e9650b84a55fbf96ee','550289e9650b84a55fbf96f4','550289ea650b84a55fbf9701','550289ea650b84a55fbf9709','550289ea650b84a55fbf970a','549b26f016365936d992335c','549b26f0972b9a3348050053','549b26f0b3ec4232f618415f','5501263c650b84a55f838721','5501263c650b84a55f83872a','5501263d650b84a55f83874f','55012c02650b84a55f847ddf','55012c04650b84a55f847e2e','55012c04650b84a55f847e3d','5501cbc7650b84a55fa7b383','5501e0b5650b84a55fab7879','5501e0b5650b84a55fab787a','5501e0b5650b84a55fab7880','5501e0b6650b84a55fab78af','5501e0b7650b84a55fab78c3','5500087f650b84a55f579fe3','55000880650b84a55f57a003','55000881650b84a55f57a035','5500368e650b84a55f5effe4','550289ea650b84a55fbf9718','550289ed650b84a55fbf978f','549b26f1972b9a334805009f','55012c04650b84a55f847e43','55012c06650b84a55f847e8e','5501e0b7650b84a55fab78d2','5501e0b8650b84a55fab78f7','55000882650b84a55f57a05d','55000883650b84a55f57a080','55003690650b84a55f5f0048','550038c6650b84a55f5f5dde']

    data = db.find({"target_id":"5501d461650b84a55faa102b"})
    # data = db.find()
    profile_count = 1

    f = open('modified_test_jsp_exp_plus_1.csv','a')
    reg_inp = open('regression.csv','a')
    reg_out = open('regression_output.csv','a')
    reg_out_curr_job_dur = open('regression_output_current_job_dur.csv','a')
    reg_out_total_job_dur = open('regression_output_total_job_dur.csv','a')
    reg_output_candidate_job_durs = open('regression_output_candidate_job_durs.csv','a')

    for document in data:

        id = document['target_id']

        similar_users = document['similar_profiles']

        fetch_target_query = {
            "query": {
                "term": {
                   "_id": {
                      "value": id
                   }
                }
            }
        }

        res = prod_es.search(index=es_index, doc_type=es_type, request_timeout = 30000, body = fetch_target_query)
        profiles = res['hits']['hits']


        for profile in profiles:

            print profile['_id']

            print "profile no", profile_count

            try:
                (target) = create_target_array(profile)
                print "actual_target",target
            except Exception as e:
                print e
                print "ERROR in create_target_array"

            # components of target array = ['id', [degree], [colleges], [college_start_dates], [company names], [company profile title], [company_start_date], [company_end_date], [current_location], [major], [skills_set]]

            print "og  profile",profile

            output = None

            # try:
            output = go_to_past(profile,similar_users)
            # except Exception as e:
            #     print e
            #     print "ERROR in go_to_past"

            print "output",output

            print "\n\n\n\n-------------------------------------------------------------------------\n\n\n\n"

            profile_count = profile_count + 1

    f.close()







    # for profile in profiles:
    #
    #     print profile['_id']
    #     print "profile no", profile_count
    #
    #     profile_count = profile_count + 1
    #
    #     try:
    #         (target) = create_target_array(profile)
    #         print "actual_target",target
    #     except Exception as e:
    #         print e
    #         print "AYUSH ERROR"
    #
    #     # components of target array = ['id', [degree], [colleges], [college_start_dates], [company names], [company profile title], [company_start_date], [company_end_date], [current_location], [major], [skills_set]]
    #
    #
    #     print "og  profile",profile
    #
    #     output = None
    #
    #     try:
    #         output = go_to_past(profile)
    #     except Exception as e:
    #         print e
    #         print "AYUSH ERROR"
    #
    #     print "output",output
    #
    #     # print "here",new_target,creased_company_index,creased_college_index
    #
    #     print "\n\n\n\npress enter!!!"
    #
    #     if output == None:
    #         inp = raw_input("\n\n\n\npress enter!!!")
    #
    # f.close()


