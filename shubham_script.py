__author__ = 'shubham grownout'
import csv,re
from ayush2 import dict_test_desig
from pymongo import MongoClient
client = MongoClient('localhost')
db=client['test']['test_changes']


def abb_check(desig):
    abb_dict={}
    with open("abb_dict.csv", "rb") as in_file1:
        reader = csv.reader(in_file1)
        for row in reader:
            abb_dict[row[0]] =(row[1])

    for key,value in abb_dict.items():
        desig = re.sub(r'\b'+key+r'\b',value,desig)

    return desig
#--------------------------------------------------------------

def get_bigrams(string):        #Takes a string and returns a list of bigrams
    s = string.lower()
    return [s[i:i+2] for i in xrange(len(s) - 1)]

def string_similarity(str1, str2):              #Perform bigram comparison between two strings and return a percentage match in decimal form
    pairs1 = get_bigrams(str1)
    pairs2 = get_bigrams(str2)
    union  = len(pairs1) + len(pairs2)
    hit_count = 0
    for x in pairs1:
        for y in pairs2:
            if x == y:
                hit_count += 1
                break
    return (2.0 * hit_count) / union


def closest_match(j):
    max_simil=0.0
    flag = 0
    for i in new_final_words:
        if string_similarity(i,j)>0.7:
            flag = 1
            if string_similarity(i,j)>max_simil:
                match_word=i
                max_simil=string_similarity(i,j)

    if flag == 0:
        # print j
        return('')

    else:
        return(match_word)

#---------------------------------------------------------------------------------------------

def abb_spell_check():
    desig_array=[]
    with open("dict_top_desig_test.csv", "rb") as in_file1:
        reader = csv.reader(in_file1)
        for row in reader:
            desig_array.append(row[0])

    for i in desig_array:
        i=re.sub(r'[/,-]',' ',i)
        i=re.sub(r'\.','',i)
        final_word = ''
        for word in i.split():
            j=abb_check(word.lower())
            final_word = final_word + ' '+j

        final_word2=''
        for k in final_word.split():
            if len(k)>1:
                l=closest_match(k)
                final_word2 = final_word2 + ' '+l
        print i,'------>',final_word2

# abb_spell_check()
#---------------------------------------------------
new_final_words=[]
ib_str=[]
with open("final_new_words_dict.csv", "rb") as in_file1:
    reader = csv.reader(in_file1)

    for row in reader:
        new_final_words.append(row[0])


with open("ib_str_rep_word_dict.csv", "rb") as in_file1:
    reader = csv.reader(in_file1)

    for row in reader:
        ib_str.append(row[0])



def abb_spell_check2(desigs_array):
    count=0
    alpha=''
    beta=''
    job_count_dict={}

    for i in desigs_array:
        i=i.lower()
        i1=re.sub(r'[/,-]',' ',i)
        i2=re.sub(r'\.',' ',i1)
        for word in ib_str:
            if len(word)>3:
                i2=re.sub(r'(.*)'+word+r'(.*)',r'\1 '+word+r' \2',i2,re.IGNORECASE)


        if len(i2)>1:
            j=abb_check(i2)
            # final_word = final_word + ' '+j
            # alpha=final_word+' '+j
        else:
            j=i2
        #     final_word=final_word+' '+word
        #     alpha=final_word+word
        # final_word2=''


        final_word = ''
        for k in j.split():
            if len(k)>3:
                if k not in new_final_words:
                    l=closest_match(k)
                    final_word = final_word + ' '+l
                    # beta=final_word2+' '+l
                else:
                    final_word=final_word+' '+k
                    # beta=final_word2+' '+k
            else:
                final_word=final_word+' '+ k
                # beta=final_word2+k

        # alpha=re.sub('\s*','',i2)
        # beta=re.sub('\s*','',final_word)
        # if alpha!= beta.lower():
        #     count+=1
        ######## print i,'------>',final_word
        #     # print dict_test_desig[i]
        #     writer = csv.writer(open('test_changes_2.csv', 'a'))
        #     writer.writerow([dict_test_desig[i],i,final_word])

            # if count==1000:
            #     print 'over'
            #     exit(0)

        # if a not in temp_array:
        #     temp_array.append(a)
        #     job_count_dict[final_word]=1
        # else:
        flag={}
        b=re.sub('\s*','',final_word)
        if not job_count_dict:
            job_count_dict[final_word]=1
        else:
            temp = 0
            for each_desig in job_count_dict:
                a=re.sub('\s*','',each_desig)
                c=0
                if len(a)>2 and len(b)>2:
                    c=string_similarity(a,b)

                if c>0.7:
                    if c > temp:
                        temp=c
                        flag[c]=each_desig
            if len(flag)==0:
                job_count_dict[final_word]=1
            else:
                job_count_dict[flag[max(flag.keys())]]+=1

    print job_count_dict
    print len(job_count_dict)
abb_spell_check2(dict_test_desig.keys())
