# Predicting Job-Switching Pattern #

A predictive model to find the job-switching pattern and job-switching willingness of working professionals, by analysing their professional demographic data available publicly on social networks such as LinkedIn, Facebook, AboutMe, etc.

### Approach ###

 * Clustered profiles using k-means clustering on suitable parameters from the dataset.
 * Also, the model predicted the job switching willingness and time remaining before switching of professionals by analysing the similar profiles trends and switching patterns. 
 * But, unfortunately, this prediction had a quite many false alarms as well for job-switching willingness as the data had around less than 5% of positives but it was satisfactory for the requirement. 
 * Gather extensive insights such as the most trending skills, organizations and similarities in career path of different individuals.

### Main File ###
```
/test_jsp/test_jsp.py
```