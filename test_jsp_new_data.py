__author__ = 'ayush'

import re
import collections
import copy
import sys
import math

from pymongo import MongoClient
from elasticsearch import Elasticsearch
from datetime import datetime

# client = MongoClient('130.211.119.14')
client = MongoClient('dev-mongo1.grownout.com')
db = client['test']['test_jsp_new_5000']

# from experience import get_job_durations

# SOME GLOCAL DEFINITIONS
# TRACE_BACK_JOB_NO = 0
TRACE_BACK_YEAR = "2010-01-01T00:00:00.000Z"
TRACE_BACK_YEAR = datetime.strptime(TRACE_BACK_YEAR, "%Y-%m-%dT%H:%M:%S.%fZ")
MIN_JOB_NO = 2 # minimumjob count a profile should have before applying the function
DAYS_IN_MONTH = 30  # One Month is 30 days
MONTHS_IN_YEAR = 12  # One year is 12 months
MONTHS_IN_HALF_YEAR = 6  # Six months in half year
MONTHS_IN_QUARTER_YEAR = 3  # Three months in Quarter Year
MIN_SIMILAR_USERS_REQUIRED = 10  # We will not do analytics if number of similar users is less.
CAN_SWITCH_NOW = 0  # Return 0 if we think he is probable to switch now.
LOOKING_FOR_JOB_CHANGE = 0  # Return 0 if he says looking for job change
MAX_SIMILAR_LIMIT = 5000
PASSIVE = 12
MAX_FETCH_PROFILE = 1000 # Indicates the number of profiles need to fetch from elasticsearch

# ERROR CODES
# ERROR_CODE = -1  # Linkedin Profile not found
# ERROR_CODE = -2  # Currently not working.
# ERROR_CODE = -3  # Working in first company, hence skipping
# ERROR_CODE = -4  # No creasing
# ERROR_CODE = -5  # Creasing will remove all company experience
# ERROR_CODE = -6  # Cannot test, as end_date is not specified

prod_es = Elasticsearch('http://130.211.114.20:6200/')
es_index = "complete"
es_type = "user"
SCROLL_TIMEOUT_SECS = '500000s'
SCROLL_SIZE = 13


def next_peak_get_analysis_level_two(similar_user_job_durations, current_job_duration_in_months):
    print"***** Entering get_analysis_level_two *****"

    longer_job_durations = []

    # for jd in similar_user_job_durations:
    #     if jd - current_job_duration_in_months > -3:
    #         longer_job_durations.append(jd)

    for jd in similar_user_job_durations:
        if jd/DAYS_IN_MONTH >= current_job_duration_in_months:
            longer_job_durations.append(jd/DAYS_IN_MONTH)


    # print"Longer Job Durations ", longer_job_durations
    counter = collections.Counter(longer_job_durations)
    # print counter

    nearest_local_maxima = 0

    print "Printing Top frequencies", counter.most_common()
    # for x in counter.most_common(len(counter)):
    #     abs_diff_from_current_jd = abs(x[0] - current_job_duration_in_months)
    #     inp = raw_input("paused")
    #     if abs_diff_from_current_jd < MONTHS_IN_YEAR:
    #         nearest_local_maxima = x[0]
    #         print "updated Local Maxima ", nearest_local_maxima
    #         break


    for x in counter.most_common(len(counter)):
        if x[0] >= current_job_duration_in_months:
            nearest_local_maxima = x[0]
            print "updated local maxima", nearest_local_maxima
            break

    # probabilities = []
    # i = 0
    # max_probability = 0
    #
    # for x in counter.most_common(len(counter)):
    #
    #     peak = x[0]
    #
    #     # filtered_freqs = []
    #     total_people = 0
    #     filtered_people = 0
    #
    #     for freq in counter.most_common(len(counter)):
    #
    #         if freq[0] <= peak:
    #             # filtered_freqs.append(freq)
    #             filtered_people += freq[1]
    #         total_people += freq[1]
    #
    #     probability = float(filtered_people)/total_people
    #
    #     probabilities.append((peak,probability))
    #
    #     if probability > max_probability:
    #         max_probability = probability
    #         nearest_local_maxima = peak
    #
    #     i += 1
    #     if i >= 5:
    #         break
    #
    # print "first five probabilities", probabilities

    # print"Nearest Local Maxima ", nearest_local_maxima
    if nearest_local_maxima == 0:
        print "in nearest_local_maxima == 0 cond"
        print"Nearest Local Maxima ", ((current_job_duration_in_months / MONTHS_IN_YEAR) + 1) * MONTHS_IN_YEAR
        return ((current_job_duration_in_months / MONTHS_IN_YEAR) + 1) * MONTHS_IN_YEAR

    # if nearest_local_maxima < current_job_duration_in_months:
    #     print "in nearest_local_maxima < current_job_duration_in_months cond"
    #     # if the nearest local maxima found is less than current job duration
    #     # then use 25:75 to reply activeness
    #     if current_job_duration_in_months - nearest_local_maxima > MONTHS_IN_QUARTER_YEAR:
    #         print "1st cond Nearest Local Maxima ",((current_job_duration_in_months / MONTHS_IN_YEAR) + 1) * MONTHS_IN_YEAR
    #         return ((current_job_duration_in_months / MONTHS_IN_YEAR) + 1) * MONTHS_IN_YEAR
    #     else:
    #         print "2nd cond Nearest Local Maxima ",current_job_duration_in_months
    #         return current_job_duration_in_months

    return nearest_local_maxima







def og_get_analysis_level_two(similar_user_job_durations, current_job_duration_in_months):
    # print"***** Entering get_analysis_level_two *****"

    longer_job_durations = []

    for jd in similar_user_job_durations:
        if jd/DAYS_IN_MONTH - current_job_duration_in_months > -3:
            longer_job_durations.append(jd/DAYS_IN_MONTH)

    # print"Longer Job Durations ", longer_job_durations
    counter = collections.Counter(longer_job_durations)
    # print counter

    nearest_local_maxima = 0

    print "Printing Top frequencies", counter.most_common(5)
    for x in counter.most_common(len(counter)):
        abs_diff_from_current_jd = abs(x[0] - current_job_duration_in_months)
        if abs_diff_from_current_jd < MONTHS_IN_YEAR:
            nearest_local_maxima = x[0]
            break

    # print"Nearest Local Maxima ", nearest_local_maxima
    if nearest_local_maxima == 0:
        return ((current_job_duration_in_months / MONTHS_IN_YEAR) + 1) * MONTHS_IN_YEAR

    if nearest_local_maxima < current_job_duration_in_months:
        # if the nearest local maxima found is less than current job duration
        # then use 25:75 to reply activeness
        if current_job_duration_in_months - nearest_local_maxima > MONTHS_IN_QUARTER_YEAR:
            return ((current_job_duration_in_months / MONTHS_IN_YEAR) + 1) * MONTHS_IN_YEAR
        else:
            return current_job_duration_in_months

    return nearest_local_maxima





def get_job_durations(experiences, educations):

    job_arr = []
    ex_index = 0
    for experience in experiences:

        try:
            if experience['end_date'] is not None:
                if type(experience['end_date']) == str or type(experience['end_date']) == unicode:
                    end_date = datetime.strptime(experience['end_date'], "%Y-%m-%dT%H:%M:%S.%fZ")
                else:
                    end_date = experience['end_date']

            elif experience['current']:
                end_date = TRACE_BACK_YEAR
                # end_date = datetime.today()
            else:
                end_date = None
        except:
            end_date = None

        try:
            if experience['start_date'] is not None:
                if type(experience['start_date']) == str or type(experience['start_date']) == unicode:
                    start_date = datetime.strptime(experience['start_date'], "%Y-%m-%dT%H:%M:%S.%fZ")
                else:
                    start_date = experience['start_date']

            else:
                start_date = None
        except:
            start_date = None

        if start_date is not None and end_date is not None:
            job_arr.append((experience['title'].lower(), start_date, end_date,'ex',ex_index))
        ex_index = ex_index + 1

    edu_arr = []
    ed_index = 0

    for education in educations:

        try:
            if education['end_date']is not None:
                if type(education['end_date']) == str or type(education['end_date']) == unicode:
                    end_date = datetime.strptime(education['end_date'], "%Y-%m-%dT%H:%M:%S.%fZ")
                else:
                    end_date = education['end_date']
            else:
                end_date = None
        except:
            end_date = None

        try:
            if education['start_date'] is not None:
                if type(education['start_date']) == str or type(education['start_date']) == unicode:
                    start_date = datetime.strptime(education['start_date'], "%Y-%m-%dT%H:%M:%S.%fZ")
                else:
                    start_date = education['start_date']
            else:
                start_date = None
        except:
            start_date = None

        if start_date is not None and end_date is not None:
            edu_arr.append(('student', start_date, end_date,'ed',ed_index))

        ed_index = ed_index + 1

    job_edu_arr = job_arr + edu_arr
    job_edu_arr.sort(key=lambda x: x[1])

    job_ex = 0
    job_ex_arr = []
    intern_ex = 0
    intern_ex_arr = []
    if len(job_edu_arr) > 0:
        min_end = job_edu_arr[0][1]

    remove_index = {}
    for job_edu in job_edu_arr:

        title, start_date, end_date, cat, index = job_edu

        if min_end > start_date:
            start_date = min_end

        work_ex = (end_date - start_date).days
        if work_ex < 0:
            work_ex = 0

        intern_word = ['intern',
                       'visitor',
                       'visiting',
                       'education',
                       'internee',
                       'scholar',
                       'summer',
                       'scholar',
                       'recipient',
                       'volunteer']
        title_word = title.split()

        if 'student' in title:
            category = -1
        elif len(list(set(title_word).intersection(intern_word))) > 0:
            category = 1
            remove_index[cat] = index
        else:
            category = 0

        if work_ex !=0:
            if category == 0:
                job_ex += work_ex
                job_ex_arr.append(work_ex)
            elif category == 1:
                intern_ex += work_ex
                intern_ex_arr.append(work_ex)

        if min_end < end_date:
            min_end = end_date

    return job_ex_arr, intern_ex_arr, remove_index


def filter_duplicate_company(company_names, job_start_dates):

   def remove_duplicates(values):
      output = []
      seen = set()
      for i, value in enumerate(values):
         if value not in seen:
            output.append(value)
            seen.add(value)


      return output

   (filtered_company_names) = remove_duplicates(company_names)

   filtered_job_start_dates = []
   company_names.reverse()

   for name in filtered_company_names:

      ind = company_names.index(name)
      filtered_job_start_dates.append(job_start_dates[-ind-1])

   return (filtered_company_names,filtered_job_start_dates)


def create_target_array(doc):

    # components of target array = ['id', [degree], [colleges], [college_start_dates], [company names], [company profile title], [company_start_date], [company_end_date], [current_location], [major], [skills_set]]

    target=[]

    institute_name=[]
    institute_start_date=[]
    institute_degree=[]
    major = []
    skills_set = []
    current_location = ''
    current_industry = ''

    company_name=[]
    company_start_date=[]
    company_end_date=[]
    company_title=[]

    # job end dates, job profile tilte and job end date are merely for manual data tesing, they are not used in the algorithm

    id = doc['_source']['_id']
    if 'current_location' in doc['_source']['linkedin']:
        current_location = doc['_source']['linkedin']['current_location']['name']


    for education in doc['_source']['linkedin']['educations']:

        # grab degree having qualification as graduate, postgraduate and postgraduate diploma


        if 'qualification' in education:
            qualification = education['qualification']['name']
            if (qualification == 'Graduate' or qualification == 'Postgraduate' or qualification == 'Postgraduate Diploma'):

                institute_name.append(education['institute']['name'])

                if 'start_date' in education:
                    institute_start_date.append(education['start_date'])
                else:
                    institute_start_date.append(None)

                if 'degree' in education:
                    institute_degree.append(education['degree']['name'])
                else:
                    institute_degree.append(None)

                if 'major' in education:
                    major.append(education['major']['name'])
                else:
                    major.append(None)

    # if education does not have qualification as mentioned above, then grab all degrees without any qualification resemblance

    if ((not institute_degree) or institute_degree == [None]):
        institute_name = []
        institute_start_date = []
        major = []
        for education in doc['_source']['linkedin']['educations']:
            institute_name.append(education['institute']['name'])

            if 'start_date' in education:
                institute_start_date.append(education['start_date'])
            else:
                institute_start_date.append(None)

            if 'degree' in education:
                institute_degree.append(education['degree']['name'])
            else:
                institute_degree.append(None)

            if 'major' in education:
                major.append(education['major']['name'])
            else:
                major.append(None)

    # check if the first company start date is null, if yes, then grab next valid start  date as first company start date
    check_last_none = ''
    for experience in doc['_source']['linkedin']['experiences']:

        company_name.append(experience['company']['name'])
        company_title.append(experience['title'])


        if 'start_date' in experience:
            check_last_none = experience['start_date']
            company_start_date.append(check_last_none)
        else:
            company_start_date.append(None)
            check_last_none = None

        if 'end_date' in experience:
            company_end_date.append(experience['end_date'])
        else:
            company_end_date.append(None)

    if check_last_none == None:
        if company_start_date and company_start_date != [None]:
            length = len(company_start_date)

            valid_start_date = ''
            for i in xrange(-2,-length,-1):
                valid_start_date = company_start_date[i]
                if valid_start_date != None or valid_start_date != '':
                    company_start_date[-1] = valid_start_date
                    break

            if valid_start_date == '':
                company_start_date = []

    if 'skills' in doc['_source']['linkedin']:
        for skills in doc['_source']['linkedin']['skills']:
            skills_set.append(skills)

    if 'current_industry' in doc['_source']['linkedin']:
        if doc['_source']['linkedin']['current_industry'] != None and doc['_source']['linkedin']['current_industry'] != '':
            current_industry = doc['_source']['linkedin']['current_industry']

    target.append(id)
    target.append(institute_degree)
    target.append(institute_name)
    target.append(institute_start_date)
    target.append(company_name)
    target.append(company_title)
    target.append(company_start_date)
    target.append(company_end_date)
    target.append([current_location])
    target.append(major)
    target.append(skills_set)
    target.append([current_industry])

    # filter_duplicate_company function filters duplicate company names and thier start dates
    if target[4] and target[6]:
        (company_names, job_start_dates) = filter_duplicate_company(target[4], target[6])
        target[4] = company_names
        target[6] = job_start_dates

    # components of target array = ['id', [degree], [colleges], [college_start_dates], [company names], [company profile title], [company_start_date], [company_end_date], [current_location], [major], [skills_set]]

    # college_start_dates_num = []
    # if target[3] and target[3] != [None] and target[3] != ['']:
    #
    #     for date in target[3]:
    #
    #         if date != None and date != '':
    #
    #             date = re.match(r'^(\d{4})\-',date,re.I)
    #             date = date.group(1)
    #             date = int(date)
    #         else:
    #             date = None
    #
    #         college_start_dates_num.append(date)
    #
    # target.append(college_start_dates_num)
    #
    # company_start_dates_num = []
    # if target[6] and target[6] != [None] and target[6] != ['']:
    #
    #     for date in target[6]:
    #
    #         if date != None and date != '':
    #
    #             date = re.match(r'^(\d{4})\-',date,re.I)
    #             date = date.group(1)
    #             date = int(date)
    #         else:
    #             date = None
    #
    #         company_start_dates_num.append(date)
    #
    # target.append(company_start_dates_num)

    return(target)



def go_to_past_similar_profiles(profile):
    creased_education_index = 0
    new_educations = []

    for education in profile['linkedin']['educations']:

        if 'start_date' in education:

            if education['start_date'] != None and education['start_date'] != '':

                if type(education['start_date']) == str or type(education['start_date']) == unicode:
                    start_date = datetime.strptime(education['start_date'], "%Y-%m-%dT%H:%M:%S.%fZ")
                else:
                    start_date = education['start_date']

                if start_date > TRACE_BACK_YEAR:
                    creased_education_index = creased_education_index + 1
                    new_educations.append(education)
                # else:
                #     new_educations = profile['linkedin']['educations'][creased_education_index : len(education)]
                #     break


    new_experiences = []
    creased_experience_index = 0

    for experience in profile['linkedin']['experiences']:

        if 'start_date' in experience:

            if experience['start_date'] != None and experience['start_date'] != '':

                if type(experience['start_date']) == str or type(experience['start_date']) == unicode:
                    start_date = datetime.strptime(experience['start_date'], "%Y-%m-%dT%H:%M:%S.%fZ")
                else:
                    start_date = experience['start_date']

                if start_date < TRACE_BACK_YEAR:
                    new_experiences.append(experience)
                else:
                    creased_experience_index = creased_experience_index + 1
                #     new_experiences = profile['linkedin']['experiences'][creased_experience_index : len(experience)]
                #     break

    if not new_educations:
        new_educations = profile['linkedin']['educations']

    # actual_end_date = None
    # actual_start_date = None
    if not new_experiences:
        return(0)
    # else:
    # # if new_experiences:
    #     actual_end_date = new_experiences[0]['end_date']
    #     print "actual_end_date",actual_end_date
    #     actual_start_date = new_experiences[0]['start_date']
    #     new_experiences[0]['end_date'] = None
    #     new_experiences[0]['current'] = True

    profile['linkedin']['educations'] = copy.deepcopy(new_educations)
    profile['linkedin']['experiences'] = copy.deepcopy(new_experiences)

    return(profile)






def go_to_past(profile):#,similar_users):


    creased_education_index = 0
    new_educations = []
    for education in profile['_source']['linkedin']['educations']:

        if 'start_date' in education:

            if education['start_date'] != None and education['start_date'] != '':

                if type(education['start_date']) == str or type(education['start_date']) == unicode:
                    start_date = datetime.strptime(education['start_date'], "%Y-%m-%dT%H:%M:%S.%fZ")
                else:
                    start_date = education['start_date']

                if start_date > TRACE_BACK_YEAR:
                    creased_education_index = creased_education_index + 1
                else:
                    new_educations = profile['_source']['linkedin']['educations'][creased_education_index : len(education)]
                    break


    new_experiences = []
    creased_experience_index = 0

    for experience in profile['_source']['linkedin']['experiences']:

        if 'start_date' in experience:

            if experience['start_date'] != None and experience['start_date'] != '':

                if type(experience['start_date']) == str or type(experience['start_date']) == unicode:
                    start_date = datetime.strptime(experience['start_date'], "%Y-%m-%dT%H:%M:%S.%fZ")
                else:
                    start_date = experience['start_date']

                if start_date > TRACE_BACK_YEAR:
                    creased_experience_index = creased_experience_index + 1
                else:
                    new_experiences = profile['_source']['linkedin']['experiences'][creased_experience_index : len(experience)]
                    break

    print "creased_experience_index",creased_experience_index
    print "creased_education_index", creased_education_index

    if not new_educations:
        new_educations = profile['_source']['linkedin']['educations']

    actual_end_date = None
    actual_start_date = None
    if not new_experiences:
        return(-4)
    else:
        actual_end_date = new_experiences[0]['end_date']
        print "actual_end_date",actual_end_date
        actual_start_date = new_experiences[0]['start_date']
        new_experiences[0]['end_date'] = None
        new_experiences[0]['current'] = True

    profile['_source']['linkedin']['educations'] = copy.deepcopy(new_educations)
    profile['_source']['linkedin']['experiences'] = copy.deepcopy(new_experiences)

    print get_job_switch_months(profile,actual_start_date,actual_end_date)#,similar_users)


    # college_start_dates_num = target[11]
    # company_start_dates_num = target[12]
    #
    # job_count = len(target[6])
    #
    # if job_count < 1:
    #     return -2
    #
    # if len(target[6]) < MIN_JOB_NO:
    #     return -3
    #
    # # components of target array = ['id', [degree], [colleges], [college_start_dates], [company names], [company profile title], [company_start_date], [company_end_date], [current_location], [major], [skills_set]]
    #
    # creased_company_index = 0
    # creased_college_index = 0
    # new_target = []
    # actual_end_date = None
    # actual_start_date = None
    #
    # if (TRACE_BACK_JOB_NO != 0) and (TRACE_BACK_JOB_NO < len(company_start_dates_num) and TRACE_BACK_YEAR == 0):
    #
    #     creased_date = company_start_dates_num[TRACE_BACK_JOB_NO]
    #
    #     old_company_start_dates = target[6]
    #     new_company_start_dates = old_company_start_dates[TRACE_BACK_JOB_NO : len(old_company_start_dates)]
    #
    #     old_exp_title = target[5]
    #     new_exp_title = old_exp_title[TRACE_BACK_JOB_NO : len(old_exp_title)]
    #
    #     old_company_end_dates = target[7]
    #     new_company_end_dates = old_company_end_dates[TRACE_BACK_JOB_NO : len(old_company_end_dates)]
    #
    #     old_company_names = target[4]
    #     new_company_names = old_company_names[TRACE_BACK_JOB_NO : len(old_company_names)]
    #
    #     creased_index = 0
    #
    #     if college_start_dates_num:
    #         for index, date in enumerate(college_start_dates_num):
    #             if date != None and date != '':
    #                 if date > creased_date:
    #                     creased_index = index + 1
    #                 else:
    #                     break
    #
    #     old_college_start_dates = target[3]
    #
    #     new_college_start_dates = old_college_start_dates[creased_index : len(old_college_start_dates)]
    #
    #     old_degree_names = target[1]
    #     new_degree_names = old_degree_names[creased_index : len(old_degree_names)]
    #
    #     old_college_names = target[2]
    #     new_college_names = old_college_names[creased_index : len(old_college_names)]
    #
    #     old_major = target[9]
    #     new_major = old_major[creased_index : len(old_major)]
    #
    #     new_target = [target[0], new_degree_names, new_college_names, new_college_start_dates, new_company_names, new_exp_title, new_company_start_dates, new_company_end_dates, new_major, target[10]]
    #
    #     creased_company_index = TRACE_BACK_JOB_NO
    #     creased_college_index = creased_index
    #
    #     print creased_index
    #     actual_end_date = new_company_end_dates[0]
    #
    #
    # elif (TRACE_BACK_YEAR != 0 and TRACE_BACK_JOB_NO == 0):
    #
    #     creased_company_index = 0
    #
    #     for index, date in enumerate(company_start_dates_num):
    #         if date != None and date != '':
    #             if date > TRACE_BACK_YEAR:
    #                 creased_company_index = index + 1
    #             else:
    #                 break
    #
    #     old_company_start_dates = target[6]
    #
    #     if creased_company_index >= len(old_company_start_dates):
    #         return(-5)
    #
    #     new_company_start_dates = old_company_start_dates[creased_company_index : len(old_company_start_dates)]
    #
    #     old_exp_title = target[5]
    #     new_exp_title = old_exp_title[creased_company_index : len(old_exp_title)]
    #
    #     old_company_end_dates = target[7]
    #     new_company_end_dates = old_company_end_dates[creased_company_index : len(old_company_end_dates)]
    #
    #     old_company_names = target[4]
    #     new_company_names = old_company_names[creased_company_index : len(old_company_names)]
    #
    #     creased_college_index = 0
    #
    #     for index, date in enumerate(college_start_dates_num):
    #         if date != None and date != '':
    #             if date > TRACE_BACK_YEAR:
    #                 creased_college_index = index + 1
    #             else:
    #                 break
    #
    #     old_college_start_dates = target[3]
    #
    #     new_college_start_dates = old_college_start_dates[creased_college_index : len(old_college_start_dates)]
    #
    #     old_degree_names = target[1]
    #     new_degree_names = old_degree_names[creased_college_index : len(old_degree_names)]
    #
    #     old_college_names = target[2]
    #     new_college_names = old_college_names[creased_college_index : len(old_college_names)]
    #
    #     old_major = target[9]
    #     new_major = old_major[creased_college_index : len(old_major)]
    #
    #     new_target = [target[0], new_degree_names, new_college_names, new_college_start_dates, new_company_names, new_exp_title, new_company_start_dates, new_company_end_dates, target[8],new_major, target[10]]
    #
    #     actual_start_date = new_company_start_dates[0]
    #     actual_end_date = new_company_end_dates[0]
    #     print "creased_company_index", creased_company_index
    #     print "creased_college_index", creased_college_index
    #
    #
    #
    #
    # else:
    #     print "enter TRACE_BACK_JOB_NO or TRACE_BACK_YEAR_COUNT"
    #     exit(0)
    #
    #
    # if creased_college_index == 0 and creased_company_index == 0:
    #     return(-4)
    #
    #
    # educations = profile['_source']['linkedin']['educations']
    # experiences = profile['_source']['linkedin']['experiences']
    #
    # print "new1 exp", experiences
    #
    # # for i in xrange(0,creased_college_index-1):
    # #     del educations[0]
    # #
    # # for i in xrange(0,creased_company_index-1):
    # #     del experiences[0]
    # #
    # experiences[0]['current'] = True
    # experiences[0]['end_date'] = None
    #
    # new_educations = educations[creased_college_index : len(educations)]
    # new_experiences = experiences[creased_company_index : len(experiences)]
    #
    # new_experiences[0]['current'] = True
    # new_experiences[0]['end_date'] = None
    #
    #
    #
    # print "new1 exp", new_experiences
    #
    # print "ACTUAL END DATE", actual_end_date
    # print get_job_switch_months(new_target, profile,actual_end_date,year_str,new_educations,new_experiences)


def is_looking_for_job_change(profile):

    profile_text = profile['name'] if ('name' in profile) else ''
    profile_text += profile['headline'] if ('headline' in profile) else ''
    profile_text += profile['summary'] if ('summary' in profile) else ''
    if profile_text == '':
        return False

    # Code for checking if he has specifically written that he is
    # looking for job change or looking for opportunities.
    # active_text_list = ["looking for job",
    #                     "looking for opportunity",
    #                     "looking for opportunities",
    #                     "looking for change"
    #                     ]
    active_text_list = ["looking for"]
    for active_text in active_text_list:
        if active_text in profile_text:
            return True

    # Code for checking email id.
    email_matched = re.search(r'[\w\.-]+@[\w\.-]+', profile_text)
    if email_matched:
        return True

    # Code for checking phone number.
    # Assuming indian phone numbers
    if "+91" in profile_text:
        return True
    phone_matched = map(int, re.findall(r'(\d{10})', profile_text))
    if phone_matched:
        return True

    return False


def get_job_switch_months(profile,actual_start_date,actual_end_date):#,similar_users):
    print"***** Entering get_job_switch_months *****"

    profile_id = str(profile['_id'])

    if 'linkedin' not in profile['_source']:
        return -1

    if is_looking_for_job_change(profile):
        return LOOKING_FOR_JOB_CHANGE

    educations = profile['_source']['linkedin']['educations'] if ('educations' in profile['_source']['linkedin']) else []
    experiences = profile['_source']['linkedin']['experiences'] if ('experiences' in profile['_source']['linkedin']) else []

    job_durations, intern_durations, remove_index = get_job_durations(experiences, educations)

    new_experiences = []
    new_educations = []

    if remove_index:

        remove_ex_indexes = []
        remove_ed_indexes = []
        for key, value in remove_index.iteritems():

            if key == 'ex':
                remove_ex_indexes.append(value)
            if key == 'ed':
                remove_ed_indexes.append(value)

        if remove_ex_indexes:
            new_experiences = [value for index, value in enumerate(experiences) if index not in remove_ex_indexes]

        if remove_ed_indexes:
            new_educations = [value for index, value in enumerate(educations) if index not in remove_ed_indexes]

    if new_experiences:
        profile['_source']['linkedin']['experiences'] = copy.deepcopy(new_experiences)

    if new_educations:
        profile['_source']['linkedin']['educations'] = copy.deepcopy(new_educations)

    print "new profile", profile

    job_count = len(job_durations)

    if job_count < 1:
        return -2


    current_job_duration = job_durations[-1]
    current_job_duration_in_months = current_job_duration/DAYS_IN_MONTH
    print "current_job_duration", current_job_duration_in_months

    month_diff = None
    if (type(actual_end_date) == str or type(actual_end_date) == unicode) and (actual_end_date != None):
        actual_end_date = datetime.strptime(actual_end_date, "%Y-%m-%dT%H:%M:%S.%fZ")

        # actual_start_date = datetime.strptime(actual_start_date, "%Y-%m-%dT%H:%M:%S.%fZ")

        month_diff = (actual_end_date - TRACE_BACK_YEAR).days/DAYS_IN_MONTH

    print "*****Actual job month diff", month_diff

    if month_diff == None or month_diff <=0: #sharma
        return(-6)

    target = create_target_array(profile)

    print "new_target", target

    final_es_query = build_es_query(target)

    # print final_es_query
    # exit(0)

    # final_es_query = {"query":{"function_score":{"query":{"bool":{"must":[{"nested":{"path":"linkedin.educations","query":{"bool":{"should":[{"bool":{"must":[{"match":{"linkedin.educations.degree.name":"Bachelor of Science (B.Sc.)"}},{"range":{"linkedin.educations.start_date":{"gte":"1982-01-01T00:00:00.000Z||-72M","lte":"1982-01-01T00:00:00.000Z||+12M"}}}]}}]}}}},{"nested":{"path":"linkedin.experiences","query":{"bool":{"must":[{"range":{"linkedin.experiences.start_date":{"gte":"1998-01-01T00:00:00.000Z||-48M","lte":"1998-01-01T00:00:00.000Z||+12M"}}},{"range":{"linkedin.experiences.end_date":{"gte":"2008-01-01T00:00:00.000Z||-48M","lte":"2008-01-01T00:00:00.000Z||+12M"}}}]}}}},{"terms":{"linkedin.skills.raw":["CTI Group","Telecommunications","Unified Communications","Enterprise Software","VoIP","Managed Services","Integration","Call Logging","SIP","Call Management","Cloud Computing","Project Delivery","Solution Architecture","Pre-sales","Telephony","Solution Selling","SaaS","Data Center","Mobile Devices","Cisco Technologies","Software Development","Product Management","Requirements Analysis","Service Delivery","Project Management","Vendor Management","Call Centers","IVR","IP","Call Center","SmartInteraction Suite","Proteus Suite","BroadSoft Integration","Analysis Software","Telecoms Data Analysis","Carrier Grade Software","Global Provider","Hosted Communications","eBilling","Call Accounting","Telecom Solutions","Trading Platforms","PBX","Channel Partners","Technology Partners","SmartListen","SmartRecord","SmartEvaluate","SmartCapture","Multitenant Solutions"],"minimum_match":6}}]}},"functions":[{"filter":{"nested":{"path":"linkedin.educations","query":{"bool":{"must":[{"match":{"linkedin.educations.degree.name":"Bachelor of Science (B.Sc.)"}}]}}}},"weight":28},{"filter":{"nested":{"path":"linkedin.educations","query":{"term":{"linkedin.educations.institute.name.raw":{"value":"University of Westminster (UW)"}}}}},"weight":12},{"filter":{"nested":{"path":"linkedin.educations","query":{"bool":{"must":[{"match":{"linkedin.educations.degree.name":"Bachelor of Science (B.Sc.)"}},{"range":{"linkedin.educations.start_date":{"gte":"1982-01-01T00:00:00.000Z","lte":"1982-01-01T00:00:00.000Z||+11M"}}}]}}}},"weight":3.2},{"filter":{"nested":{"path":"linkedin.educations","query":{"bool":{"must":[{"match":{"linkedin.educations.degree.name":"Bachelor of Science (B.Sc.)"}},{"range":{"linkedin.educations.start_date":{"gte":"1982-01-01T00:00:00.000Z||-12M","lte":"1982-01-01T00:00:00.000Z||-1M"}}}]}}}},"weight":9.6},{"filter":{"nested":{"path":"linkedin.educations","query":{"bool":{"must":[{"match":{"linkedin.educations.degree.name":"Bachelor of Science (B.Sc.)"}},{"range":{"linkedin.educations.start_date":{"gte":"1982-01-01T00:00:00.000Z||-24M","lte":"1982-01-01T00:00:00.000Z||-13M"}}}]}}}},"weight":16},{"filter":{"nested":{"path":"linkedin.educations","query":{"bool":{"must":[{"match":{"linkedin.educations.degree.name":"Bachelor of Science (B.Sc.)"}},{"range":{"linkedin.educations.start_date":{"gte":"1982-01-01T00:00:00.000Z||-36M","lte":"1982-01-01T00:00:00.000Z||-25M"}}}]}}}},"weight":12.8},{"filter":{"nested":{"path":"linkedin.educations","query":{"bool":{"must":[{"match":{"linkedin.educations.degree.name":"Bachelor of Science (B.Sc.)"}},{"range":{"linkedin.educations.start_date":{"gte":"1982-01-01T00:00:00.000Z||-48M","lte":"1982-01-01T00:00:00.000Z||-37M"}}}]}}}},"weight":6.4},{"filter":{"nested":{"path":"linkedin.experiences","query":{"match_phrase":{"linkedin.experiences.company.name":"CTI Group"}}}},"weight":7},{"filter":{"nested":{"path":"linkedin.experiences","query":{"match_phrase":{"linkedin.experiences.company.name":"CTI Data Solutions"}}}},"weight":7},{"filter":{"term":{"linkedin.current_location":"Blackburn"}},"weight":4},{"script_score":{"script":"f_score=0;total=0;def date='';target_year=1998;target_no_jobs=2;for(exp in _source.linkedin.experiences){total=total+1;if(exp.containsKey('start_date')){date=exp['start_date'];};};if(total<=target_no_jobs){return(-100);};if(date!=''){def year = date[0..3].toInteger();year = year.toInteger();def diff=year-target_year;if((-4<diff) && (diff<0)){f_score=Math.abs(diff)*3;};else{return(-100);};};def job_diff=Math.abs(target_no_jobs-total+1);job_score=target_no_jobs-job_diff;job_score=(job_score/target_no_jobs)*11;return(f_score+job_score);"}}],"score_mode":"sum","boost_mode":"replace"}},"size":10}



    # target_job_start_dates = target[6]
    # target_job_end_dates = target[7]
    #
    # target_job_start_dates.reverse()
    # target_job_end_dates.reverse()



    try:
        similar_users = prod_es.search(index=es_index, doc_type=es_type, body=final_es_query, request_timeout=30000)
#        result = db.save({"target_id": profile_id,"similar_profiles":similar_users})
#        print "data saved", result
    except Exception as e:
        print e
        return(0)


    if similar_users['hits']['total'] > 0:
        print "profiles fetched", similar_users['hits']['total']
        if similar_users['hits']['total'] < 10: #sharma
            return(-7)
        similar_users = similar_users['hits']['hits']

    similar_user_job_durations = []
    same_user_job_durations = []
    closeness_array = [] #sharma
    candidate_job_durs = []
    for similar_user in similar_users:
        # print similar_user
        if '_source' in similar_user:
            similar_user = similar_user['_source']

            # reminder for khaleeque: include nth term

            closeness = None
            if 'experiences' in similar_user['linkedin'] and len(similar_user['linkedin']['experiences']) >= job_count:

#                cropped_similar_user = go_to_past_similar_profiles(similar_user)
                cropped_similar_user = similar_user
                if isinstance(cropped_similar_user,dict):

                    jds, ids, remove_index = get_job_durations(cropped_similar_user['linkedin']['experiences'], cropped_similar_user['linkedin']['educations'])

                    if len(jds) >= job_count:

                        candi_job_dur_to_write = None

                        for index, job_duration in enumerate(job_durations):

                            candidate_job_dur = jds[index]
                            diff = abs(candidate_job_dur - job_duration)/DAYS_IN_MONTH
                            if closeness ==None:
                                closeness = 0
                            closeness += diff
                            # print "calc diff",job_duration,candidate_job_dur,diff,closeness,jds
                            candi_job_dur_to_write = candidate_job_dur/DAYS_IN_MONTH

                        # if candi_job_dur_to_write != None and closeness != None:
                        #     candidate_job_durs.append(candi_job_dur_to_write)
                        # else:
                        #     continue
                        #
                        # score = 0
                        # if closeness != None:
                        #
                        #     closeness_array.append(closeness) #sharma
                        #
                        #     cond = float(closeness)/job_count
                        #
                        #     if cond <= 2:
                        #         score = 1
                        #     else:
                        #         score = 2*(math.exp(-0.347*cond))

                        # print jds
                        if len(jds) > job_count:
                            # print "similar_user", similar_user['_id']
                            similar_user_job_durations.append((jds[job_count-1]))#,score))

                        elif len(jds) == job_count:
                            same_user_job_durations.append(jds[job_count-1])

                        # print "\n##############################################\n"


    # reg_str_write = profile_id #sharma

    # for closeness in closeness_array:
    #     reg_str_write = reg_str_write + "," + str(closeness)
    # reg_str_write = reg_str_write + "," + month_diff + "\n"

    # reg_str_write = reg_str_write + "\n"

    # reg_inp.write(reg_str_write)

    # reg_str_out_write = str(month_diff) + "\n"
    # reg_out.write(reg_str_out_write)

    # reg_out_curr_job_dur_write = str(current_job_duration/DAYS_IN_MONTH) + "\n"
    # reg_out_curr_job_dur.write(reg_out_curr_job_dur_write)

    # reg_out_total_job_dur_write = str((current_job_duration/DAYS_IN_MONTH)+month_diff) + "\n"
    # reg_out_total_job_dur.write(reg_out_total_job_dur_write)

    # reg_candidate_job_durs_write = profile_id
    # for candi in candidate_job_durs:
    #     reg_candidate_job_durs_write = reg_candidate_job_durs_write + "," +str(candi)

    # reg_candidate_job_durs_write = reg_candidate_job_durs_write + "\n"

    # reg_output_candidate_job_durs.write(reg_candidate_job_durs_write)

    similar_users_count = len(similar_user_job_durations)

    print"Printing number of similar users : ", similar_users_count

    # Performing Analysis after getting similar users

    print "current_job_duration", current_job_duration_in_months
    print "*****Actual job month diff", month_diff

    # print "similar_user_job_durations", similar_user_job_durations

    # If no similar user is found, What we can suggest is that
    # he will complete his this year if more than 3 moths passed
    # else ready to switch (25:75 thing)
    if (similar_users_count == 0 ) or (similar_users_count < MIN_SIMILAR_USERS_REQUIRED):
        similar_users_count = len(same_user_job_durations)
        if similar_users_count < MIN_SIMILAR_USERS_REQUIRED:

            print "similar users less than MIN_SIMILAR_USERS_REQUIRED"

            months_into_current_job = (current_job_duration/DAYS_IN_MONTH) % MONTHS_IN_YEAR
            if months_into_current_job > MONTHS_IN_QUARTER_YEAR:
                job_switch_months_left = MONTHS_IN_YEAR - months_into_current_job
                str_write = profile_id + "," + str(month_diff) + "," + str(job_switch_months_left) + "\n"
                # f.write(str_write)
            else:
                job_switch_months_left = 0
                print "Months Remaining to Switch", job_switch_months_left
                str_write = profile_id + "," + str(month_diff) + "," + str(job_switch_months_left) + "\n"
                # f.write(str_write)
            return job_switch_months_left
        else:
            minimum_working_duration = get_minimum_working_duration(same_user_job_durations, current_job_duration_in_months)
            if minimum_working_duration - current_job_duration_in_months > 3:
                str_write = profile_id + "," + str(month_diff) + "," + str(PASSIVE) + "\n"
                # f.write(str_write)
                return PASSIVE
            else:
                str_write = profile_id + "," + str(month_diff) + "," + str(minimum_working_duration - current_job_duration_in_months + 1) + "\n"
                # f.write(str_write)
                return minimum_working_duration - current_job_duration_in_months + 1

    # Doing some more analysis over the returned array of job durations.

    # similar_user_job_durations = [x/DAYS_IN_MONTH for x in similar_user_job_durations]
    # print "similar_user_job_durations in months", similar_user_job_durations
    # print "same_user_job_durations", same_user_job_durations
    optimal_job_duration = og_get_analysis_level_two(similar_user_job_durations, current_job_duration_in_months)
    months_remaining_to_switch = optimal_job_duration - current_job_duration_in_months

    print "Months Remaining to Switch", months_remaining_to_switch
    print "RESULT*********************************************"
    str_write = profile_id + "," + str(month_diff) + "," + str(months_remaining_to_switch) + "\n"
    print str_write
    f.write(str_write)
    if months_remaining_to_switch < 1:
        return 0
    return months_remaining_to_switch


def modified_build_es_query(target):

    # components of target array = ['id', [degree], [colleges], [college_start_dates], [company names], [company profile title], [company_start_date], [company_end_date], [current_location], [major], [skills_set]]

    print "new_target",target

    all_degree_query = [] # it contains the AND query components for education (i.e degrees,major and start_date)
    degree_filter_only = [] # it contains the filter weight query for degrees and major(if exists)
    considered_degrees = [] # out of all valid and rubbish degree names, which degrees were chosen is maintained here using their index value
    filters = [] # it contains all the filter weights for used in es query
    considered_filter_degree = [] # it contains the index of considered degree filter

    # check if target degrees exists, if yes then check for corresponding major_name and start_date

    degree_query = {
        "bool": {
            "must": []
        }
    }

    education_index = max(len(target[1]),len(target[3]),len(target[9])) - 1

    for index in xrange(0,education_index):

        if target[1] and target[1] != [None]:
            if target[1][index] != None and target[1][index] != '':

                input_degree = target[1][index]

                degree_query['bool']['must'].append(
                    {
                        "term":{
                            "linkedin.educations.degree.name": input_degree.encode('utf8')
                        }
                    }
                )

        if target[9] and target[9] != [None]:
            if target[9][index] != None and target[9][index] != '':

                input_major = target[9][index]

                degree_query['bool']['must'].append(
                    {
                        "term":{
                            "linkedin.educations.major.name": input_major.encode('utf8')
                        }
                    }
                )

        if target[3] and target[3] != [None]:
            if target[3][index] != None and target[3][index] != '':

                input_college_start_date = target[3][index]

                degree_query['bool']['must'].append({
                       "range": {
                          "linkedin.educations.start_date": {
                             "gte": input_college_start_date.encode('utf8') + "||-72M",
                             "lte": input_college_start_date.encode('utf8') + "||+12M",
                          }
                       }
                    })

        all_degree_query.append(degree_query)






    filter_deg_query = {
        "filter":{
            "nested": {
                "path": "linkedin.educations",
                "query": {
                    "bool": {
                        "must": []
                    }
                }
            }
        }
    }

    if education_index:

        each_degree_weight = 28/float(education_index+1)

        for index in xrange(0,education_index):

            if target[1] and target[1] != [None]:
                if target[1][index] != None and target[1][index] != '':

                    input_degree = target[1][index]

                    filter_deg_query['filter']['nested']['query']['bool']['must'].append(
                        {
                            "term":{
                                "linkedin.educations.degree.name": input_degree.encode('utf8')
                            }
                        }
                    )

            if target[9] and target[9] != [None]:
                if target[9][index] != None and target[9][index] != '':

                    input_major = target[9][index]

                    filter_deg_query['filter']['nested']['query']['bool']['must'].append(
                        {
                            "term":{
                                "linkedin.educations.major.name": input_major.encode('utf8')
                            }
                        }
                    )

            if target[3] and target[3] != [None]:
                if target[3][index] != None and target[3][index] != '':

                    input_college_start_date = target[3][index]

                    filter_deg_query['filter']['nested']['query']['bool']['must'].append({
                           "range": {
                              "linkedin.educations.start_date": {
                                 "gte": input_college_start_date.encode('utf8') + "||-72M",
                                 "lte": input_college_start_date.encode('utf8') + "||+12M",
                              }
                           }
                        })



            filters.append(filter_deg_query)
























    # if target[1] and target[1] != [None]:
    #
    #    for index, input_degree in enumerate(target[1]):
    #
    #        if input_degree != None:
    #
    #           degree_query = {
    #              "bool": {
    #                 "must": [
    #                    {
    #                       "match": {
    #                          "linkedin.educations.degree.name": input_degree.encode('utf8')
    #                       }
    #                    }
    #                 ]
    #              }
    #            }
    #
    #           # check if major exists for corresponding degree
    #
    #           if target[9] and target[9][index] != None and target[9][index] != '':
    #
    #              major = target[9][index]
    #              degree_query['bool']['must'].append({
    #                 "match": {
    #                    "linkedin.educations.major.name": major.encode('utf8')
    #                 }
    #              })
    #
    #           # check if start_date exists for corresponding degree
    #
    #           if target[3] and target[3][index] != None and target[3][index] != '':
    #
    #              degree_start_date = target[3][index]
    #              degree_query['bool']['must'].append({
    #                    "range": {
    #                       "linkedin.educations.start_date": {
    #                          "gte": degree_start_date.encode('utf8') + "||-72M",
    #                          "lte": degree_start_date.encode('utf8') + "||+12M",
    #                       }
    #                    }
    #                 })
    #
    #           all_degree_query.append(degree_query)
    #
    #           considered_degrees.append(index)
    #
    #
    # # if degree array does not exist, then coose major names and start date only
    # elif target[1] == [None] or (not target[1]):
    #
    #    print "NOT A PERFECT PROFILE" # because it does not have degree mentioned
    #
    #    # check if major exists for corresponding degree
    #
    #    if target[9] and target[9] != [None] and target[9] != '':
    #
    #       for index, major in enumerate(target[9]):
    #
    #          if major != None and major != '':
    #
    #              degree_query = {
    #                 "bool": {
    #                    "must": [
    #                       {
    #                          "match": {
    #                             "linkedin.educations.major.name": major.encode('utf8')
    #                          }
    #                       }
    #                    ]
    #                 }
    #               }
    #
    #              # check if start_date exists for corresponding degree
    #
    #              if target[3] and target[3][index] != None and target[3]['index'] != '':
    #
    #                 degree_start_date = target[3][index]
    #                 degree_query['bool']['must'].append({
    #                       "range": {
    #                          "linkedin.educations.start_date": {
    #                             "gte": degree_start_date.encode('utf8') + "||-72M",
    #                             "lte": degree_start_date.encode('utf8') + "||+12M",
    #                          }
    #                       }
    #                    })
    #
    #              all_degree_query.append(degree_query)
    #
    #              considered_degrees.append(index)
    #
    #    # if degree_name and major_name does not exist, then select start_date only
    #
    #    elif (target[9] == [None] or (not target[9]) or target[9] == ['']) and (target[3] and target[3] != [None] and target[3] != ['']):
    #
    #       for index, degree_start_date in enumerate(target[3]):
    #
    #          if degree_start_date != None and degree_start_date != '':
    #
    #              degree_query = {
    #                 "bool": {
    #                    "must": [
    #                       {
    #                          "range": {
    #                             "linkedin.educations.start_date": {
    #                                "gte": degree_start_date.encode('utf8') + "||-72M",
    #                                "lte": degree_start_date.encode('utf8') + "||+12M",
    #                             }
    #                          }
    #                       }
    #                    ]
    #                 }
    #               }
    #
    #              all_degree_query.append(degree_query)
    #
    #              considered_degrees.append(index)



    # create filters for degree with major if exists with specific weights

    if considered_degrees:
        each_degree_weight = 28/float(len(considered_degrees))

        if target[1] and target[1] != [None]:

           for index, input_degree in enumerate(target[1]):

              if input_degree != None and input_degree != '':

                  filter_deg_query = {
                      "filter":{
                         "nested": {
                            "path": "linkedin.educations",
                            "query": {
                               "bool": {
                                 "must": [
                                    {
                                       "match": {
                                          "linkedin.educations.degree.name": input_degree.encode('utf8')
                                       }
                                    }
                                 ]
                              }
                            }
                         }
                      },
                      "weight": each_degree_weight
                   }


                  if target[9] and target[9][index] != None and target[9][index] != '':

                     major = target[9][index]
                     filter_deg_query['filter']['nested']['query']['bool']['must'].append({
                        "match": {
                           "linkedin.educations.major.name": major.encode('utf8')
                        }
                     })

                  filters.append(filter_deg_query)
                  degree_filter_only.append(filter_deg_query)
                  considered_filter_degree.append(index)

        # create filter for major, if degree does not exist with specific weights

        elif target[1] == [None] or (not target[1]):

           if target[9] and target[9] != [None] and target[9] != ['']:
              each_degree_weight = 28/float(len(target[9]))

              for index, major in enumerate(target[9]):

                  if major != None and major != '':

                      filter_deg_query = {
                          "filter":{
                             "nested": {
                                "path": "linkedin.educations",
                                "query": {
                                   "bool": {
                                     "must": [
                                        {
                                           "match": {
                                              "linkedin.educations.major.name": major.encode('utf8')
                                           }
                                        }
                                     ]
                                  }
                                }
                             }
                          },
                          "weight": each_degree_weight
                       }

                      filters.append(filter_deg_query)
                      degree_filter_only.append(filter_deg_query)
                      considered_filter_degree.append(index)


    # create filter for colleges for consisdered degrees only, (i.e ignoring school names) with specific weights

    if considered_degrees:
       each_college_weight = 12/float(len(considered_degrees))
       for index in considered_degrees:

          if target[2] and target[2][index] != None and target[2][index] != '' :

             input_college = target[2][index]

             filter_college = {
                 "filter":{
                    "nested": {
                       "path": "linkedin.educations",
                       "query": {
                          "term": {
                             "linkedin.educations.institute.name.raw": {
                                "value": input_college.encode('utf8')
                             }
                          }
                       }
                    }
                 },
                "weight": each_college_weight
              }

             filters.append(filter_college)

    # if we did not catch considered degrees, then grab all the institue names as college degrees and create their filters with specific weights

    elif target[2] and target[2] != [None]:
       each_college_weight = 12/float(len(target[2]))
       for index, college_name in enumerate(target[2]):

          input_college = target[2][index]

          if input_college != None and input_college != '':

              filter_college = {
                  "filter":{
                     "nested": {
                        "path": "linkedin.educations",
                        "query": {
                           "term": {
                              "linkedin.educations.institute.name.raw": {
                                 "value": input_college.encode('utf8')
                              }
                           }
                        }
                     }
                  },
                 "weight": each_college_weight
               }

              filters.append(filter_college)



    # create filter for college start date using degree name, major name and start date if exists
    # created each filter covering all the time duration individually, as there was no other means to ...
    # do it except in script by looping in education of each profile which would be very time consuming

    if degree_filter_only:
        degree_filter_only_length = len(degree_filter_only)
        for index, fil_deg in enumerate(degree_filter_only):

            fil_index = considered_filter_degree[index]

            if target[3][fil_index] != None and target[3][fil_index] != '':

                degree_start_date = target[3][fil_index]

                filter_college_start_date = copy.deepcopy(fil_deg)

                filter_college_start_date['filter']['nested']['query']['bool']['must'].append(
                  {
                     "range": {
                        "linkedin.educations.start_date": {
                           "gte": degree_start_date.encode('utf8'),
                           "lte": degree_start_date.encode('utf8') + "||+11M"
                        }
                     }
                  })


                filter_college_start_date['weight'] = 16*0.2/float(degree_filter_only_length)

                filters.append(filter_college_start_date)


                filter_college_start_date1 = copy.deepcopy(fil_deg)
                filter_college_start_date1['filter']['nested']['query']['bool']['must'].append(
                    {
                     "range": {
                         "linkedin.educations.start_date": {
                            "gte": degree_start_date.encode('utf8') + "||-12M",
                            "lte": degree_start_date.encode('utf8') + "||-1M"
                         }
                      }
                    })

                filter_college_start_date1['weight'] = 16*0.6/degree_filter_only_length

                filters.append(filter_college_start_date1)


                filter_college_start_date2 = copy.deepcopy(fil_deg)
                filter_college_start_date2['filter']['nested']['query']['bool']['must'].append(
                    {
                     "range": {
                         "linkedin.educations.start_date": {
                            "gte": degree_start_date.encode('utf8') + "||-24M",
                            "lte": degree_start_date.encode('utf8') + "||-13M"
                         }
                      }
                    })

                filter_college_start_date2['weight'] = 16/degree_filter_only_length


                filters.append(filter_college_start_date2)


                filter_college_start_date3 = copy.deepcopy(fil_deg)
                filter_college_start_date3['filter']['nested']['query']['bool']['must'].append(
                    {
                     "range": {
                         "linkedin.educations.start_date": {
                            "gte": degree_start_date.encode('utf8') + "||-36M",
                            "lte": degree_start_date.encode('utf8') + "||-25M"
                         }
                      }
                    })

                filter_college_start_date3['weight'] = 16*0.8/degree_filter_only_length

                filters.append(filter_college_start_date3)


                filter_college_start_date4 = copy.deepcopy(fil_deg)
                filter_college_start_date4['filter']['nested']['query']['bool']['must'].append(
                    {
                     "range": {
                         "linkedin.educations.start_date": {
                            "gte": degree_start_date.encode('utf8') + "||-48M",
                            "lte": degree_start_date.encode('utf8') + "||-37M"
                         }
                      }
                    })

                filter_college_start_date4['weight'] = 16*0.4/degree_filter_only_length

                filters.append(filter_college_start_date4)

                filter_college_start_date = []
                filter_college_start_date1 = []
                filter_college_start_date2 = []
                filter_college_start_date3 = []
                filter_college_start_date4 = []

















    # create filter for company names with specific weights

    company_length = 0
    if target[4] and target[4] != [None] and target[4] != '':

       company_length = len(target[4])
       for company_name in target[4]:

           if company_name != None and company_name != '':

               filter_company_name = {
                   "filter": {
                      "nested": {
                         "path": "linkedin.experiences",
                         "query": {
                            "match_phrase": {
                               "linkedin.experiences.company.name": company_name.encode('utf8')
                            }
                         }
                      }
                   },
                   "weight": 14/float(company_length)
                }

               filters.append(filter_company_name)

    # create filter for current location with specific weights

    curr_loc_name = target[8][0]
    if curr_loc_name != None and curr_loc_name != '':
        filter_curr_loc = {
        "filter": {
           "term": {
              "linkedin.current_location": curr_loc_name.encode('utf8')
           }
        },
        "weight": 4
        }

        filters.append(filter_curr_loc)




    # create AND component having skill set

    skill_query = ''
    if target[10] and target[10] != [None] and target[10] != ['']:
       min_match = 0.4*len(target[10])

       min_match = int(min_match)

       if min_match > 6:
           min_match = 6

       skill_query = {
          "terms": {
             "linkedin.skills.raw": target[10],"minimum_match": min_match
          }
       }



    es_input_query = {
      "bool": {
         "must": []
      }
    }

    # if degree AND components created then append in the es query
    if all_degree_query:

        es_input_query['bool']['must'].append(
            {
                "nested": {
                  "path": "linkedin.educations",
                  "query": {
                      "bool": {
                         "should": [
                         ]
                      }
                   }
               }
            }
        )

        for degree in all_degree_query:
            es_input_query['bool']['must'][0]['nested']['query']['bool']['should'].append(degree)

    # if skills_set AND component created above, then append in the es query
    if skill_query:
        es_input_query['bool']['must'].append(skill_query)


    # if degree and skills_set AND components created above, then only create filter having scripting score, ...
    # otherwise the script would run on huge number of records if any of the AND components does not exist
    if (all_degree_query) and (skill_query):

        if target[6] and target[6][-1] != None and target[6][-1] != '' and company_length != 0:
            first_job_start_date = target[6][-1]
            first_job_start_date = re.match(r'^(\d{4})\-',first_job_start_date,re.I)
            first_job_start_date = first_job_start_date.group(1)

            first_job_start_date = str(first_job_start_date)
            company_length = str(company_length)

            # inp = raw_input("script_score")

            filters.append(
                {
                   "script_score": {
                      "script" : "f_score=0;total=0;def date='';target_year=" + first_job_start_date + ";target_no_jobs=" + company_length + ";for(exp in _source.linkedin.experiences){total=total+1;if(exp.containsKey('start_date')){date=exp['start_date'];};};match=date=~/^(?<year>\\d{4})\\-/;if(match){def year=match.group('year').toInteger();def diff=year-target_year;if((-4<diff) && (diff<0)){f_score=Math.abs(diff);f_score=f_score*3;};else{return(-100);};if(f_score<0){f_score=0};};def job_diff=Math.abs(target_no_jobs-total+1);job_score=target_no_jobs-job_diff;job_score=(job_score/target_no_jobs)*11;if(job_score<0){job_score=0;};f_score=f_score+job_score;return(f_score);"

                      # "script" : "f_score=0;total=0;def date='';target_year=" + first_job_start_date + ";target_no_jobs=" + company_length + ";for(exp in _source.linkedin.experiences){total=total+1;if(exp.containsKey('start_date')){date=exp['start_date'];};};if(total<=target_no_jobs){return(-100)};match=date=~/^(?<year>\\d{4})\\-/;if(match){def year=match.group('year').toInteger();def diff=Math.abs(target_year-year);if(diff<5){f_score=5-diff;f_score=f_score*3;};if(f_score<0){f_score=0};};def job_diff=Math.abs(target_no_jobs-total+1);job_score=target_no_jobs-job_diff;job_score=(job_score/target_no_jobs)*11;if(job_score<0){job_score=0;};f_score=f_score+job_score;return(f_score);"
                       # "script": "f_score=0;total=0;def date='';target_year=" + first_job_start_date + ";target_no_jobs=" + company_length + ";for(exp in _source.linkedin.experiences){total=total+1;if(exp.containsKey('start_date')){date=exp['start_date'];};};match=date=~/^(?<year>\\d{4})\\-/;if(match){def year=match.group('year').toInteger();def diff=Math.abs(target_year-year);if(diff<5){f_score=5-diff;f_score=f_score*3;};if(f_score<0){f_score=0};};def job_diff=Math.abs(target_no_jobs-total+1);job_score=target_no_jobs-job_diff;job_score=(job_score/target_no_jobs)*11;if(job_score<0){job_score=0;};f_score=f_score+job_score;return(f_score);"
                   }
                }
                )

    # if degree and skills_set AND component are NOT created then simply match_all the es query with the assigned filters

    if (not all_degree_query) and (not skill_query):
        es_input_query = {
            "match_all": {}
        }

        print "NOT A PERFECT PROFILE"





    print "es_input_query : ", es_input_query
    print "\n"
    print "filters : ", filters
    print "\n"

    final_es_query = {
           "size": MAX_FETCH_PROFILE,
           "query": {
              "function_score": {
                 "query": es_input_query,
                 "functions": filters,
                 "score_mode": "sum",
                 "boost_mode": "replace"
              }
           }
        }

    print "final_es_query : ", final_es_query

    return final_es_query


def build_es_query(target):

    # components of target array = ['id', [degree], [colleges], [college_start_dates], [company names], [company profile title], [company_start_date], [company_end_date], [current_location], [major], [skills_set]]

    all_degree_query = [] # it contains the AND query components for education (i.e degrees,major and start_date)
    degree_filter_only = [] # it contains the filter weight query for degrees and major(if exists)
    considered_degrees = [] # out of all valid and rubbish degree names, which degrees were chosen is maintained here using their index value
    filters = [] # it contains all the filter weights for used in es query
    considered_filter_degree = [] # it contains the index of considered degree filter

    # check if target degrees exists, if yes then check for corresponding major_name and start_date
    if target[1] and target[1] != [None]:

       for index, input_degree in enumerate(target[1]):

           if input_degree != None:

              degree_query = {
                 "bool": {
                    "must": [
                       {
                          "match": {
                             "linkedin.educations.degree.name": input_degree.encode('utf8')
                          }
                       }
                    ]
                 }
               }

              # check if major exists for corresponding degree

              if target[9] and target[9][index] != None and target[9][index] != '':

                 major = target[9][index]
                 degree_query['bool']['must'].append({
                    "match": {
                       "linkedin.educations.major.name": major.encode('utf8')
                    }
                 })

              # check if start_date exists for corresponding degree

              if target[3] and target[3][index] != None and target[3][index] != '':

                 degree_start_date = target[3][index]
                 degree_query['bool']['must'].append({
                       "range": {
                          "linkedin.educations.start_date": {
                             "gte": degree_start_date.encode('utf8') + "||-72M",
                             "lte": degree_start_date.encode('utf8') + "||+12M",
                          }
                       }
                    })

              all_degree_query.append(degree_query)

              considered_degrees.append(index)


    # if degree array does not exist, then coose major names and start date only
    elif target[1] == [None] or (not target[1]):

       print "NOT A PERFECT PROFILE" # because it does not have degree mentioned

       # check if major exists for corresponding degree

       if target[9] and target[9] != [None] and target[9] != '':

          for index, major in enumerate(target[9]):

             if major != None and major != '':

                 degree_query = {
                    "bool": {
                       "must": [
                          {
                             "match": {
                                "linkedin.educations.major.name": major.encode('utf8')
                             }
                          }
                       ]
                    }
                  }

                 # check if start_date exists for corresponding degree

                 if target[3] and target[3][index] != None and target[3]['index'] != '':

                    degree_start_date = target[3][index]
                    degree_query['bool']['must'].append({
                          "range": {
                             "linkedin.educations.start_date": {
                                "gte": degree_start_date.encode('utf8') + "||-72M",
                                "lte": degree_start_date.encode('utf8') + "||+12M",
                             }
                          }
                       })

                 all_degree_query.append(degree_query)

                 considered_degrees.append(index)

       # if degree_name and major_name does not exist, then select start_date only

       elif (target[9] == [None] or (not target[9]) or target[9] == ['']) and (target[3] and target[3] != [None] and target[3] != ['']):

          for index, degree_start_date in enumerate(target[3]):

             if degree_start_date != None and degree_start_date != '':

                 degree_query = {
                    "bool": {
                       "must": [
                          {
                             "range": {
                                "linkedin.educations.start_date": {
                                   "gte": degree_start_date.encode('utf8') + "||-72M",
                                   "lte": degree_start_date.encode('utf8') + "||+12M",
                                }
                             }
                          }
                       ]
                    }
                  }

                 all_degree_query.append(degree_query)

                 considered_degrees.append(index)



    # create filters for degree with major if exists with specific weights

    if considered_degrees:
        each_degree_weight = 28/float(len(considered_degrees))

        if target[1] and target[1] != [None]:

           for index, input_degree in enumerate(target[1]):

              if input_degree != None and input_degree != '':

                  filter_deg_query = {
                      "filter":{
                         "nested": {
                            "path": "linkedin.educations",
                            "query": {
                               "bool": {
                                 "must": [
                                    {
                                       "match": {
                                          "linkedin.educations.degree.name": input_degree.encode('utf8')
                                       }
                                    }
                                 ]
                              }
                            }
                         }
                      },
                      "weight": each_degree_weight
                   }


                  if target[9] and target[9][index] != None and target[9][index] != '':

                     major = target[9][index]
                     filter_deg_query['filter']['nested']['query']['bool']['must'].append({
                        "match": {
                           "linkedin.educations.major.name": major.encode('utf8')
                        }
                     })

                  filters.append(filter_deg_query)
                  degree_filter_only.append(filter_deg_query)
                  considered_filter_degree.append(index)

        # create filter for major, if degree does not exist with specific weights

        elif target[1] == [None] or (not target[1]):

           if target[9] and target[9] != [None] and target[9] != ['']:
              each_degree_weight = 28/float(len(target[9]))

              for index, major in enumerate(target[9]):

                  if major != None and major != '':

                      filter_deg_query = {
                          "filter":{
                             "nested": {
                                "path": "linkedin.educations",
                                "query": {
                                   "bool": {
                                     "must": [
                                        {
                                           "match": {
                                              "linkedin.educations.major.name": major.encode('utf8')
                                           }
                                        }
                                     ]
                                  }
                                }
                             }
                          },
                          "weight": each_degree_weight
                       }

                      filters.append(filter_deg_query)
                      degree_filter_only.append(filter_deg_query)
                      considered_filter_degree.append(index)


    # create filter for colleges for consisdered degrees only, (i.e ignoring school names) with specific weights

    if considered_degrees:
       each_college_weight = 12/float(len(considered_degrees))
       for index in considered_degrees:

          if target[2] and target[2][index] != None and target[2][index] != '' :

             input_college = target[2][index]

             filter_college = {
                 "filter":{
                    "nested": {
                       "path": "linkedin.educations",
                       "query": {
                          "term": {
                             "linkedin.educations.institute.name.raw": {
                                "value": input_college.encode('utf8')
                             }
                          }
                       }
                    }
                 },
                "weight": each_college_weight
              }

             filters.append(filter_college)

    # if we did not catch considered degrees, then grab all the institue names as college degrees and create their filters with specific weights

    elif target[2] and target[2] != [None]:
       each_college_weight = 12/float(len(target[2]))
       for index, college_name in enumerate(target[2]):

          input_college = target[2][index]

          if input_college != None and input_college != '':

              filter_college = {
                  "filter":{
                     "nested": {
                        "path": "linkedin.educations",
                        "query": {
                           "term": {
                              "linkedin.educations.institute.name.raw": {
                                 "value": input_college.encode('utf8')
                              }
                           }
                        }
                     }
                  },
                 "weight": each_college_weight
               }

              filters.append(filter_college)



    # create filter for college start date using degree name, major name and start date if exists
    # created each filter covering all the time duration individually, as there was no other means to ...
    # do it except in script by looping in education of each profile which would be very time consuming

    if degree_filter_only:
        degree_filter_only_length = len(degree_filter_only)
        for index, fil_deg in enumerate(degree_filter_only):

            fil_index = considered_filter_degree[index]

            if target[3][fil_index] != None and target[3][fil_index] != '':

                degree_start_date = target[3][fil_index]

                filter_college_start_date = copy.deepcopy(fil_deg)

                filter_college_start_date['filter']['nested']['query']['bool']['must'].append(
                  {
                     "range": {
                        "linkedin.educations.start_date": {
                           "gte": degree_start_date.encode('utf8'),
                           "lte": degree_start_date.encode('utf8') + "||+11M"
                        }
                     }
                  })


                filter_college_start_date['weight'] = 16*0.2/float(degree_filter_only_length)

                filters.append(filter_college_start_date)


                filter_college_start_date1 = copy.deepcopy(fil_deg)
                filter_college_start_date1['filter']['nested']['query']['bool']['must'].append(
                    {
                     "range": {
                         "linkedin.educations.start_date": {
                            "gte": degree_start_date.encode('utf8') + "||-12M",
                            "lte": degree_start_date.encode('utf8') + "||-1M"
                         }
                      }
                    })

                filter_college_start_date1['weight'] = 16*0.6/degree_filter_only_length

                filters.append(filter_college_start_date1)


                filter_college_start_date2 = copy.deepcopy(fil_deg)
                filter_college_start_date2['filter']['nested']['query']['bool']['must'].append(
                    {
                     "range": {
                         "linkedin.educations.start_date": {
                            "gte": degree_start_date.encode('utf8') + "||-24M",
                            "lte": degree_start_date.encode('utf8') + "||-13M"
                         }
                      }
                    })

                filter_college_start_date2['weight'] = 16/degree_filter_only_length


                filters.append(filter_college_start_date2)


                filter_college_start_date3 = copy.deepcopy(fil_deg)
                filter_college_start_date3['filter']['nested']['query']['bool']['must'].append(
                    {
                     "range": {
                         "linkedin.educations.start_date": {
                            "gte": degree_start_date.encode('utf8') + "||-36M",
                            "lte": degree_start_date.encode('utf8') + "||-25M"
                         }
                      }
                    })

                filter_college_start_date3['weight'] = 16*0.8/degree_filter_only_length

                filters.append(filter_college_start_date3)


                filter_college_start_date4 = copy.deepcopy(fil_deg)
                filter_college_start_date4['filter']['nested']['query']['bool']['must'].append(
                    {
                     "range": {
                         "linkedin.educations.start_date": {
                            "gte": degree_start_date.encode('utf8') + "||-48M",
                            "lte": degree_start_date.encode('utf8') + "||-37M"
                         }
                      }
                    })

                filter_college_start_date4['weight'] = 16*0.4/degree_filter_only_length

                filters.append(filter_college_start_date4)

                filter_college_start_date = []
                filter_college_start_date1 = []
                filter_college_start_date2 = []
                filter_college_start_date3 = []
                filter_college_start_date4 = []

















    # create filter for company names with specific weights

    company_length = 0
    if target[4] and target[4] != [None] and target[4] != '':

       company_length = len(target[4])
       for company_name in target[4]:

           if company_name != None and company_name != '':

               filter_company_name = {
                   "filter": {
                      "nested": {
                         "path": "linkedin.experiences",
                         "query": {
                            "match_phrase": {
                               "linkedin.experiences.company.name": company_name.encode('utf8')
                            }
                         }
                      }
                   },
                   "weight": 14/float(company_length)
                }

               filters.append(filter_company_name)

    # create filter for current location with specific weights

    curr_loc_name = target[8][0]
    if curr_loc_name != None and curr_loc_name != '':
        filter_curr_loc = {
        "filter": {
           "term": {
              "linkedin.current_location": curr_loc_name.encode('utf8')
           }
        },
        "weight": 4
        }

        filters.append(filter_curr_loc)




    # create AND component having skill set

    skill_query = ''
    if target[10] and target[10] != [None] and target[10] != ['']:
       min_match = 0.4*len(target[10])

       min_match = int(min_match)

       if min_match > 6:
           min_match = 6

       skill_query = {
          "terms": {
             "linkedin.skills.raw": target[10],"minimum_match": min_match
          }
       }


    curr_ind = target[11][0]
    if curr_ind != None and curr_ind != '':
        filter_curr_ind = {
            "filter": {
               "term": {
                  "linkedin.current_industry": curr_ind.encode('utf8')
               }
            },
            "weight": 10
            }

        filters.append(filter_curr_ind)


    es_input_query = {
      "bool": {
         "must": []
      }
    }

    # if degree AND components created then append in the es query
    if all_degree_query:

        es_input_query['bool']['must'].append(
            {
                "nested": {
                  "path": "linkedin.educations",
                  "query": {
                      "bool": {
                         "should": [
                         ]
                      }
                   }
               }
            }
        )

        for degree in all_degree_query:
            es_input_query['bool']['must'][0]['nested']['query']['bool']['should'].append(degree)

    # if skills_set AND component created above, then append in the es query
    if skill_query:
        es_input_query['bool']['must'].append(skill_query)


    # if degree and skills_set AND components created above, then only create filter having scripting score, ...
    # otherwise the script would run on huge number of records if any of the AND components does not exist
    if (all_degree_query) and (skill_query):

        if target[6] and target[6][-1] != None and target[6][-1] != '' and company_length != 0:
            first_job_start_date = target[6][-1]
            first_job_start_date = re.match(r'^(\d{4})\-',first_job_start_date,re.I)
            first_job_start_date = first_job_start_date.group(1)

            first_job_start_date = str(first_job_start_date)
            company_length = str(company_length)

            # inp = raw_input("script_score")

            filters.append(
                {
                   "script_score": {
                       "script" : "f_score=0;total=0;def date='';target_year=" + first_job_start_date + ";target_no_jobs=" + company_length + ";for(exp in _source.linkedin.experiences){total=total+1;if(exp.containsKey('start_date')){date=exp['start_date'];};};if(total<=target_no_jobs){return(-100);};if(date!=''){def year = date[0..3].toInteger();year = year.toInteger();def diff=year-target_year;if((-4<diff) && (diff<0)){f_score=Math.abs(diff)*3;};else{return(-100);};};def job_diff=Math.abs(target_no_jobs-total+1);job_score=target_no_jobs-job_diff;job_score=(job_score/target_no_jobs)*11;return(f_score+job_score);"

                      # "script" : "f_score=0;total=0;def date='';target_year=" + first_job_start_date + ";target_no_jobs=" + company_length + ";for(exp in _source.linkedin.experiences){total=total+1;if(exp.containsKey('start_date')){date=exp['start_date'];};};if(total<=target_no_jobs){return(-100)};match=date=~/^(?<year>\\d{4})\\-/;if(match){def year=match.group('year').toInteger();def diff=Math.abs(target_year-year);if(diff<5){f_score=5-diff;f_score=f_score*3;};if(f_score<0){f_score=0};};def job_diff=Math.abs(target_no_jobs-total+1);job_score=target_no_jobs-job_diff;job_score=(job_score/target_no_jobs)*11;if(job_score<0){job_score=0;};f_score=f_score+job_score;return(f_score);"
                       # "script": "f_score=0;total=0;def date='';target_year=" + first_job_start_date + ";target_no_jobs=" + company_length + ";for(exp in _source.linkedin.experiences){total=total+1;if(exp.containsKey('start_date')){date=exp['start_date'];};};match=date=~/^(?<year>\\d{4})\\-/;if(match){def year=match.group('year').toInteger();def diff=Math.abs(target_year-year);if(diff<5){f_score=5-diff;f_score=f_score*3;};if(f_score<0){f_score=0};};def job_diff=Math.abs(target_no_jobs-total+1);job_score=target_no_jobs-job_diff;job_score=(job_score/target_no_jobs)*11;if(job_score<0){job_score=0;};f_score=f_score+job_score;return(f_score);"
                   }
                }
            )

    # if degree and skills_set AND component are NOT created then simply match_all the es query with the assigned filters

    if (not all_degree_query) and (not skill_query):
        es_input_query = {
            "match_all": {}
        }

        print "NOT A PERFECT PROFILE"





    print "es_input_query : ", es_input_query
    print "\n"
    print "filters : ", filters
    print "\n"

    final_es_query = {
           "size": MAX_FETCH_PROFILE,
           "query": {
              "function_score": {
                 "query": es_input_query,
                 "functions": filters,
                 "score_mode": "sum",
                 "boost_mode": "replace"
              }
           }
        }

    print "final_es_query : ", final_es_query

    return final_es_query


def get_minimum_working_duration(currently_working_user_job_durations, current_job_duration_in_months):
    larger_durations = []
    for x in currently_working_user_job_durations:
        x = x/DAYS_IN_MONTH
        if x >= current_job_duration_in_months:
            larger_durations.append(x)
    if not len(larger_durations):
        return current_job_duration_in_months

    avg_min_duration = sum(larger_durations)/len(larger_durations)
    return avg_min_duration


def get_analysis_level_two(similar_user_job_durations, current_job_duration_in_months):
    print"***** Entering get_analysis_level_two *****"

    longer_job_durations = {}

    # for jd in similar_user_job_durations:
    #     if jd - current_job_duration_in_months > -3:
    #         longer_job_durations.append(jd)


    print "initial simi",similar_user_job_durations

    for index,similar_user_job_duration in enumerate(similar_user_job_durations):

        jd = similar_user_job_duration[0]/DAYS_IN_MONTH
        similar_user_job_durations[index] = jd
        if jd >= current_job_duration_in_months:
            score = similar_user_job_duration[1]

            if jd in longer_job_durations:
                longer_job_durations[jd] += score
            else:
                longer_job_durations[jd] = score

    print "similar user job durations in months", similar_user_job_durations
    print"Longer Job Durations ", longer_job_durations

    max_score = 0
    nearest_local_maxima = 0
    for ljd,l_score in longer_job_durations.iteritems():

        if l_score > max_score:
            max_score = l_score
            nearest_local_maxima = ljd


    # sorted_longer_job_durations_index = sorted(longer_job_durations, key=longer_job_durations.__getitem__,reverse=True)
    #
    #
    # probabilities = []
    # i = 0
    # max_probability = 0
    #
    # for key in sorted_longer_job_durations_index:
    #
    #     peak = key
    #
    #     # filtered_freqs = []
    #     total_people = 0
    #     filtered_people = 0
    #
    #     for new_key,new_value in longer_job_durations.iteritems():
    #
    #         if new_key <= peak:
    #             # filtered_freqs.append(freq)
    #             filtered_people += new_value
    #         total_people += new_value
    #
    #     probability = float(filtered_people)/total_people
    #
    #     probabilities.append((peak,probability))
    #
    #     if probability > max_probability:
    #         max_probability = probability
    #         nearest_local_maxima = peak
    #
    #     i += 1
    #     if i >= 5:
    #         break
    #
    # print "nearest local maxima", nearest_local_maxima
    # print "first five probabilities", probabilities


    # counter = collections.Counter(longer_job_durations)
    # print counter

    # nearest_local_maxima = 0
    #
    # max_score = 0
    # for longer_job_duration in longer_job_durations:
    #
    #     score = longer_job_duration[1]
    #
    #     if score > max_score:
    #         print "score", score
    #         max_score = score
    #         nearest_local_maxima = longer_job_duration[0]
    #         print "maxima", nearest_local_maxima


    print "\n\nmax score", max_score
    print "nearest local", nearest_local_maxima

    # print "Printing Top frequencies", counter.most_common()
    # for x in counter.most_common(len(counter)):
    #     abs_diff_from_current_jd = abs(x[0] - current_job_duration_in_months)
    #     inp = raw_input("paused")
    #     if abs_diff_from_current_jd < MONTHS_IN_YEAR:
    #         nearest_local_maxima = x[0]
    #         print "updated Local Maxima ", nearest_local_maxima
    #         break


    # if current_job_duration_in_months <= 24:

    # for x in counter.most_common(len(counter)):
    #     if x[0] >= current_job_duration_in_months:
    #         nearest_local_maxima = x[0]
    #         print "nearest local maxima", nearest_local_maxima
    #         break
    # else:
    #
    #     probabilities = []
    #     i = 0
    #     max_probability = 0
    #
    #     for x in counter.most_common(len(counter)):
    #
    #         peak = x[0]
    #
    #         # filtered_freqs = []
    #         total_people = 0
    #         filtered_people = 0
    #
    #         for freq in counter.most_common(len(counter)):
    #
    #             if freq[0] <= peak:
    #                 # filtered_freqs.append(freq)
    #                 filtered_people += freq[1]
    #             total_people += freq[1]
    #
    #         probability = float(filtered_people)/total_people
    #
    #         probabilities.append((peak,probability))
    #
    #         if probability > max_probability:
    #             max_probability = probability
    #             nearest_local_maxima = peak
    #
    #         i += 1
    #         if i >= 5:
    #             break
    #
    #     print "nearest local maxima", nearest_local_maxima
    #     print "first five probabilities", probabilities

    # print"Nearest Local Maxima ", nearest_local_maxima
    if nearest_local_maxima == 0:
        print "in nearest_local_maxima == 0 cond"
        print"Nearest Local Maxima ", ((current_job_duration_in_months / MONTHS_IN_YEAR) + 1) * MONTHS_IN_YEAR
        return ((current_job_duration_in_months / MONTHS_IN_YEAR) + 1) * MONTHS_IN_YEAR

    # if nearest_local_maxima < current_job_duration_in_months:
    #     print "in nearest_local_maxima < current_job_duration_in_months cond"
    #     # if the nearest local maxima found is less than current job duration
    #     # then use 25:75 to reply activeness
    #     if current_job_duration_in_months - nearest_local_maxima > MONTHS_IN_QUARTER_YEAR:
    #         print "1st cond Nearest Local Maxima ",((current_job_duration_in_months / MONTHS_IN_YEAR) + 1) * MONTHS_IN_YEAR
    #         return ((current_job_duration_in_months / MONTHS_IN_YEAR) + 1) * MONTHS_IN_YEAR
    #     else:
    #         print "2nd cond Nearest Local Maxima ",current_job_duration_in_months
    #         return current_job_duration_in_months

    return nearest_local_maxima









if __name__ == '__main__':
    print "In main"

    fetch_target_query = {
        "query": {
            "match_all": {}
        },
        "from": 3000
    }
    #55028a30650b84a55fbfa289,5501d54e650b84a55faa4c12,550035e8650b84a55f5edcb9

    # fetch_target_query = {
    #     "query": {
    #         "term": {
    #            "_id": {
    #               "value": "5500071e650b84a55f576124"
    #            }
    #         }
    #     }
    # }


    # print "fetching 20000 input profiles"

    args_array = sys.argv

    # print "here", args_array
    #
    # if len(args_array) > 1:
    #     from_num = args_array[1]
    #     from_num = int(from_num)
    #     fetch_target_query["from"] = from_num
    #
    total_fetched = 0

    try:
        resp = prod_es.search(index=es_index, doc_type=es_type, body=fetch_target_query, request_timeout=200000, search_type="scan", scroll=SCROLL_TIMEOUT_SECS, size=SCROLL_SIZE)

        while True:
            resp = prod_es.scroll(resp['_scroll_id'], scroll=SCROLL_TIMEOUT_SECS)
            if len(resp['hits']['hits']) == 0 or total_fetched >=5000:
                break
            else:
                es_resps = resp['hits']['hits']
                profiles = []
                profiles = es_resps
                profile_count = (total_fetched + 1)
                total_fetched += len(profiles)
                print "total profiles fetched : ",total_fetched

                f = open('modified_test_jsp_exp_plus_1.csv','a')
                # reg_inp = open('regression.csv','a')
                # reg_out = open('regression_output.csv','a')
                # reg_out_curr_job_dur = open('regression_output_current_job_dur.csv','a')
                # reg_out_total_job_dur = open('regression_output_total_job_dur.csv','a')
                # reg_output_candidate_job_durs = open('regression_output_candidate_job_durs.csv','a')

                for profile in profiles:

                    print profile['_id']

                    print "profile no", profile_count

                    try:
                        (target) = create_target_array(profile)
                        print "actual_target",target
                    except Exception as e:
                        print e
                        print "ERROR in create_target_array"

                    # components of target array = ['id', [degree], [colleges], [college_start_dates], [company names], [company profile title], [company_start_date], [company_end_date], [current_location], [major], [skills_set]]

                    print "og  profile",profile

                    output = None

                    # try:
                    output = go_to_past(profile)
                    # except Exception as e:
                    #     print e
                    #     print "ERROR in go_to_past"

                    print "output",output

                    print "\n\n\n\n-------------------------------------------------------------------------\n\n\n\n"

                    profile_count = profile_count + 1

                # f.close()
    # #
    # #
    # #
    except Exception as e:
        print Exception, e





# data = db.find({"target_id":"55003874650b84a55f5f4f95"})
#     data = db.find()
#     profile_count = 1
#
#     f = open('modified_test_jsp_exp_plus_1.csv','a')
#     # reg_inp = open('regression.csv','a')
#     # reg_out = open('regression_output.csv','a')
#     # reg_out_curr_job_dur = open('regression_output_current_job_dur.csv','a')
#     # reg_out_total_job_dur = open('regression_output_total_job_dur.csv','a')
#     # reg_output_candidate_job_durs = open('regression_output_candidate_job_durs.csv','a')
#
#     for document in data:
#
#         id = document['target_id']
#
#         similar_users = document['similar_profiles']
#
#         fetch_target_query = {
#             "query": {
#                 "term": {
#                    "_id": {
#                       "value": id
#                    }
#                 }
#             }
#         }
#
#         res = prod_es.search(index=es_index, doc_type=es_type, request_timeout = 30000, body = fetch_target_query)
#         profiles = res['hits']['hits']
#
#
#         for profile in profiles:
#
#             print profile['_id']
#
#             print "profile no", profile_count
#
#             try:
#                 (target) = create_target_array(profile)
#                 print "actual_target",target
#             except Exception as e:
#                 print e
#                 print "ERROR in create_target_array"
#
#             # components of target array = ['id', [degree], [colleges], [college_start_dates], [company names], [company profile title], [company_start_date], [company_end_date], [current_location], [major], [skills_set]]
#
#             print "og  profile",profile
#
#             output = None
#
#             # try:
#             output = go_to_past(profile,similar_users)
#             # except Exception as e:
#             #     print e
#             #     print "ERROR in go_to_past"
#
#             print "output",output
#
#             print "\n\n\n\n-------------------------------------------------------------------------\n\n\n\n"
#
#             profile_count = profile_count + 1
#
#     f.close()
