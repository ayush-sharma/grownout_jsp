__author__ = 'grownout'
__author__ = 'shubham_grownout'
from elasticsearch import Elasticsearch
import csv
import re


def fetch_data():
    prod_es = Elasticsearch('http://130.211.114.20:6200/')
    es_index = "complete"
    es_type = "user"

    es_query ={
    "query": {
        "nested": {
           "path": "linkedin.experiences",
           "query": {
               "match_all": {}
           }
        }
    },
    "_source":[
    "linkedin.experiences.title"
    ],
    "size": 50000
    }

    res = prod_es.search(index=es_index, doc_type=es_type, request_timeout = 30000, body = es_query)
    res=res['hits']['hits']
    # print res
    dict_test_desig={}
    for profile in res:
        id=profile['_id']
        # dict_test_desig[id] = []
        if '_source' in profile:
            if 'linkedin' in profile['_source']:
                if 'experiences' in profile['_source']['linkedin']:
                    for exp in profile['_source']['linkedin']['experiences']:
                        if 'title' in exp:
                            # print id,exp['title']
                            title = exp['title'].encode('utf8').lower()
                            if title in dict_test_desig:
                                dict_test_desig[title].append(id.encode('utf8'))
                            else:
                                dict_test_desig[title] = [id.encode('utf8')]


                            # dict_test_desig[id].append(exp['title'].encode('utf8'))

    return(dict_test_desig)

dict_test_desig = fetch_data()
# print dict_test_desig.keys()
# for i in dict_test_desig.values():
#     for each in i:
#         print each


# -----------------------------------------------------------------------------



