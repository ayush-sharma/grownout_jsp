__author__ = 'ayush'

import csv
import linecache
# import math


MONTHS_IN_QUARTER_YEAR = 3
MONTHS_IN_YEAR = 12

for alpha_index in xrange(1,26):

    print "\n\n===============alpha_index", alpha_index,"\n\n=============="

    # f_error_mean = open('error_mean_0_neg_1000.csv','a')
    # alpha_index = 1
    # f_acc = open('alphas_acc_hit_n_try.csv','w+')
    
    # error_iter = {}
    
    
    alphas = [1] * 26
    
    
    
    
    for iter in xrange(0,30):
    
        # with open('alphas_regression.csv', 'r') as csvfile:
        with open('loc_boost_regression.csv', 'r') as csvfile:
        
            csv_handle_input = csv.reader(csvfile, delimiter=',', quotechar='|')
        
            # profile_count = 0
            error_profiles = []
        
            # alphas[alpha_index] = alphas[alpha_index] - (0.02) # ayush
            alphas[alpha_index] = alphas[alpha_index] + 1 # ayush
        
            for row_index,row_input in enumerate(csv_handle_input):
        
                # print "Iter : ",iter," , ","Profile_count",row_index
                # print "profile count",row_index
        
                # continue
        
                # inp = raw_input("paused")
        
                # candidate_job_durs =  linecache.getline('alphas_regression_output_candidate_job_durs.csv', row_index + 1).split(',') #sharma
                candidate_job_durs =  linecache.getline('loc_boost_regression_output_candidate_job_durs.csv', row_index + 1).split(',') #sharma
        
                # if row_index >= 337:# or (iter == 0 and row_index == 1900):
                # print "candidate_job_durs",candidate_job_durs
        
                # target_current_job_durs =  linecache.getline('alphas_regression_output_current_job_dur.csv', row_index + 1).split(',') #sharma
                target_current_job_durs =  linecache.getline('loc_boost_regression_output_current_job_dur.csv', row_index + 1).split(',') #sharma
                target_current_job_durs = int(target_current_job_durs[0])
        
                # if row_index >= 337:# or (iter == 0 and row_index == 1900):
                # print "target_current_job_durs",target_current_job_durs
        
                # target_total_job_durs =  linecache.getline('alphas_regression_output_total_job_dur.csv', row_index + 1).split(',') #sharma
                target_total_job_durs =  linecache.getline('loc_boost_regression_output_total_job_dur.csv', row_index + 1).split(',') #sharma
                target_total_job_durs = int(target_total_job_durs[0])
        
                # if row_index >= 337:# or (iter == 0 and row_index == 1900):
                # print "target_total_job_durs",target_total_job_durs
        
                job_dur_alpha_dict = {}
        
                id = row_input[0]
                all_closeness = row_input[1 : len(row_input)]
        
                # if row_index >= 337:# or (iter == 0 and row_index == 1900):
                # print "all_closeness",all_closeness
        
                # if row_index >= 337:# or (iter == 0 and row_index == 1900):
                # inp = raw_input("paused")
        
                for closeness_index,closeness in enumerate(all_closeness):
        
                    candi_job_dur = int(candidate_job_durs[closeness_index+1])
        
                    # if row_index >= 337:# or (iter == 0 and row_index == 1900):
                    #     print "candi_job_dur",candi_job_dur
                        # inp = raw_input("paused")
        
                    # if candi_job_dur < target_current_job_durs:
                    #     continue
        
                    alpha = 0

                    if int(closeness) <= 4:
                        alpha = alphas[0]
                    elif 4 < int(closeness) <= 8:
                        alpha = alphas[1]
                    elif 8 < int(closeness) <= 12:
                        alpha = alphas[2]
                    elif 12 < int(closeness) <= 16:
                        alpha = alphas[3]
                    elif 16 < int(closeness) <= 20:
                        alpha = alphas[4]
    
                    elif 20 < int(closeness) <= 24:
                        alpha = alphas[5]
    
    
    
                    elif 24 < int(closeness) <= 28:
                        alpha = alphas[6]
    
                    # if int(jd) <= 6:
                    #     alpha = alphas[1]
                    elif 28 < int(closeness) <= 32:
                        alpha = alphas[7]
    
                    elif 32 < int(closeness) <= 36:
                        alpha = alphas[8]
    
                    elif 36 < int(closeness) <= 40:
                        alpha = alphas[9]
    
                    elif 40 < int(closeness) <= 44:
                        alpha = alphas[10]
    
                    elif 44 < int(closeness) <= 48:
                        alpha = alphas[11]
    
                    elif 48 < int(closeness) <= 52:
                        alpha = alphas[12]
    
                    elif 52 < int(closeness) <= 56:
                        alpha = alphas[13]
    
                    elif 56 < int(closeness) <= 60:
                        alpha = alphas[14]
    
                    elif 60 < int(closeness) <= 64:
                        alpha = alphas[15]
    
                    elif 64 < int(closeness) <= 68:
                        alpha = alphas[16]
    
                    elif 68 < int(closeness) <= 72:
                        alpha = alphas[17]
    
                    elif 72 < int(closeness) <= 76:
                        alpha = alphas[18]
    
    
    
                    elif 76 < int(closeness) <= 80:
                        alpha = alphas[19]
    
                    elif 80 < int(closeness) <= 84:
                        alpha = alphas[20]
    
                    elif 84 < int(closeness) <= 88:
                        alpha = alphas[21]
    
                    elif 88 < int(closeness) <= 92:
                        alpha = alphas[22]
    
                    elif 92 < int(closeness) <= 96:
                        alpha = alphas[23]
    
                    elif 96 < int(closeness) <= 100:
                        alpha = alphas[24]
    
                    elif 100 < int(closeness):
                        alpha = alphas[25]
        
    
        
                    # print "alpha",alpha
        
                    if candi_job_dur in job_dur_alpha_dict:
                        job_dur_alpha_dict[candi_job_dur] += alpha
                    else:
                        job_dur_alpha_dict[candi_job_dur] = alpha
        
                # if row_index >= 337:
                # print "job_dur_dict",job_dur_alpha_dict
                # inp = raw_input("paused no job")
        
        
                if not job_dur_alpha_dict:
                    continue
        
                sorted_asc_keys_job_dur_alpha_dict_index = sorted(job_dur_alpha_dict)
        
                sorted_desc_values_job_durations_index = sorted(job_dur_alpha_dict, key=job_dur_alpha_dict.__getitem__,reverse=True)
        
        
                max_score = 0
                pred = None
        
                for key in sorted_asc_keys_job_dur_alpha_dict_index:
                    value = job_dur_alpha_dict[key]
                    if value > max_score:
                        max_score = value
                        pred = key
        
                if pred == None:
                    pred = sorted_asc_keys_job_dur_alpha_dict_index[0]
        
        
        
                # for key in sorted_desc_values_job_durations_index:
                #
                #     if key <= target_current_job_durs:
                #         if (key-target_current_job_durs) <= MONTHS_IN_QUARTER_YEAR:
                #             pred = key
                #             break
                #     else:
                #         value = job_dur_alpha_dict[key]
                #         if value > max_score:
                #             max_score = value
                #             pred = key
        
        
        
                # pred = 0
                #
                # for key in sorted_desc_values_job_durations_index:
                #     abs_diff_from_current_jd = abs(key - target_current_job_durs)
                #     if abs_diff_from_current_jd < MONTHS_IN_YEAR:
                #         pred = key
                #         break
                #
                # # print"Nearest Local Maxima ", nearest_local_maxima
                # if pred == 0:
                #     pred = ((target_current_job_durs / MONTHS_IN_YEAR) + 1) * MONTHS_IN_YEAR
                #
                # if pred < target_current_job_durs:
                #     # if the nearest local maxima found is less than current job duration
                #     # then use 25:75 to reply activeness
                #     if target_current_job_durs - pred > MONTHS_IN_QUARTER_YEAR:
                #         pred = ((target_current_job_durs / MONTHS_IN_YEAR) + 1) * MONTHS_IN_YEAR
                #     else:
                #         pred = target_current_job_durs
        
                # if pred == 0:
                #     pred = sorted_asc_keys_job_dur_alpha_dict_index[0]
        
        
        
        
        
        
        
                # try:
                #     pred = sorted_desc_values_job_durations_index[2]
                # except:
                # if pred == None:
                #     pred = sorted_asc_keys_job_dur_alpha_dict_index[0]
        
                actual_month_diff = abs(target_total_job_durs - target_current_job_durs)
                pred_month_diff = abs(pred - target_current_job_durs)
        
        
        
        
                # if actual_month_diff <= 4 and pred_month_diff <= 4:
                #     error = 0
                # elif actual_month_diff >4 and pred_month_diff > 4:
                #     error = 0
                # else:
                #     error = 1
        
        
        
        
                # if actual_month_diff <= 4 and pred_month_diff > 4:
                #     error = 1
                if actual_month_diff > 4 and pred_month_diff <= 4:
                    error = 1
                else:
                    error = 0
        
        
                # error = abs(pred - target_total_job_durs)
        
        
        
                # print "target_total",target_total_job_durs
                # print "target_current_job_durs",target_current_job_durs
                # print "pred job dur",pred
                # print "actual_month_diff",actual_month_diff
                # print "pred_month_diff",pred_month_diff
                # print "sorted_asc_job_dur_alpha_dict_index",sorted_asc_keys_job_dur_alpha_dict_index
                # print "sorted_desc_values_job_durations_index",sorted_desc_values_job_durations_index
                # print "error",error
        
                # if actual_month_diff <=4 or pred_month_diff <= 4:
                #     inp = raw_input("paused")
                # print "max score",max_score
        
                error_profiles.append(error)
        
                # f_acc_write = str(id) + "," + str(target_total_job_durs - target_current_job_durs) + "," + str(pred - target_current_job_durs) + "\n"
                f_acc_write = str(id) + "," + str(actual_month_diff) + "," + str(pred_month_diff) + "\n"
                # f_acc.write(f_acc_write)
        
                # inp = raw_input("profile completed")
        
            mean_error = reduce(lambda x, y: x + y, error_profiles) / float(len(error_profiles))
        
            # error_iter[alphas[alpha_index]] = error_profiles[:]
        
            # error_iter[alphas[alpha_index]] = mean_error
            print "error mean",mean_error,"alpha",alpha_index,",",alphas[alpha_index]
        
            # f_error_mean_str = str(alphas[alpha_index]) + "," + str(mean_error) + "\n"
        
            # f_error_mean.write(f_error_mean_str)
        
            # csv_handle_input = csv.reader(csvfile, delimiter=',', quotechar='|')
        
        # inp = raw_input("iter completed")
        # print error_iter
