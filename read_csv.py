__author__ = 'ayush'

import csv
import linecache
import json

# i= 0

# f_write = open('regression_input_bucket_0_6.csv','w+')

# with open('regression_input.csv', 'r') as csvfile:
#     csv_handle = csv.reader(csvfile, delimiter=',', quotechar='|')
#
#     # arr =  linecache.getline('regression_input.csv', 2)
#     # arr = arr.split(',')
#     # print arr
#     # print arr[3]
#     # exit(0)
#
#     for index,row in enumerate(csv_handle):
#
#         count_0_2 = 0
#         count_3_6 = 0
#         # count_0_6 = 0
#         count_6_12 = 0
#         count_12_24 = 0
#         count_24_48 = 0
#         count_48_100 = 0
#         count_above_100 = 0
#         id = row[0]
#         jds = row[1 : len(row)]
#         for jd in jds:
#             if int(jd) <= 2:
#                 count_0_2 += 1
#             elif 6 >= int(jd) > 2:
#                 count_3_6 += 1
#             # if int(jd) <= 6:
#             #     count_0_6 += 1
#             elif 12 >= int(jd) > 6:
#                 count_6_12 += 1
#             elif 24 >= int(jd) > 12:
#                 count_12_24 += 1
#             elif 48 >= int(jd) > 24:
#                 count_24_48 += 1
#             elif 100 >= int(jd) > 48:
#                 count_48_100 += 1
#             elif int(jd) > 100:
#                 count_above_100 += 1
#
#         write_str = id + "," + str(count_0_2) + "," + str(count_3_6) + "," + str(count_6_12) + "," + str(count_12_24) + "," + str(count_24_48) + "," + str(count_48_100) + "," + str(count_above_100) + "\n"
#         # write_str = id + "," + str(count_0_6) + "," + str(count_6_12) + "," + str(count_12_24) + "," + str(count_24_48) + "," + str(count_48_100) + "," + str(count_above_100) + "\n"
#         # f_write.write(write_str)
#         # i += 1





f = open('alpha_all_errors.txt','a')
f_maintain_errors = open('alphas_maintain_errors.csv','a')
profile_count = 0

with open('alphas_regression.csv', 'r') as csvfile:

    print "profile_count",profile_count
    profile_count += 1

    csv_handle_input = csv.reader(csvfile, delimiter=',', quotechar='|')

    # with open('regression_output_candidate_job_durs.csv', 'r') as csvfile:
    #     csv_handle_candi = csv.reader(csvfile, delimiter=',', quotechar='|')

    all_errors = []
    for row_index,row_input in enumerate(csv_handle_input):

        # if row_index == 0:
        #     continue

        print "row_input",row_input

        candidate_job_durs =  linecache.getline('alphas_regression_output_candidate_job_durs.csv', row_index + 1).split(',') #sharma

        print "candidate_job_durs",candidate_job_durs


        target_current_job_durs =  linecache.getline('alphas_regression_output_current_job_dur.csv', row_index + 1).split(',') #sharma
        target_current_job_durs = int(target_current_job_durs[0])

        target_total_job_durs =  linecache.getline('alphas_regression_output_total_job_dur.csv', row_index + 1).split(',') #sharma
        target_total_job_durs = int(target_total_job_durs[0])

        print "target_current_job_durs",target_current_job_durs
        print "target_total_job_durs",target_total_job_durs


        alphas = [0.5] * 7

        errors_alphas = [] # 7 * 1000
        # for alpha_index in xrange(0,len(alphas)):

        alpha_index = 0

        error_iter = []
        # all_errors = [[] * len(alphas)]

        for iter in xrange(0,1000):

            print "iter",iter

            alphas[alpha_index] = alphas[alpha_index] + alphas[alpha_index]*(0.2)

            print "now alphas",alphas
            # inp = raw_input()

            job_dur_alpha_dict = {}

            count_0_2 = 0
            count_3_6 = 0
            # count_0_6 = 0
            count_6_12 = 0
            count_12_24 = 0
            count_24_48 = 0
            count_48_100 = 0
            count_above_100 = 0

            # alpha_0_2 = 0.5
            # alpha_3_6 = 0.5
            # # alpha_0_6 = 0
            # alpha_6_12 = 0.5
            # alpha_12_24 = 0.5
            # alpha_24_48 = 0.5
            # alpha_48_100 = 0.5
            # alpha_above_100 = 0.5

            id = row_input[0]
            all_closeness = row_input[1 : len(row_input)]
            # rbsmrt20
            # rbwest25
            for closeness_index,closeness in enumerate(all_closeness):

                print "closeness_index",closeness_index

                print "closeness",closeness
                # inp =raw_input()

                candi_job_dur = int(candidate_job_durs[closeness_index+1])

                print "candi_job_dur",candi_job_dur

                # inp = raw_input("paused")

                if candi_job_dur < target_current_job_durs:
                    continue

                alpha = 0

                if int(closeness) <= 2:
                    count_0_2 += 1
                    alpha = alphas[0]
                    print "here"

                elif 6 >= int(closeness) > 2:
                    count_3_6 += 1
                    alpha = alphas[1]
                # if int(jd) <= 6:
                #     count_0_6 += 1
                #     alpha = alphas[1]
                elif 12 >= int(closeness) > 6:
                    count_6_12 += 1
                    alpha = alphas[2]
                elif 24 >= int(closeness) > 12:
                    count_12_24 += 1
                    alpha = alphas[3]
                elif 48 >= int(closeness) > 24:
                    count_24_48 += 1
                    alpha = alphas[4]
                elif 100 >= int(closeness) > 48:
                    count_48_100 += 1
                    alpha = alphas[5]
                elif int(closeness) > 100:
                    count_above_100 += 1
                    alpha = alphas[6]

                if candi_job_dur in job_dur_alpha_dict:
                    job_dur_alpha_dict[candi_job_dur] += alpha
                else:
                    job_dur_alpha_dict[candi_job_dur] = alpha

                print "alpha", alpha

            sorted_job_dur_alpha_dict_index = sorted(job_dur_alpha_dict)

            print "sorted",sorted_job_dur_alpha_dict_index
            # inp = raw_input("paused")

            print "job_dur_alpha_dict",job_dur_alpha_dict
            # inp = raw_input()

            if not job_dur_alpha_dict:
                break

            max_score = 0
            pred = None

            for key in sorted_job_dur_alpha_dict_index:

                value = job_dur_alpha_dict[key]
                if value > max_score:
                    max_score = value
                    pred = key


            # for key,value in job_dur_alpha_dict.iteritems():
            #     if value > max_score:
            #         max_score = value
            #         pred = key

            print "pred",pred
            print "max_score",max_score

            error = abs(pred - target_total_job_durs)
            print "error",error

            f_str = str(error) + "," + str(alphas[alpha_index]) + "\n"

            f_maintain_errors.write(f_str)

            print "error alphas",alphas
            error_iter.append((error,alphas[:]))

            print "iter",iter,"row_index",row_index
            print "error_iter",error_iter


            # inp = raw_input("pause")

        errors_alphas.append(error_iter)
        print "error alphas",errors_alphas
        # inp = raw_input("paused")

        all_errors.append(errors_alphas[:])
        print "all_errors",all_errors
        # inp = raw_input("paused")


    all_errors_str = json.dumps(all_errors, indent=4)

    f.write(all_errors_str)


f.close()
f_maintain_errors.close()
