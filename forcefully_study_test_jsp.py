__author__ = 'ayush'

import re
import collections
import copy
import sys
import math

from pymongo import MongoClient
from elasticsearch import Elasticsearch
from datetime import datetime

# client = MongoClient('130.211.119.14')
client = MongoClient('dev-mongo1.grownout.com')
# db = client['test']['test_jsp_new_5000']

# from experience import get_job_durations

# SOME GLOCAL DEFINITIONS
# TRACE_BACK_JOB_NO = 0
TRACE_BACK_YEAR = "2012-01-01T00:00:00.000Z"
TRACE_BACK_YEAR = datetime.strptime(TRACE_BACK_YEAR, "%Y-%m-%dT%H:%M:%S.%fZ")
MIN_JOB_NO = 2 # minimumjob count a profile should have before applying the function
DAYS_IN_MONTH = 30  # One Month is 30 days
MONTHS_IN_YEAR = 12  # One year is 12 months
MONTHS_IN_HALF_YEAR = 6  # Six months in half year
MONTHS_IN_QUARTER_YEAR = 3  # Three months in Quarter Year
MIN_SIMILAR_USERS_REQUIRED = 10  # We will not do analytics if number of similar users is less.
CAN_SWITCH_NOW = 0  # Return 0 if we think he is probable to switch now.
LOOKING_FOR_JOB_CHANGE = 0  # Return 0 if he says looking for job change
MAX_SIMILAR_LIMIT = 5000
PASSIVE = 12
MAX_FETCH_PROFILE = 1000 # Indicates the number of profiles need to fetch from elasticsearch

# ERROR CODES
# ERROR_CODE = -1  # Linkedin Profile not found
# ERROR_CODE = -2  # Job count less than 1
# ERROR_CODE = -3  # Not working in any company

prod_es = Elasticsearch('http://130.211.114.20:6200/')
es_index = "complete"
es_type = "user"
SCROLL_TIMEOUT_SECS = '500000s'
SCROLL_SIZE = 13

def get_job_durations(experiences, educations):

    job_arr = []
    ex_index = 0
    for experience in experiences:

        try:
            if experience['end_date'] is not None:
                if type(experience['end_date']) == str or type(experience['end_date']) == unicode:
                    end_date = datetime.strptime(experience['end_date'], "%Y-%m-%dT%H:%M:%S.%fZ")
                else:
                    end_date = experience['end_date']

            elif experience['current']:
                end_date = TRACE_BACK_YEAR
                # end_date = datetime.today()
            else:
                end_date = None
        except:
            end_date = None

        try:
            if experience['start_date'] is not None:
                if type(experience['start_date']) == str or type(experience['start_date']) == unicode:
                    start_date = datetime.strptime(experience['start_date'], "%Y-%m-%dT%H:%M:%S.%fZ")
                else:
                    start_date = experience['start_date']

            else:
                start_date = None
        except:
            start_date = None

        if start_date is not None and end_date is not None:
            job_arr.append((experience['title'].lower(), start_date, end_date,'ex',ex_index))
        ex_index = ex_index + 1

    edu_arr = []
    ed_index = 0

    for education in educations:

        try:
            if education['end_date']is not None:
                if type(education['end_date']) == str or type(education['end_date']) == unicode:
                    end_date = datetime.strptime(education['end_date'], "%Y-%m-%dT%H:%M:%S.%fZ")
                else:
                    end_date = education['end_date']
            else:
                end_date = None
        except:
            end_date = None

        try:
            if education['start_date'] is not None:
                if type(education['start_date']) == str or type(education['start_date']) == unicode:
                    start_date = datetime.strptime(education['start_date'], "%Y-%m-%dT%H:%M:%S.%fZ")
                else:
                    start_date = education['start_date']
            else:
                start_date = None
        except:
            start_date = None

        if start_date is not None and end_date is not None:
            edu_arr.append(('student', start_date, end_date,'ed',ed_index))

        ed_index = ed_index + 1

    job_edu_arr = job_arr + edu_arr
    job_edu_arr.sort(key=lambda x: x[1])

    job_ex = 0
    job_ex_arr = []
    intern_ex = 0
    intern_ex_arr = []
    if len(job_edu_arr) > 0:
        min_end = job_edu_arr[0][1]

    remove_index = {}
    for job_edu in job_edu_arr:

        title, start_date, end_date, cat, index = job_edu

        if min_end > start_date:
            start_date = min_end

        work_ex = (end_date - start_date).days
        if work_ex < 0:
            work_ex = 0

        intern_word = ['intern',
                       'visitor',
                       'visiting',
                       'education',
                       'internee',
                       'scholar',
                       'summer',
                       'scholar',
                       'recipient',
                       'volunteer']
        title_word = title.split()

        if 'student' in title:
            category = -1
        elif len(list(set(title_word).intersection(intern_word))) > 0:
            category = 1
            remove_index[cat] = index
        else:
            category = 0

        if work_ex !=0:
            if category == 0:
                job_ex += work_ex
                job_ex_arr.append(work_ex)
            elif category == 1:
                intern_ex += work_ex
                intern_ex_arr.append(work_ex)

        if min_end < end_date:
            min_end = end_date

    return job_ex_arr, intern_ex_arr, remove_index

def filter_duplicate_company(company_names, job_start_dates):

   def remove_duplicates(values):
      output = []
      seen = set()
      for i, value in enumerate(values):
         if value not in seen:
            output.append(value)
            seen.add(value)


      return output

   (filtered_company_names) = remove_duplicates(company_names)

   filtered_job_start_dates = []
   company_names.reverse()

   for name in filtered_company_names:

      ind = company_names.index(name)
      filtered_job_start_dates.append(job_start_dates[-ind-1])

   return (filtered_company_names,filtered_job_start_dates)

def create_target_dict(doc):

    # components of target array = ['id', [degree], [colleges], [college_start_dates], [company names], [company profile title], [company_start_date], [company_end_date], [current_location], [major], [skills_set]]

    target={}

    institute_name=[]
    institute_start_date=[]
    institute_degree=[]
    major = []
    skills_set = []
    current_location = ''
    current_industry = ''

    company_name=[]
    company_start_date=[]
    company_end_date=[]
    company_title=[]

    # job end dates, job profile tilte and job end date are merely for manual data tesing, they are not used in the algorithm

    id = doc['_source']['_id']
    if 'current_location' in doc['_source']['linkedin']:
        current_location = doc['_source']['linkedin']['current_location']['name']

    print "rea 1"
    for education in doc['_source']['linkedin']['educations']:

        # grab degree having qualification as graduate, postgraduate and postgraduate diploma


        if 'qualification' in education:
            qualification = education['qualification']['name']
            if (qualification == 'Graduate' or qualification == 'Postgraduate' or qualification == 'Postgraduate Diploma'):

                institute_name.append(education['institute']['name'])

                if 'start_date' in education:
                    institute_start_date.append(education['start_date'])
                else:
                    institute_start_date.append(None)

                if 'degree' in education:
                    institute_degree.append(education['degree']['name'])
                else:
                    institute_degree.append(None)

                if 'major' in education:
                    major.append(education['major']['name'])
                else:
                    major.append(None)

    # if education does not have qualification as mentioned above, then grab all degrees without any qualification resemblance

    if ((not institute_degree) or institute_degree == [None]):
        institute_name = []
        institute_start_date = []
        major = []
        for education in doc['_source']['linkedin']['educations']:
            institute_name.append(education['institute']['name'])

            if 'start_date' in education:
                institute_start_date.append(education['start_date'])
            else:
                institute_start_date.append(None)

            if 'degree' in education:
                institute_degree.append(education['degree']['name'])
            else:
                institute_degree.append(None)

            if 'major' in education:
                major.append(education['major']['name'])
            else:
                major.append(None)

    # check if the first company start date is null, if yes, then grab next valid start  date as first company start date

    print "reached here"

    check_last_none = ''
    for experience in doc['_source']['linkedin']['experiences']:

        company_name.append(experience['company']['name'])
        company_title.append(experience['title'])


        if 'start_date' in experience:
            check_last_none = experience['start_date']
            company_start_date.append(check_last_none)
        else:
            company_start_date.append(None)
            check_last_none = None

        if 'end_date' in experience:
            company_end_date.append(experience['end_date'])
        else:
            company_end_date.append(None)

    if check_last_none == None:
        if company_start_date and company_start_date != [None]:
            length = len(company_start_date)

            valid_start_date = ''
            for i in xrange(-2,-length,-1):
                valid_start_date = company_start_date[i]
                if valid_start_date != None or valid_start_date != '':
                    company_start_date[-1] = valid_start_date
                    break

            if valid_start_date == '':
                company_start_date = []

    if 'skills' in doc['_source']['linkedin']:
        for skills in doc['_source']['linkedin']['skills']:
            skills_set.append(skills)

    if 'current_industry' in doc['_source']['linkedin']:
        if doc['_source']['linkedin']['current_industry'] != None and doc['_source']['linkedin']['current_industry'] != '':
            current_industry = doc['_source']['linkedin']['current_industry']

    target['id'] = (id)
    target['institute_degree'] = institute_degree
    target['institute_name'] = institute_name
    target['institute_start_date'] = institute_start_date
    target['company_name'] = company_name
    target['company_title'] = company_title
    target['company_start_date'] = company_start_date
    target['company_end_date'] = company_end_date
    target['current_location'] = [current_location]
    target['major'] = major
    target['skills_set'] = skills_set
    target['current_industry'] = [current_industry]

    return(target)

def build_es_query(target):

    # components of target array = ['id', [degree], [colleges], [college_start_dates], [company names], [company profile title], [company_start_date], [company_end_date], [current_location], [major], [skills_set]]

    all_degree_query = [] # it contains the AND query components for education (i.e degrees,major and start_date)
    degree_filter_only = [] # it contains the filter weight query for degrees and major(if exists)
    considered_degrees = [] # out of all valid and rubbish degree names, which degrees were chosen is maintained here using their index value
    filters = [] # it contains all the filter weights for used in es query
    considered_filter_degree = [] # it contains the index of considered degree filter
    # check if target degrees exists, if yes then check for corresponding major_name and start_date
    if 'institute_degree' in target:
        if  target['institute_degree'] != [None]:

           for index, input_degree in enumerate(target['institute_degree']):

               if input_degree != None:

                  degree_query = {
                     "bool": {
                        "must": [
                           {
                              "match": {
                                 "linkedin.educations.degree.name": input_degree.encode('utf8')
                              }
                           }
                        ]
                     }
                   }

                  # check if major exists for corresponding degree

                  if 'major' in target:
                      if target['major'][index] != None and target['major'][index] != '':

                         major = target['major'][index]
                         degree_query['bool']['must'].append({
                            "match": {
                               "linkedin.educations.major.name": major.encode('utf8')
                            }
                         })

                  # check if start_date exists for corresponding degree

                  if target['institute_start_date']:
                      if target['institute_start_date'][index] != None and target['institute_start_date'][index] != '':

                         degree_start_date = target['institute_start_date'][index]
                         degree_query['bool']['must'].append({
                               "range": {
                                  "linkedin.educations.start_date": {
                                     "gte": degree_start_date.encode('utf8') + "||-72M",
                                     "lte": degree_start_date.encode('utf8') + "||+12M",
                                  }
                               }
                            })

                  all_degree_query.append(degree_query)

                  considered_degrees.append(index)


    # if degree array does not exist, then coose major names and start date only
    elif target['institute_degree'] == [None] or ('institute_degree' not in target):

       print "NOT A PERFECT PROFILE" # because it does not have degree mentioned

       # check if major exists for corresponding degree

       if 'major' in target:
           if target['major'] != [None] and target['major'] != '':

              for index, major in enumerate(target['major']):

                 if major != None and major != '':

                     degree_query = {
                        "bool": {
                           "must": [
                              {
                                 "match": {
                                    "linkedin.educations.major.name": major.encode('utf8')
                                 }
                              }
                           ]
                        }
                      }

                     # check if start_date exists for corresponding degree

                     if 'institute_start_date' in target:
                         if target['institute_start_date'][index] != None and target['institute_start_date'][index] != '':

                            degree_start_date = target['institute_start_date'][index]
                            degree_query['bool']['must'].append({
                                  "range": {
                                     "linkedin.educations.start_date": {
                                        "gte": degree_start_date.encode('utf8') + "||-72M",
                                        "lte": degree_start_date.encode('utf8') + "||+12M",
                                     }
                                  }
                               })

                     all_degree_query.append(degree_query)

                     considered_degrees.append(index)

       # if degree_name and major_name does not exist, then select start_date only

       elif (target['major'] == [None] or ('major' not in target) or target['major'] == ['']) and ('institute_start_date' in target and target['institute_start_date'] != [None] and target['institute_start_date'] != ['']):

          for index, degree_start_date in enumerate(target['institute_start_date']):

             if degree_start_date != None and degree_start_date != '':

                 degree_query = {
                    "bool": {
                       "must": [
                          {
                             "range": {
                                "linkedin.educations.start_date": {
                                   "gte": degree_start_date.encode('utf8') + "||-72M",
                                   "lte": degree_start_date.encode('utf8') + "||+12M",
                                }
                             }
                          }
                       ]
                    }
                  }

                 all_degree_query.append(degree_query)

                 considered_degrees.append(index)



    # create filters for degree with major if exists with specific weights

    if considered_degrees:

        each_degree_weight = 28/float(len(considered_degrees))

        if target['institute_degree'] and target['institute_degree'] != [None]:

           for index, input_degree in enumerate(target['institute_degree']):

              if input_degree != None and input_degree != '':

                  filter_deg_query = {
                      "filter":{
                         "nested": {
                            "path": "linkedin.educations",
                            "query": {
                               "bool": {
                                 "must": [
                                    {
                                       "match": {
                                          "linkedin.educations.degree.name": input_degree.encode('utf8')
                                       }
                                    }
                                 ]
                              }
                            }
                         }
                      },
                      "weight": each_degree_weight
                   }


                  if target['major'] and target['major'][index] != None and target['major'][index] != '':

                     major = target['major'][index]
                     filter_deg_query['filter']['nested']['query']['bool']['must'].append({
                        "match": {
                           "linkedin.educations.major.name": major.encode('utf8')
                        }
                     })

                  filters.append(filter_deg_query)
                  degree_filter_only.append(filter_deg_query)
                  considered_filter_degree.append(index)

        # create filter for major, if degree does not exist with specific weights

        elif target['institute_degree'] == [None] or ('institute_degree' not in target):

           if target['major'] and target['major'] != [None] and target['major'] != ['']:
              each_degree_weight = 28/float(len(target['major']))

              for index, major in enumerate(target['major']):

                  if major != None and major != '':

                      filter_deg_query = {
                          "filter":{
                             "nested": {
                                "path": "linkedin.educations",
                                "query": {
                                   "bool": {
                                     "must": [
                                        {
                                           "match": {
                                              "linkedin.educations.major.name": major.encode('utf8')
                                           }
                                        }
                                     ]
                                  }
                                }
                             }
                          },
                          "weight": each_degree_weight
                       }

                      filters.append(filter_deg_query)
                      degree_filter_only.append(filter_deg_query)
                      considered_filter_degree.append(index)


    # create filter for colleges for consisdered degrees only, (i.e ignoring school names) with specific weights

    if considered_degrees:
       each_college_weight = 12/float(len(considered_degrees))
       for index in considered_degrees:

          if target['institute_name'] and target['institute_name'][index] != None and target['institute_name'][index] != '' :

             input_college = target['institute_name'][index]

             filter_college = {
                 "filter":{
                    "nested": {
                       "path": "linkedin.educations",
                       "query": {
                          "term": {
                             "linkedin.educations.institute.name.raw": {
                                "value": input_college.encode('utf8')
                             }
                          }
                       }
                    }
                 },
                "weight": each_college_weight
              }

             filters.append(filter_college)

    # if we did not catch considered degrees, then grab all the institue names as college degrees and create their filters with specific weights

    elif target['institute_name'] and target['institute_name'] != [None]:
       each_college_weight = 12/float(len(target['institute_name']))
       for index, college_name in enumerate(target['institute_name']):

          input_college = target['institute_name'][index]

          if input_college != None and input_college != '':

              filter_college = {
                  "filter":{
                     "nested": {
                        "path": "linkedin.educations",
                        "query": {
                           "term": {
                              "linkedin.educations.institute.name.raw": {
                                 "value": input_college.encode('utf8')
                              }
                           }
                        }
                     }
                  },
                 "weight": each_college_weight
               }

              filters.append(filter_college)



    # create filter for college start date using degree name, major name and start date if exists
    # created each filter covering all the time duration individually, as there was no other means to ...
    # do it except in script by looping in education of each profile which would be very time consuming

    if degree_filter_only:
        degree_filter_only_length = len(degree_filter_only)
        for index, fil_deg in enumerate(degree_filter_only):

            fil_index = considered_filter_degree[index]

            if target['institute_start_date'][fil_index] != None and target['institute_start_date'][fil_index] != '':

                degree_start_date = target['institute_start_date'][fil_index]

                filter_college_start_date = copy.deepcopy(fil_deg)

                filter_college_start_date['filter']['nested']['query']['bool']['must'].append(
                  {
                     "range": {
                        "linkedin.educations.start_date": {
                           "gte": degree_start_date.encode('utf8'),
                           "lte": degree_start_date.encode('utf8') + "||+11M"
                        }
                     }
                  })


                filter_college_start_date['weight'] = 16*0.2/float(degree_filter_only_length)

                filters.append(filter_college_start_date)


                filter_college_start_date1 = copy.deepcopy(fil_deg)
                filter_college_start_date1['filter']['nested']['query']['bool']['must'].append(
                    {
                     "range": {
                         "linkedin.educations.start_date": {
                            "gte": degree_start_date.encode('utf8') + "||-12M",
                            "lte": degree_start_date.encode('utf8') + "||-1M"
                         }
                      }
                    })

                filter_college_start_date1['weight'] = 16*0.6/degree_filter_only_length

                filters.append(filter_college_start_date1)


                filter_college_start_date2 = copy.deepcopy(fil_deg)
                filter_college_start_date2['filter']['nested']['query']['bool']['must'].append(
                    {
                     "range": {
                         "linkedin.educations.start_date": {
                            "gte": degree_start_date.encode('utf8') + "||-24M",
                            "lte": degree_start_date.encode('utf8') + "||-13M"
                         }
                      }
                    })

                filter_college_start_date2['weight'] = 16/degree_filter_only_length


                filters.append(filter_college_start_date2)


                filter_college_start_date3 = copy.deepcopy(fil_deg)
                filter_college_start_date3['filter']['nested']['query']['bool']['must'].append(
                    {
                     "range": {
                         "linkedin.educations.start_date": {
                            "gte": degree_start_date.encode('utf8') + "||-36M",
                            "lte": degree_start_date.encode('utf8') + "||-25M"
                         }
                      }
                    })

                filter_college_start_date3['weight'] = 16*0.8/degree_filter_only_length

                filters.append(filter_college_start_date3)


                filter_college_start_date4 = copy.deepcopy(fil_deg)
                filter_college_start_date4['filter']['nested']['query']['bool']['must'].append(
                    {
                     "range": {
                         "linkedin.educations.start_date": {
                            "gte": degree_start_date.encode('utf8') + "||-48M",
                            "lte": degree_start_date.encode('utf8') + "||-37M"
                         }
                      }
                    })

                filter_college_start_date4['weight'] = 16*0.4/degree_filter_only_length

                filters.append(filter_college_start_date4)

                filter_college_start_date = []
                filter_college_start_date1 = []
                filter_college_start_date2 = []
                filter_college_start_date3 = []
                filter_college_start_date4 = []


    # create filter for company names with specific weights
    company_length = 0
    if target['company_name'] and target['company_name'] != [None] and target['company_name'] != '':

       company_length = len(target['company_name'])
       for company_name in target['company_name']:

           if company_name != None and company_name != '':

               filter_company_name = {
                   "filter": {
                      "nested": {
                         "path": "linkedin.experiences",
                         "query": {
                            "match_phrase": {
                               "linkedin.experiences.company.name": company_name.encode('utf8')
                            }
                         }
                      }
                   },
                   "weight": 14/float(company_length)
                }

               filters.append(filter_company_name)

    # create filter for current location with specific weights

    curr_loc_name = target['current_location'][0]
    if curr_loc_name != None and curr_loc_name != '':
        filter_curr_loc = {
        "filter": {
           "term": {
              "linkedin.current_location": curr_loc_name.encode('utf8')
           }
        },
        "weight": 4
        }

        filters.append(filter_curr_loc)




    # create AND component having skill set

    skill_query = ''
    if target['skills_set'] and target['skills_set'] != [None] and target['skills_set'] != ['']:
       min_match = 0.4*len(target['skills_set'])

       min_match = int(min_match)

       if min_match > 6:
           min_match = 6

       skill_query = {
          "terms": {
             "linkedin.skills.raw": target['skills_set'],"minimum_match": min_match
          }
       }


    curr_ind = target['current_industry'][0]
    if curr_ind != None and curr_ind != '':
        filter_curr_ind = {
            "filter": {
               "term": {
                  "linkedin.current_industry": curr_ind.encode('utf8')
               }
            },
            "weight": 10
            }

        filters.append(filter_curr_ind)


    es_input_query = {
      "bool": {
         "must": []
      }
    }

    # if degree AND components created then append in the es query
    if all_degree_query:

        es_input_query['bool']['must'].append(
            {
                "nested": {
                  "path": "linkedin.educations",
                  "query": {
                      "bool": {
                         "should": [
                         ]
                      }
                   }
               }
            }
        )

        for degree in all_degree_query:
            es_input_query['bool']['must'][0]['nested']['query']['bool']['should'].append(degree)

    # if skills_set AND component created above, then append in the es query
    if skill_query:
        es_input_query['bool']['must'].append(skill_query)


    # if degree and skills_set AND components created above, then only create filter having scripting score, ...
    # otherwise the script would run on huge number of records if any of the AND components does not exist
    if (all_degree_query) and (skill_query):
        if target['company_start_date'] and target['company_start_date'][-1] != None and target['company_start_date'][-1] != '' and company_length != 0:
            first_job_start_date = target['company_start_date'][-1]
            first_job_start_date = re.match(r'^(\d{4})\-',first_job_start_date,re.I)
            first_job_start_date = first_job_start_date.group(1)

            first_job_start_date = str(first_job_start_date)
            company_length = str(company_length)

            # inp = raw_input("script_score")

            filters.append(
                {
                   "script_score": {
                       "script" : "f_score=0;total=0;def date='';target_year=" + first_job_start_date + ";target_no_jobs=" + company_length + ";for(exp in _source.linkedin.experiences){total=total+1;if(exp.containsKey('start_date')){date=exp['start_date'];};};if(total<=target_no_jobs){return(-100);};if(date!=''){def year = date[0..3].toInteger();year = year.toInteger();def diff=year-target_year;if((-4<diff) && (diff<0)){f_score=Math.abs(diff)*3;};else{return(-100);};};def job_diff=Math.abs(target_no_jobs-total+1);job_score=target_no_jobs-job_diff;job_score=(job_score/target_no_jobs)*11;return(f_score+job_score);"

                      # "script" : "f_score=0;total=0;def date='';target_year=" + first_job_start_date + ";target_no_jobs=" + company_length + ";for(exp in _source.linkedin.experiences){total=total+1;if(exp.containsKey('start_date')){date=exp['start_date'];};};if(total<=target_no_jobs){return(-100)};match=date=~/^(?<year>\\d{4})\\-/;if(match){def year=match.group('year').toInteger();def diff=Math.abs(target_year-year);if(diff<5){f_score=5-diff;f_score=f_score*3;};if(f_score<0){f_score=0};};def job_diff=Math.abs(target_no_jobs-total+1);job_score=target_no_jobs-job_diff;job_score=(job_score/target_no_jobs)*11;if(job_score<0){job_score=0;};f_score=f_score+job_score;return(f_score);"
                       # "script": "f_score=0;total=0;def date='';target_year=" + first_job_start_date + ";target_no_jobs=" + company_length + ";for(exp in _source.linkedin.experiences){total=total+1;if(exp.containsKey('start_date')){date=exp['start_date'];};};match=date=~/^(?<year>\\d{4})\\-/;if(match){def year=match.group('year').toInteger();def diff=Math.abs(target_year-year);if(diff<5){f_score=5-diff;f_score=f_score*3;};if(f_score<0){f_score=0};};def job_diff=Math.abs(target_no_jobs-total+1);job_score=target_no_jobs-job_diff;job_score=(job_score/target_no_jobs)*11;if(job_score<0){job_score=0;};f_score=f_score+job_score;return(f_score);"
                   }
                }
            )

    # if degree and skills_set AND component are NOT created then simply match_all the es query with the assigned filters

    if (not all_degree_query) and (not skill_query):
        es_input_query = {
            "match_all": {}
        }

        print "NOT A PERFECT PROFILE"

    print "es_input_query : ", es_input_query
    print "\n"
    print "filters : ", filters
    print "\n"

    final_es_query = {
           "size": MAX_FETCH_PROFILE,
           "query": {
              "function_score": {
                 "query": es_input_query,
                 "functions": filters,
                 "score_mode": "sum",
                 "boost_mode": "replace"
              }
           }
        }

    print "final_es_query : ", final_es_query

    return final_es_query

def go_to_past(profile):#,similar_users):


    creased_education_index = 0
    new_educations = []
    for education in profile['_source']['linkedin']['educations']:

        if 'start_date' in education:

            if education['start_date'] != None and education['start_date'] != '':

                if type(education['start_date']) == str or type(education['start_date']) == unicode:
                    start_date = datetime.strptime(education['start_date'], "%Y-%m-%dT%H:%M:%S.%fZ")
                else:
                    start_date = education['start_date']

                if start_date > TRACE_BACK_YEAR:
                    creased_education_index = creased_education_index + 1
                else:
                    new_educations = profile['_source']['linkedin']['educations'][creased_education_index : len(education)]
                    break


    new_experiences = []
    creased_experience_index = 0

    for experience in profile['_source']['linkedin']['experiences']:

        if 'start_date' in experience:

            if experience['start_date'] != None and experience['start_date'] != '':

                if type(experience['start_date']) == str or type(experience['start_date']) == unicode:
                    start_date = datetime.strptime(experience['start_date'], "%Y-%m-%dT%H:%M:%S.%fZ")
                else:
                    start_date = experience['start_date']

                if start_date > TRACE_BACK_YEAR:
                    creased_experience_index = creased_experience_index + 1
                else:
                    new_experiences = profile['_source']['linkedin']['experiences'][creased_experience_index : len(experience)]
                    break

    print "creased_experience_index",creased_experience_index
    print "creased_education_index", creased_education_index

    if not new_educations:
        new_educations = profile['_source']['linkedin']['educations']

    actual_end_date = None
    actual_start_date = None
    if not new_experiences:
        new_experiences = profile['_source']['linkedin']['experiences']
    else:
        try:
            actual_end_date = new_experiences[0]['end_date']
            print "actual_end_date",actual_end_date
            actual_start_date = new_experiences[0]['start_date']
            new_experiences[0]['end_date'] = None
            new_experiences[0]['current'] = True
        except:
            return('no end date')

    profile['_source']['linkedin']['educations'] = copy.deepcopy(new_educations)
    profile['_source']['linkedin']['experiences'] = copy.deepcopy(new_experiences)

    print get_job_switch_months(profile,actual_start_date,actual_end_date)#,similar_users)

def is_looking_for_job_change(profile):

    profile_text = profile['name'] if ('name' in profile) else ''
    profile_text += profile['headline'] if ('headline' in profile) else ''
    profile_text += profile['summary'] if ('summary' in profile) else ''
    if profile_text == '':
        return False

    # Code for checking if he has specifically written that he is
    # looking for job change or looking for opportunities.
    # active_text_list = ["looking for job",
    #                     "looking for opportunity",
    #                     "looking for opportunities",
    #                     "looking for change"
    #                     ]
    active_text_list = ["looking for"]
    for active_text in active_text_list:
        if active_text in profile_text:
            return True

    # Code for checking email id.
    email_matched = re.search(r'[\w\.-]+@[\w\.-]+', profile_text)
    if email_matched:
        return True

    # Code for checking phone number.
    # Assuming indian phone numbers
    if "+91" in profile_text:
        return True
    phone_matched = map(int, re.findall(r'(\d{10})', profile_text))
    if phone_matched:
        return True

    return False

def get_minimum_working_duration(currently_working_user_job_durations, current_job_duration_in_months):
    larger_durations = []
    for x in currently_working_user_job_durations:
        x = x/DAYS_IN_MONTH
        if x >= current_job_duration_in_months:
            larger_durations.append(x)
    if not len(larger_durations):
        return current_job_duration_in_months

    avg_min_duration = sum(larger_durations)/len(larger_durations)
    return avg_min_duration

def get_analysis_level_two(similar_user_job_durations, current_job_duration_in_months):
    # print"***** Entering get_analysis_level_two *****"

    longer_job_durations = []

    for jd in similar_user_job_durations:
        if jd - current_job_duration_in_months > -3:
            longer_job_durations.append(jd)

    # print"Longer Job Durations ", longer_job_durations
    counter = collections.Counter(longer_job_durations)
    # print counter

    nearest_local_maxima = 0

    print "Printing Top frequencies", counter.most_common(5)
    for x in counter.most_common(len(counter)):
        abs_diff_from_current_jd = abs(x[0] - current_job_duration_in_months)
        if abs_diff_from_current_jd < MONTHS_IN_YEAR:
            nearest_local_maxima = x[0]
            break

    # print"Nearest Local Maxima ", nearest_local_maxima
    if nearest_local_maxima == 0:
        return ((current_job_duration_in_months / MONTHS_IN_YEAR) + 1) * MONTHS_IN_YEAR

    if nearest_local_maxima < current_job_duration_in_months:
        # if the nearest local maxima found is less than current job duration
        # then use 25:75 to reply activeness
        if current_job_duration_in_months - nearest_local_maxima > MONTHS_IN_QUARTER_YEAR:
            return ((current_job_duration_in_months / MONTHS_IN_YEAR) + 1) * MONTHS_IN_YEAR
        else:
            return current_job_duration_in_months

    return nearest_local_maxima

def forcefully_get_analysis_level_two(similar_user_job_durations, current_job_duration_in_months, month_diff, profile_id):
    # print"***** Entering get_analysis_level_two *****"

    longer_job_durations = []

    for jd in similar_user_job_durations:
        if jd >= current_job_duration_in_months:
            longer_job_durations.append(jd)

    print"Longer Job Durations ", longer_job_durations
    counter = collections.Counter(longer_job_durations)
    # print counter

    nearest_local_maxima = 0

    print "Printing Top frequencies", counter.most_common()

    print "counter",counter

    count_to_3 = 1
    forcefully_write_str = ''

    for x in counter.most_common(len(counter)):

        if count_to_3 >= 4:
            break

        if x[0]>1:

            if count_to_3==1:
                nearest_local_maxima = x[0]

            forcefully_nearest_local_maxima = x[0]

            estimated_month_diff = forcefully_nearest_local_maxima - current_job_duration_in_months

            if (estimated_month_diff <= 4) and (month_diff <= 4):
                forcefully_write_str = str(profile_id) + "," + str(count_to_3) + "," + str(month_diff) + "," + str(estimated_month_diff) + ",pos,1" + "\n"
                forcefully_f.write(forcefully_write_str)
                return(forcefully_nearest_local_maxima)
            elif (estimated_month_diff > 4) and (month_diff > 4):
                forcefully_write_str = str(profile_id) + "," + str(count_to_3) + "," + str(month_diff) + "," + str(estimated_month_diff) + ",neg,1" + "\n"
                forcefully_f.write(forcefully_write_str)
                return(forcefully_nearest_local_maxima)
        count_to_3 += 1

    if forcefully_write_str == '' and month_diff <= 4:
        forcefully_write_str = str(profile_id) + ",not in 3," + str(month_diff) + "," + str(nearest_local_maxima - current_job_duration_in_months) + ",pos,0" + "\n"
    elif (forcefully_write_str == '' and month_diff > 4):
        forcefully_write_str = str(profile_id) + ",not in 3," + str(month_diff) + "," + str(nearest_local_maxima - current_job_duration_in_months) + ",neg,0" + "\n"

    #     abs_diff_from_current_jd = abs(x[0] - current_job_duration_in_months)
    #     if abs_diff_from_current_jd < MONTHS_IN_YEAR:
    #         nearest_local_maxima = x[0]
    #         break

    if nearest_local_maxima == 0:
        nearest_local_maxima = ((current_job_duration_in_months / MONTHS_IN_YEAR) + 1) * MONTHS_IN_YEAR
        print"Nearest Local Maxima exceptional case I: ", nearest_local_maxima
        estimated_month_diff = nearest_local_maxima - current_job_duration_in_months

        if estimated_month_diff <= 4 and month_diff<= 4:
            forcefully_write_str = str(profile_id) + ",ex1," + str(month_diff) + "," + str(nearest_local_maxima - current_job_duration_in_months) + ",pos,1" + "\n"
        elif estimated_month_diff > 4 and month_diff > 4:
            forcefully_write_str = str(profile_id) + ",ex1," + str(month_diff) + "," + str(nearest_local_maxima - current_job_duration_in_months) + ",neg,1" + "\n"
        else:
            if month_diff <= 4:
                forcefully_write_str = str(profile_id) + ",ex1," + str(month_diff) + "," + str(nearest_local_maxima - current_job_duration_in_months) + ",pos,0" + "\n"
            else:
                forcefully_write_str = str(profile_id) + ",ex1," + str(month_diff) + "," + str(nearest_local_maxima - current_job_duration_in_months) + ",neg,0" + "\n"

        forcefully_f.write(forcefully_write_str)
        return ((current_job_duration_in_months / MONTHS_IN_YEAR) + 1) * MONTHS_IN_YEAR

    if (nearest_local_maxima < current_job_duration_in_months):
        # if the nearest local maxima found is less than current job duration
        # then use 25:75 to reply activeness

        # print "here in exceptional case"

        if current_job_duration_in_months - nearest_local_maxima > MONTHS_IN_QUARTER_YEAR:
            nearest_local_maxima = ((current_job_duration_in_months / MONTHS_IN_YEAR) + 1) * MONTHS_IN_YEAR
            print"Nearest Local Maxima before exceptional case II : ", nearest_local_maxima
            estimated_month_diff = nearest_local_maxima - month_diff

            if estimated_month_diff <= 4 and month_diff<= 4:
                forcefully_write_str = str(profile_id) + ",ex2," + str(month_diff) + "," + str(nearest_local_maxima - current_job_duration_in_months) + ",pos,1" + "\n"
            elif estimated_month_diff > 4 and month_diff > 4:
                forcefully_write_str = str(profile_id) + ",ex2," + str(month_diff) + "," + str(nearest_local_maxima - current_job_duration_in_months) + ",neg,1" + "\n"
            else:
                if month_diff <= 4:
                    forcefully_write_str = str(profile_id) + ",ex2," + str(month_diff) + "," + str(nearest_local_maxima - current_job_duration_in_months) + ",pos,0" + "\n"
                else:
                    forcefully_write_str = str(profile_id) + ",ex2," + str(month_diff) + "," + str(nearest_local_maxima - current_job_duration_in_months) + ",neg,0" + "\n"

            forcefully_f.write(forcefully_write_str)

            return ((current_job_duration_in_months / MONTHS_IN_YEAR) + 1) * MONTHS_IN_YEAR
        else:
            nearest_local_maxima = current_job_duration_in_months
            print"Nearest Local Maxima before exceptional case III : ", nearest_local_maxima

            estimated_month_diff = nearest_local_maxima - month_diff

            if estimated_month_diff <= 4 and month_diff<= 4:
                forcefully_write_str = str(profile_id) + ",ex3," + str(month_diff) + "," + str(nearest_local_maxima - current_job_duration_in_months) + ",pos,1" + "\n"
            elif estimated_month_diff > 4 and month_diff > 4:
                forcefully_write_str = str(profile_id) + ",ex3," + str(month_diff) + "," + str(nearest_local_maxima - current_job_duration_in_months) + ",neg,1" + "\n"
            else:
                if month_diff <= 4:
                    forcefully_write_str = str(profile_id) + ",ex3," + str(month_diff) + "," + str(nearest_local_maxima - current_job_duration_in_months) + ",pos,0" + "\n"
                else:
                    forcefully_write_str = str(profile_id) + ",ex3," + str(month_diff) + "," + str(nearest_local_maxima - current_job_duration_in_months) + ",neg,0" + "\n"

            forcefully_f.write(forcefully_write_str)
            return current_job_duration_in_months

    print "Nearest Local Maxima",nearest_local_maxima
    forcefully_f.write(forcefully_write_str)

    return nearest_local_maxima

def forcefully_study_get_analysis_level_two(similar_user_job_durations, current_job_duration_in_months, month_diff, profile_id,target_profile):
    # print"***** Entering get_analysis_level_two *****"

    target_profile = create_target_dict(target_profile)

    print "len of my dict",len(similar_user_job_durations)
    longer_job_durations = []
    peak_profile_dict = {}

    for jd in similar_user_job_durations:
        if jd >= current_job_duration_in_months:
            jd_list = [jd] * len(similar_user_job_durations[jd])
            longer_job_durations.extend(jd_list)


    print"Longer Job Durations ", longer_job_durations
    counter = collections.Counter(longer_job_durations)
    # print counter

    nearest_local_maxima = 0

    print "Printing Top frequencies", counter.most_common()

    print "counter",counter

    count_to_3 = 1
    forcefully_write_str = ''

    for x in counter.most_common(len(counter)):

        if count_to_3 >= 6:
            break

        if x[0]>1:

            if count_to_3==1:
                nearest_local_maxima = x[0]

            forcefully_nearest_local_maxima = x[0]

            estimated_month_diff = forcefully_nearest_local_maxima - current_job_duration_in_months

            if (estimated_month_diff <= 4) and (month_diff <= 4):
                forcefully_write_str = str(profile_id) + "," + str(count_to_3) + "," + str(month_diff) + "," + str(estimated_month_diff) + ",pos,1" + "\n"
                forcefully_f.write(forcefully_write_str)

                forcefully_study_write_str = str(profile_id) + ",peak_no " + str(count_to_3) + "\n" + str([target_profile['company_title'],target_profile['current_industry'],target_profile['current_location']]) + "\n" + str(similar_user_job_durations[forcefully_nearest_local_maxima]) + "\n#############################################################################\n"
                forcefully_study_f_pos.write(forcefully_study_write_str)

                if len(counter.most_common(len(counter))) >= 1:
                    forcefully_other_peaks_study_write_str = str(profile_id) + ",peak_no " + str(1) + "\n" + str([target_profile['company_title'],target_profile['current_industry'],target_profile['current_location']]) + "\n" + str(similar_user_job_durations[counter.most_common(len(counter))[0][0]]) + "\n$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$\n"
                    forcefully_study_f_pos.write(forcefully_other_peaks_study_write_str)
                if len(counter.most_common(len(counter))) >= 2:
                    forcefully_other_peaks_study_write_str = str(profile_id) + ",peak_no " + str(2) + "\n" + str([target_profile['company_title'],target_profile['current_industry'],target_profile['current_location']]) + "\n" + str(similar_user_job_durations[counter.most_common(len(counter))[1][0]]) + "\n$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$\n"
                    forcefully_study_f_pos.write(forcefully_other_peaks_study_write_str)
                if len(counter.most_common(len(counter))) >= 3:
                    forcefully_other_peaks_study_write_str = str(profile_id) + ",peak_no " + str(3) + "\n" + str([target_profile['company_title'],target_profile['current_industry'],target_profile['current_location']]) + "\n" + str(similar_user_job_durations[counter.most_common(len(counter))[2][0]]) + "\n$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$\n"
                    forcefully_study_f_pos.write(forcefully_other_peaks_study_write_str)
                if len(counter.most_common(len(counter))) >= 4:
                    forcefully_other_peaks_study_write_str = str(profile_id) + ",peak_no " + str(4) + "\n" + str([target_profile['company_title'],target_profile['current_industry'],target_profile['current_location']]) + "\n" + str(similar_user_job_durations[counter.most_common(len(counter))[3][0]]) + "\n$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$\n"
                    forcefully_study_f_pos.write(forcefully_other_peaks_study_write_str)
                if len(counter.most_common(len(counter))) >= 5:
                    forcefully_other_peaks_study_write_str = str(profile_id) + ",peak_no " + str(5) + "\n" + str([target_profile['company_title'],target_profile['current_industry'],target_profile['current_location']]) + "\n" + str(similar_user_job_durations[counter.most_common(len(counter))[4][0]]) + "\n$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$\n"
                    forcefully_study_f_pos.write(forcefully_other_peaks_study_write_str)
                forcefully_other_peaks_study_write_str = "\n--------------------------------------------------------------------------------------------------\n\n\n"
                forcefully_study_f_pos.write(forcefully_other_peaks_study_write_str)
                return(forcefully_nearest_local_maxima)
            elif (estimated_month_diff > 4) and (month_diff > 4):
                forcefully_write_str = str(profile_id) + "," + str(count_to_3) + "," + str(month_diff) + "," + str(estimated_month_diff) + ",neg,1" + "\n"
                forcefully_f.write(forcefully_write_str)
                forcefully_study_write_str = str(profile_id) + ",peak_no " + str(count_to_3) + "\n" + str([target_profile['company_title'],target_profile['current_industry'],target_profile['current_location']]) + "\n" + str(similar_user_job_durations[forcefully_nearest_local_maxima]) + "\n#############################################################################\n"
                forcefully_study_f_neg.write(forcefully_study_write_str)

                if len(counter.most_common(len(counter))) >= 1:
                    forcefully_other_peaks_study_write_str = str(profile_id) + ",peak_no " + str(1) + "\n" + str([target_profile['company_title'],target_profile['current_industry'],target_profile['current_location']]) + "\n" + str(similar_user_job_durations[counter.most_common(len(counter))[0][0]]) + "\n$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$\n"
                    forcefully_study_f_neg.write(forcefully_other_peaks_study_write_str)
                if len(counter.most_common(len(counter))) >= 2:
                    forcefully_other_peaks_study_write_str = str(profile_id) + ",peak_no " + str(2) + "\n" + str([target_profile['company_title'],target_profile['current_industry'],target_profile['current_location']]) + "\n" + str(similar_user_job_durations[counter.most_common(len(counter))[1][0]]) + "\n$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$\n"
                    forcefully_study_f_neg.write(forcefully_other_peaks_study_write_str)
                if len(counter.most_common(len(counter))) >= 3:
                    forcefully_other_peaks_study_write_str = str(profile_id) + ",peak_no " + str(3) + "\n" + str([target_profile['company_title'],target_profile['current_industry'],target_profile['current_location']]) + "\n" + str(similar_user_job_durations[counter.most_common(len(counter))[2][0]]) + "\n$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$\n"
                    forcefully_study_f_neg.write(forcefully_other_peaks_study_write_str)
                if len(counter.most_common(len(counter))) >= 4:
                    forcefully_other_peaks_study_write_str = str(profile_id) + ",peak_no " + str(4) + "\n" + str([target_profile['company_title'],target_profile['current_industry'],target_profile['current_location']]) + "\n" + str(similar_user_job_durations[counter.most_common(len(counter))[3][0]]) + "\n$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$\n"
                    forcefully_study_f_neg.write(forcefully_other_peaks_study_write_str)
                if len(counter.most_common(len(counter))) >= 5:
                    forcefully_other_peaks_study_write_str = str(profile_id) + ",peak_no " + str(5) + "\n" + str([target_profile['company_title'],target_profile['current_industry'],target_profile['current_location']]) + "\n" + str(similar_user_job_durations[counter.most_common(len(counter))[4][0]]) + "\n$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$\n"
                    forcefully_study_f_neg.write(forcefully_other_peaks_study_write_str)
                forcefully_other_peaks_study_write_str = "\n--------------------------------------------------------------------------------------------------\n\n\n"
                forcefully_study_f_neg.write(forcefully_other_peaks_study_write_str)
                return(forcefully_nearest_local_maxima)
        count_to_3 += 1

    if forcefully_write_str == '' and month_diff <= 4:
        forcefully_write_str = str(profile_id) + ",not in 3," + str(month_diff) + "," + str(nearest_local_maxima - current_job_duration_in_months) + ",pos,0" + "\n"
    elif (forcefully_write_str == '' and month_diff > 4):
        forcefully_write_str = str(profile_id) + ",not in 3," + str(month_diff) + "," + str(nearest_local_maxima - current_job_duration_in_months) + ",neg,0" + "\n"

    #     abs_diff_from_current_jd = abs(x[0] - current_job_duration_in_months)
    #     if abs_diff_from_current_jd < MONTHS_IN_YEAR:
    #         nearest_local_maxima = x[0]
    #         break

    if nearest_local_maxima == 0:
        nearest_local_maxima = ((current_job_duration_in_months / MONTHS_IN_YEAR) + 1) * MONTHS_IN_YEAR
        print"Nearest Local Maxima exceptional case I: ", nearest_local_maxima
        estimated_month_diff = nearest_local_maxima - current_job_duration_in_months

        if estimated_month_diff <= 4 and month_diff<= 4:
            forcefully_write_str = str(profile_id) + ",ex1," + str(month_diff) + "," + str(nearest_local_maxima - current_job_duration_in_months) + ",pos,1" + "\n"
        elif estimated_month_diff > 4 and month_diff > 4:
            forcefully_write_str = str(profile_id) + ",ex1," + str(month_diff) + "," + str(nearest_local_maxima - current_job_duration_in_months) + ",neg,1" + "\n"
        else:
            if month_diff <= 4:
                forcefully_write_str = str(profile_id) + ",ex1," + str(month_diff) + "," + str(nearest_local_maxima - current_job_duration_in_months) + ",pos,0" + "\n"
            else:
                forcefully_write_str = str(profile_id) + ",ex1," + str(month_diff) + "," + str(nearest_local_maxima - current_job_duration_in_months) + ",neg,0" + "\n"

        forcefully_f.write(forcefully_write_str)
        return ((current_job_duration_in_months / MONTHS_IN_YEAR) + 1) * MONTHS_IN_YEAR

    if (nearest_local_maxima < current_job_duration_in_months):
        # if the nearest local maxima found is less than current job duration
        # then use 25:75 to reply activeness

        # print "here in exceptional case"

        if current_job_duration_in_months - nearest_local_maxima > MONTHS_IN_QUARTER_YEAR:
            nearest_local_maxima = ((current_job_duration_in_months / MONTHS_IN_YEAR) + 1) * MONTHS_IN_YEAR
            print"Nearest Local Maxima before exceptional case II : ", nearest_local_maxima
            estimated_month_diff = nearest_local_maxima - month_diff

            if estimated_month_diff <= 4 and month_diff<= 4:
                forcefully_write_str = str(profile_id) + ",ex2," + str(month_diff) + "," + str(nearest_local_maxima - current_job_duration_in_months) + ",pos,1" + "\n"
            elif estimated_month_diff > 4 and month_diff > 4:
                forcefully_write_str = str(profile_id) + ",ex2," + str(month_diff) + "," + str(nearest_local_maxima - current_job_duration_in_months) + ",neg,1" + "\n"
            else:
                if month_diff <= 4:
                    forcefully_write_str = str(profile_id) + ",ex2," + str(month_diff) + "," + str(nearest_local_maxima - current_job_duration_in_months) + ",pos,0" + "\n"
                else:
                    forcefully_write_str = str(profile_id) + ",ex2," + str(month_diff) + "," + str(nearest_local_maxima - current_job_duration_in_months) + ",neg,0" + "\n"

            forcefully_f.write(forcefully_write_str)

            return ((current_job_duration_in_months / MONTHS_IN_YEAR) + 1) * MONTHS_IN_YEAR
        else:
            nearest_local_maxima = current_job_duration_in_months
            print"Nearest Local Maxima before exceptional case III : ", nearest_local_maxima

            estimated_month_diff = nearest_local_maxima - month_diff

            if estimated_month_diff <= 4 and month_diff<= 4:
                forcefully_write_str = str(profile_id) + ",ex3," + str(month_diff) + "," + str(nearest_local_maxima - current_job_duration_in_months) + ",pos,1" + "\n"
            elif estimated_month_diff > 4 and month_diff > 4:
                forcefully_write_str = str(profile_id) + ",ex3," + str(month_diff) + "," + str(nearest_local_maxima - current_job_duration_in_months) + ",neg,1" + "\n"
            else:
                if month_diff <= 4:
                    forcefully_write_str = str(profile_id) + ",ex3," + str(month_diff) + "," + str(nearest_local_maxima - current_job_duration_in_months) + ",pos,0" + "\n"
                else:
                    forcefully_write_str = str(profile_id) + ",ex3," + str(month_diff) + "," + str(nearest_local_maxima - current_job_duration_in_months) + ",neg,0" + "\n"

            forcefully_f.write(forcefully_write_str)
            return current_job_duration_in_months

    print "Nearest Local Maxima",nearest_local_maxima
    forcefully_f.write(forcefully_write_str)

    return nearest_local_maxima

def next_peak_get_analysis_level_two(similar_user_job_durations, current_job_duration_in_months):
    # print"***** Entering get_analysis_level_two *****"

    longer_job_durations = []

    for jd in similar_user_job_durations:
        if jd >= current_job_duration_in_months:
            longer_job_durations.append(jd)

    print"Longer Job Durations ", longer_job_durations
    counter = collections.Counter(longer_job_durations)
    # print counter

    nearest_local_maxima = 0

    print "Printing Top frequencies", counter.most_common()

    print "counter",counter

    for x in counter.most_common(len(counter)):
        if x[0]>1:
            nearest_local_maxima = x[0]
            break
        break
    #     abs_diff_from_current_jd = abs(x[0] - current_job_duration_in_months)
    #     if abs_diff_from_current_jd < MONTHS_IN_YEAR:
    #         nearest_local_maxima = x[0]
    #         break

    if nearest_local_maxima == 0:
        print"Nearest Local Maxima exceptional case I: ", nearest_local_maxima
        return ((current_job_duration_in_months / MONTHS_IN_YEAR) + 1) * MONTHS_IN_YEAR

    if nearest_local_maxima < current_job_duration_in_months:
        # if the nearest local maxima found is less than current job duration
        # then use 25:75 to reply activeness

        # print "here in exceptional case"

        if current_job_duration_in_months - nearest_local_maxima > MONTHS_IN_QUARTER_YEAR:
            print"Nearest Local Maxima before exceptional case II : ", nearest_local_maxima
            return ((current_job_duration_in_months / MONTHS_IN_YEAR) + 1) * MONTHS_IN_YEAR
        else:
            print"Nearest Local Maxima before exceptional case III : ", nearest_local_maxima
            return current_job_duration_in_months

    print "Nearest Local Maxima",nearest_local_maxima

    return nearest_local_maxima

def next_prev_peak_get_analysis_level_two(similar_user_job_durations, current_job_duration_in_months):
    # print"***** Entering get_analysis_level_two *****"

    longer_job_durations = []

    for jd in similar_user_job_durations:
        if (jd - current_job_duration_in_months) > -3:
            longer_job_durations.append(jd)

    print"Longer Job Durations ", longer_job_durations
    counter = collections.Counter(longer_job_durations)
    # print counter

    nearest_local_maxima = 0

    print "Printing Top frequencies", counter.most_common()

    print "counter",counter

    for x in counter.most_common(len(counter)):
        if x[0]>1:
            nearest_local_maxima = x[0]
            break
        break
    #     abs_diff_from_current_jd = abs(x[0] - current_job_duration_in_months)
    #     if abs_diff_from_current_jd < MONTHS_IN_YEAR:
    #         nearest_local_maxima = x[0]
    #         break

    if nearest_local_maxima == 0:
        print"Nearest Local Maxima exceptional case I: ", nearest_local_maxima
        return ((current_job_duration_in_months / MONTHS_IN_YEAR) + 1) * MONTHS_IN_YEAR

    if nearest_local_maxima < current_job_duration_in_months:
        # if the nearest local maxima found is less than current job duration
        # then use 25:75 to reply activeness

        # print "here in exceptional case"

        if current_job_duration_in_months - nearest_local_maxima > MONTHS_IN_QUARTER_YEAR:
            print"Nearest Local Maxima before exceptional case II : ", nearest_local_maxima
            return ((current_job_duration_in_months / MONTHS_IN_YEAR) + 1) * MONTHS_IN_YEAR
        else:
            print"Nearest Local Maxima before exceptional case III : ", nearest_local_maxima
            return current_job_duration_in_months

    print "Nearest Local Maxima",nearest_local_maxima

    return nearest_local_maxima

def get_job_switch_months(profile,actual_start_date,actual_end_date):#,similar_users):
    print"***** Entering get_job_switch_months *****"

    profile_id = str(profile['_id'])

    if 'linkedin' not in profile['_source']:
        return -1

    if is_looking_for_job_change(profile):
        return LOOKING_FOR_JOB_CHANGE

    educations = profile['_source']['linkedin']['educations'] if ('educations' in profile['_source']['linkedin']) else []
    experiences = profile['_source']['linkedin']['experiences'] if ('experiences' in profile['_source']['linkedin']) else []

    job_durations, intern_durations, remove_index = get_job_durations(experiences, educations)

    new_experiences = []
    new_educations = []

    if remove_index:

        remove_ex_indexes = []
        remove_ed_indexes = []
        for key, value in remove_index.iteritems():

            if key == 'ex':
                remove_ex_indexes.append(value)
            if key == 'ed':
                remove_ed_indexes.append(value)

        if remove_ex_indexes:
            new_experiences = [value for index, value in enumerate(experiences) if index not in remove_ex_indexes]

        if remove_ed_indexes:
            new_educations = [value for index, value in enumerate(educations) if index not in remove_ed_indexes]

    if new_experiences:
        profile['_source']['linkedin']['experiences'] = copy.deepcopy(new_experiences)

    if new_educations:
        profile['_source']['linkedin']['educations'] = copy.deepcopy(new_educations)

    print "new profile", profile

    job_count = len(job_durations)

    if job_count < 1:
        return -2


    current_job_duration = job_durations[-1]
    current_job_duration_in_months = current_job_duration/DAYS_IN_MONTH
    print "current_job_duration", current_job_duration_in_months

    month_diff = None
    if (type(actual_end_date) == str or type(actual_end_date) == unicode) and (actual_end_date != None):
        actual_end_date = datetime.strptime(actual_end_date, "%Y-%m-%dT%H:%M:%S.%fZ")

        # actual_start_date = datetime.strptime(actual_start_date, "%Y-%m-%dT%H:%M:%S.%fZ")

        month_diff = (actual_end_date - TRACE_BACK_YEAR).days/DAYS_IN_MONTH

    print "*****Actual job month diff", month_diff

    if month_diff == None or month_diff <=0: #sharma
        return -3

    target = create_target_dict(profile)

    print "new_target", target

    final_es_query = build_es_query(target)

    try:
        similar_users = prod_es.search(index=es_index, doc_type=es_type, body=final_es_query, request_timeout=30000)
        # result = db.save({"target_id": profile_id,"similar_profiles":similar_users})
        # print "data saved", result
    except Exception as e:
        print e
        return(0)


    if similar_users['hits']['total'] > 0:
        print "profiles fetched", similar_users['hits']['total']
        similar_users = similar_users['hits']['hits']

    similar_user_job_durations = {}
    same_user_job_durations = []

    for similar_user in similar_users:
        # print similar_user
        if '_source' in similar_user:

            if 'experiences' in similar_user['_source']['linkedin'] and len(similar_user['_source']['linkedin']['experiences']) >= job_count:

                if 'experiences' in similar_user['_source']['linkedin'] and len(similar_user['_source']['linkedin']['experiences']) >= job_count:
                    jds, ids, remove_index = get_job_durations(similar_user['_source']['linkedin']['experiences'], similar_user['_source']['linkedin']['educations'])
                    # print jds
                    if len(jds) > job_count:
                        print "similar_user", similar_user['_id']

                        similar_user_dict = create_target_dict(similar_user)

                        if jds[job_count-1]/DAYS_IN_MONTH in similar_user_job_durations:
                            similar_user_job_durations[(jds[job_count-1])/DAYS_IN_MONTH].append([similar_user_dict['company_title'],similar_user_dict['current_industry'],similar_user_dict['current_location']])
                        else:
                            similar_user_job_durations[(jds[job_count-1])/DAYS_IN_MONTH] = [[similar_user_dict['company_title'],similar_user_dict['current_industry'],similar_user_dict['current_location']]]
                    elif len(jds) == job_count:
                        same_user_job_durations.append(jds[job_count-1])

    similar_users_count = len(similar_user_job_durations)

    print"Printing number of similar users : ", similar_users_count

    # Performing Analysis after getting similar users

    print "current_job_duration", current_job_duration_in_months
    print "*****Actual job month diff", month_diff

    # print "similar_user_job_durations", similar_user_job_durations

    # If no similar user is found, What we can suggest is that
    # he will complete his this year if more than 3 moths passed
    # else ready to switch (25:75 thing)
    if (similar_users_count == 0 ) or (similar_users_count < MIN_SIMILAR_USERS_REQUIRED):
        similar_users_count = len(same_user_job_durations)
        if similar_users_count < MIN_SIMILAR_USERS_REQUIRED:
            months_into_current_job = (current_job_duration/DAYS_IN_MONTH) % MONTHS_IN_YEAR
            if months_into_current_job > MONTHS_IN_QUARTER_YEAR:
                job_switch_months_left = MONTHS_IN_YEAR - months_into_current_job
                str_write = profile_id + "," + str(month_diff) + "," + str(job_switch_months_left) + "\n"
                f.write(str_write)

                if job_switch_months_left <= 4 and month_diff<= 4:
                    forcefully_write_str = str(profile_id) + ",less_sim 1," + str(month_diff) + "," + str(job_switch_months_left) + ",pos,1" + "\n"
                elif job_switch_months_left > 4 and month_diff > 4:
                    forcefully_write_str = str(profile_id) + ",less_sim 1," + str(month_diff) + "," + str(job_switch_months_left) + ",neg,1" + "\n"
                elif month_diff <= 4:
                    forcefully_write_str = str(profile_id) + ",less_sim 1," + str(month_diff) + "," + str(job_switch_months_left) + ",pos,0" + "\n"
                else:
                    forcefully_write_str = str(profile_id) + ",less_sim 1," + str(month_diff) + "," + str(job_switch_months_left) + ",neg,0" + "\n"
                forcefully_f.write(forcefully_write_str)
            else:
                job_switch_months_left = 0
                print "Months Remaining to Switch", job_switch_months_left
                str_write = profile_id + "," + str(month_diff) + "," + str(job_switch_months_left) + "\n"
                f.write(str_write)

                if job_switch_months_left <= 4 and month_diff<= 4:
                    forcefully_write_str = str(profile_id) + ",less_sim 2," + str(month_diff) + "," + str(job_switch_months_left) + ",pos,1" + "\n"
                elif job_switch_months_left > 4 and month_diff > 4:
                    forcefully_write_str = str(profile_id) + ",less_sim 2," + str(month_diff) + "," + str(job_switch_months_left) + ",neg,1" + "\n"
                elif month_diff <= 4:
                    forcefully_write_str = str(profile_id) + ",less_sim 2," + str(month_diff) + "," + str(job_switch_months_left) + ",pos,0" + "\n"
                else:
                    forcefully_write_str = str(profile_id) + ",less_sim 2," + str(month_diff) + "," + str(job_switch_months_left) + ",neg,0" + "\n"
                forcefully_f.write(forcefully_write_str)

            return job_switch_months_left
        else:
            minimum_working_duration = get_minimum_working_duration(same_user_job_durations, current_job_duration_in_months)
            if minimum_working_duration - current_job_duration_in_months > 3:
                str_write = profile_id + "," + str(month_diff) + "," + str(PASSIVE) + "\n"
                f.write(str_write)

                if PASSIVE <= 4 and month_diff<= 4:
                    forcefully_write_str = str(profile_id) + ",less_sim 1," + str(month_diff) + "," + str(PASSIVE) + ",pos,1" + "\n"
                elif PASSIVE > 4 and month_diff > 4:
                    forcefully_write_str = str(profile_id) + ",less_sim 1," + str(month_diff) + "," + str(PASSIVE) + ",neg,1" + "\n"
                elif month_diff <= 4:
                    forcefully_write_str = str(profile_id) + ",less_sim 1," + str(month_diff) + "," + str(PASSIVE) + ",pos,0" + "\n"
                else:
                    forcefully_write_str = str(profile_id) + ",less_sim 1," + str(month_diff) + "," + str(PASSIVE) + ",neg,0" + "\n"
                forcefully_f.write(forcefully_write_str)

                return PASSIVE
            else:
                job_switch_months_left = minimum_working_duration - current_job_duration_in_months + 1
                str_write = profile_id + "," + str(month_diff) + "," + str(minimum_working_duration - current_job_duration_in_months + 1) + "\n"
                f.write(str_write)

                if job_switch_months_left <= 4 and month_diff<= 4:
                    forcefully_write_str = str(profile_id) + ",less_sim 1," + str(month_diff) + "," + str(job_switch_months_left) + ",pos,1" + "\n"
                elif job_switch_months_left > 4 and month_diff > 4:
                    forcefully_write_str = str(profile_id) + ",less_sim 1," + str(month_diff) + "," + str(job_switch_months_left) + ",neg,1" + "\n"
                elif month_diff <= 4:
                    forcefully_write_str = str(profile_id) + ",less_sim 1," + str(month_diff) + "," + str(job_switch_months_left) + ",pos,0" + "\n"
                else:
                    forcefully_write_str = str(profile_id) + ",less_sim 1," + str(month_diff) + "," + str(job_switch_months_left) + ",neg,0" + "\n"
                forcefully_f.write(forcefully_write_str)

                return minimum_working_duration - current_job_duration_in_months + 1

    # Doing some more analysis over the returned array of job durations.
    # similar_user_job_durations = [x/DAYS_IN_MONTH for x in similar_user_job_durations]
    print "similar_user_job_durations in days", similar_user_job_durations
    optimal_job_duration = forcefully_study_get_analysis_level_two(similar_user_job_durations, current_job_duration_in_months, month_diff,profile_id,profile)
    months_remaining_to_switch = optimal_job_duration - current_job_duration_in_months

    print "Months Remaining to Switch", months_remaining_to_switch
    print "RESULT*********************************************"
    str_write = profile_id + "," + str(month_diff) + "," + str(months_remaining_to_switch) + "\n"
    print str_write
    f.write(str_write)

    # similar_user_job_durations = [x/DAYS_IN_MONTH for x in similar_user_job_durations]
    # print "similar_user_job_durations in months", similar_user_job_durations
    # optimal_job_duration = get_analysis_level_two(similar_user_job_durations, current_job_duration_in_months)
    # months_remaining_to_switch = optimal_job_duration - current_job_duration_in_months
    #
    # print "Months Remaining to Switch", months_remaining_to_switch
    # print "RESULT*********************************************"
    # str_write = profile_id + "," + str(month_diff) + "," + str(months_remaining_to_switch) + "\n"
    # print str_write
    # f.write(str_write)

    # print "similar_user_job_durations in months next_peak", similar_user_job_durations
    # optimal_job_duration = next_peak_get_analysis_level_two(similar_user_job_durations, current_job_duration_in_months)
    # months_remaining_to_switch = optimal_job_duration - current_job_duration_in_months
    #
    # print "Months Remaining to Switch next_peak", months_remaining_to_switch
    # print "RESULT next_peak*********************************************"
    # str_write = profile_id + "," + str(month_diff) + "," + str(months_remaining_to_switch) + "\n"
    # print str_write
    # f_next_peak.write(str_write)
    #
    # print "similar_user_job_durations in months next_peak", similar_user_job_durations
    # optimal_job_duration = next_prev_peak_get_analysis_level_two(similar_user_job_durations, current_job_duration_in_months)
    # months_remaining_to_switch = optimal_job_duration - current_job_duration_in_months
    #
    # print "Months Remaining to Switch next_prev_peak", months_remaining_to_switch
    # print "RESULT next_peak*********************************************"
    # str_write = profile_id + "," + str(month_diff) + "," + str(months_remaining_to_switch) + "\n"
    # print str_write
    # f_next_prev_peak.write(str_write)

    if months_remaining_to_switch < 1:
        return 0
    return months_remaining_to_switch


if __name__ == '__main__':
    print "In main"

    fetch_target_query = {
       "query": {
          "match": {
             "linkedin.current_location.name": {
                "query": "Mumbai Bangalore Delhi Hyderabad Chennai Pune Kolkata Ahmedabad",
                "operator": "or"
             }
          }
       }
    }

    #55028a30650b84a55fbfa289,5501d54e650b84a55faa4c12,550035e8650b84a55f5edcb9

    # fetch_target_query = {
    #     "query": {
    #         "term": {
    #            "_id": {
    #               "value": "5500366a650b84a55f5ef8cb"
    #            }
    #         }
    #     }
    # }


    # print "fetching 20000 input profiles"

    args_array = sys.argv

    # print "here", args_array
    #
    # if len(args_array) > 1:
    #     from_num = args_array[1]
    #     from_num = int(from_num)
    #     fetch_target_query["from"] = from_num
    #
    total_fetched = 0

    try:
        resp = prod_es.search(index=es_index, doc_type=es_type, body=fetch_target_query, request_timeout=200000, search_type="scan", scroll=SCROLL_TIMEOUT_SECS, size=SCROLL_SIZE)

        while True:
            resp = prod_es.scroll(resp['_scroll_id'], scroll=SCROLL_TIMEOUT_SECS)
            if len(resp['hits']['hits']) == 0 or total_fetched >=20000:
                break
            else:
                es_resps = resp['hits']['hits']
                profiles = []
                profiles = es_resps
                profile_count = (total_fetched + 1)
                total_fetched += len(profiles)
                print "total profiles fetched : ",total_fetched

                f = open('output_test_jsp.csv','a')
                # f_next_peak = open('output_test_jsp_next_peak.csv','a')
                # f_next_prev_peak = open('output_test_jsp_next_prev_peak.csv','a')
                forcefully_f = open('forcefully_test_jsp_output.csv','a')
                forcefully_study_f_pos = open('forcefully_study_profiles_pos.txt','a')
                forcefully_study_f_neg = open('forcefully_study_profiles_neg.txt','a')

                for profile in profiles:

                    print profile['_id']

                    print "profile no", profile_count

                    try:
                        (target) = create_target_dict(profile)
                        print "actual_target",target
                    except Exception as e:
                        print e
                        print "ERROR in create_target_dict"

                    # components of target array = ['id', [degree], [colleges], [college_start_dates], [company names], [company profile title], [company_start_date], [company_end_date], [current_location], [major], [skills_set]]

                    print "og  profile",profile

                    output = None

                    # try:
                    output = go_to_past(profile)
                    # except Exception as e:
                    #     print e
                    #     print "ERROR in go_to_past"

                    print "output",output

                    print "\n\n\n\n-------------------------------------------------------------------------\n\n\n\n"

                    profile_count = profile_count + 1

                # f.close()
    except Exception as e:
        print Exception, e

